package com.spectral369.views.home;

import com.spectral369.views.MainLayout;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

@PageTitle("Home")
@Route(value = "home", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class HomeView extends VerticalLayout {

	private static final long serialVersionUID = 5045650989135721377L;

	public HomeView() {
		setSpacing(false);

		Image img = new Image("images/dvlogo.png", "DV Logo");
		img.setWidth("44%");
		add(img);

		add(new H2("Primaria Dudestii-Vechi"));
		add(new H3("Registrul APIA"));

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.CENTER);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

}
