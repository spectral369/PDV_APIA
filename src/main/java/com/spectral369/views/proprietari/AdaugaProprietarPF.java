package com.spectral369.views.proprietari;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.converter.StringToBigIntegerConverter;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.ValueProvider;

public class AdaugaProprietarPF extends VerticalLayout {

	private static final long serialVersionUID = 6510806926311568232L;
	private Binder<AdaugaProprietarPF> binder;
	private InputStream fileData = null;
	private TextField IdProprietar;
	private TextField DenumireProprietar;
	private TextField cnp;
	private TextField adresaLocalitate;
	private TextField adresaStrada;
	private TextField adresaNumar;
	private TextField adresaBloc;
	private TextField adresaEtaj;
	private TextField adresaApartament;
	private TextField telefon;
	private IntegerField rol;
	private TextField pf_pj;
	private Upload upload;

	public AdaugaProprietarPF(ProprietariRepository proprietarRepo) {
		binder = new Binder<AdaugaProprietarPF>(AdaugaProprietarPF.class);
		H2 title = new H2("Adauga Proprietar PF");
		add(title);

		HorizontalLayout content = new HorizontalLayout();
		VerticalLayout panel1 = new VerticalLayout();
		VerticalLayout panel2 = new VerticalLayout();
		content.setAlignItems(Alignment.CENTER);
		content.setJustifyContentMode(JustifyContentMode.CENTER);

		FormLayout formLayout1 = new FormLayout();
		FormLayout formLayout2 = new FormLayout();
		panel1.add(formLayout1);
		panel2.add(formLayout2);
		content.add(panel1, panel2);

		IdProprietar = new TextField("ID Proprietar");
		IdProprietar.setRequiredIndicatorVisible(true);
		binder.forField(IdProprietar).asRequired().bind(new ValueProvider<AdaugaProprietarPF, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaProprietarPF source) {
				return null;
			}
		}, new Setter<AdaugaProprietarPF, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaProprietarPF bean, String fieldvalue) {

			}
		});
		IdProprietar.setErrorMessage("ID Proprietar Required !");
		IdProprietar.setWidth("100%");
		IdProprietar.setMaxLength(12);
		IdProprietar.setPlaceholder("ID Proprietar");
		formLayout1.add(IdProprietar);

		DenumireProprietar = new TextField("Nume Proprietar");
		DenumireProprietar.setRequiredIndicatorVisible(true);
		binder.forField(DenumireProprietar).asRequired()
				.withValidator(str -> str.toString().length() > 4,
						"Nume Proprietarului trebuie sa aibe mai mult de 4 caractere")
				.bind(new ValueProvider<AdaugaProprietarPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaProprietarPF source) {
						return null;
					}
				}, new Setter<AdaugaProprietarPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaProprietarPF bean, String fieldvalue) {

					}
				});
		DenumireProprietar.setErrorMessage("Nume Required !");
		DenumireProprietar.setWidth("100%");
		DenumireProprietar.setMaxLength(30);
		DenumireProprietar.setPlaceholder("Nume Proprietar");
		formLayout1.add(DenumireProprietar);

		cnp = new TextField("CNP Proprietar");
		cnp.setRequiredIndicatorVisible(true);
		binder.forField(cnp).asRequired().withValidator(str -> str.toString().length() == 13, "CNP-ul are 13 cifre")
				.withConverter(new StringToBigIntegerConverter("CNP-ul poate contine doar cifre"))
				.bind(new ValueProvider<AdaugaProprietarPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(AdaugaProprietarPF source) {
						return null;
					}
				}, new Setter<AdaugaProprietarPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaProprietarPF bean, BigInteger fieldvalue) {

					}
				});
		cnp.setErrorMessage("CNP Required !");
		cnp.setWidth("100%");
		cnp.setMaxLength(13);
		cnp.setPlaceholder("CNP Proprietar");
		cnp.setMinWidth(25f, Unit.EM);
		formLayout1.add(cnp);

		adresaLocalitate = new TextField("Adresa Localiatate");
		adresaLocalitate.setRequiredIndicatorVisible(true);
		binder.forField(adresaLocalitate).asRequired()
				.withValidator(str -> str.toString().length() > 4,
						"Localitatea trebuie sa aibe mai mult de 4 caractere")
				.bind(new ValueProvider<AdaugaProprietarPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaProprietarPF source) {
						return null;
					}
				}, new Setter<AdaugaProprietarPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaProprietarPF bean, String fieldvalue) {

					}
				});
		adresaLocalitate.setErrorMessage("Localitate Required !");
		adresaLocalitate.setWidth("100%");
		adresaLocalitate.setMaxLength(25);
		adresaLocalitate.setPlaceholder("Localitate");
		adresaLocalitate.setMinWidth(15f, Unit.EM);
		formLayout1.add(adresaLocalitate);

		adresaStrada = new TextField("Adresa Strada");
		adresaStrada.setWidth("100%");
		adresaStrada.setMaxLength(35);
		adresaStrada.setPlaceholder("Strada");
		adresaStrada.setMinWidth(12f, Unit.EM);
		formLayout1.add(adresaStrada);

		adresaNumar = new TextField("Adresa Numar");
		adresaNumar.setRequiredIndicatorVisible(true);
		binder.forField(adresaNumar).asRequired()
				.withValidator(str -> str.matches("^(\\d){1,4}[/]{0,1}[a-zA-Z]{0,1}$"), "Numar de casa invalid !")
				.bind(new ValueProvider<AdaugaProprietarPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaProprietarPF source) {
						return null;
					}
				}, new Setter<AdaugaProprietarPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaProprietarPF bean, String fieldvalue) {

					}
				});
		adresaNumar.setErrorMessage("Numar Required !");
		adresaNumar.setWidth("100%");
		adresaNumar.setMaxLength(8);
		adresaNumar.setPlaceholder("Numar");
		formLayout1.add(adresaNumar);

		adresaBloc = new TextField("Adresa Bloc");
		adresaBloc.setWidth("100%");
		adresaBloc.setMaxLength(4);
		adresaBloc.setPlaceholder("Bloc");
		formLayout1.add(adresaBloc);

		adresaEtaj = new TextField("Adresa Etaj");
		adresaEtaj.setWidth("100%");
		adresaEtaj.setMaxLength(4);
		adresaEtaj.setPlaceholder("Etaj");
		formLayout1.add(adresaEtaj);

		adresaApartament = new TextField("Adresa Apartament");
		adresaApartament.setWidth("100%");
		adresaApartament.setMaxLength(4);
		adresaApartament.setPlaceholder("Apartament");
		formLayout1.add(adresaApartament);

		telefon = new TextField("Telefon");
		telefon.setRequiredIndicatorVisible(true);
		binder.forField(telefon).asRequired()
				.withValidator(str -> str.matches("^([00]{0,1}[\\d]{1,3}[0-9]{9,12})$"), "Numar de telefon invalid !")
				.withConverter(new StringToBigIntegerConverter("Nr. telefon poate contine doar cifre"))
				.bind(new ValueProvider<AdaugaProprietarPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(AdaugaProprietarPF source) {
						return null;
					}
				}, new Setter<AdaugaProprietarPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaProprietarPF bean, BigInteger fieldvalue) {

					}
				});
		telefon.setErrorMessage("Telefon Required !");
		telefon.setWidth("100%");
		telefon.setMaxLength(14);
		telefon.setPlaceholder("07xxxxxxxx");
		telefon.setMinWidth(15f, Unit.EM);
		formLayout1.add(telefon);

		rol = new IntegerField("Rol");
		rol.setRequiredIndicatorVisible(true);
		binder.forField(rol).asRequired().bind(new ValueProvider<AdaugaProprietarPF, Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Integer apply(AdaugaProprietarPF source) {
				return null;
			}
		}, new Setter<AdaugaProprietarPF, Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaProprietarPF bean, Integer fieldvalue) {

			}
		});
		rol.setErrorMessage("Rol Required !");
		rol.setWidth("100%");
		rol.setPlaceholder("Rol");
		rol.setValueChangeMode(ValueChangeMode.TIMEOUT);
		rol.setValueChangeTimeout(1000);
		formLayout2.add(rol);

		pf_pj = new TextField("PF/PJ");
		pf_pj.setRequiredIndicatorVisible(true);
		pf_pj.setValue("Persoana Fizica");
		pf_pj.setEnabled(false);
		pf_pj.setErrorMessage("PF/PJ Required !");
		pf_pj.setWidth("100%");
		pf_pj.setMaxLength(18);
		pf_pj.setPlaceholder("PF/PJ");
		formLayout2.add(pf_pj);

		FileBuffer FileMemoryBuffer = new FileBuffer();
		upload = new Upload(FileMemoryBuffer);
		Button uploadButton = new Button("Upload PDF...");
		uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		upload.setUploadButton(uploadButton);
		upload.setAcceptedFileTypes("application/pdf", ".pdf");
		upload.setMaxFiles(1);
		upload.setDropAllowed(false);
		int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
		upload.setMaxFileSize(maxFileSizeInBytes);
		upload.setMinWidth(15, Unit.EM);

		upload.addSucceededListener(event -> {

			fileData = FileMemoryBuffer.getInputStream();
			String fileName = event.getFileName();
			String mimeType = event.getMIMEType();
			System.out.println(fileName + " " + mimeType);
		});
		upload.addFileRejectedListener(event -> {
			String errorMessage = event.getErrorMessage();

			Notification notification = Notification.show(errorMessage, 5000, Notification.Position.BOTTOM_CENTER);
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
		});

		upload.getElement().addEventListener("max-files-reached-changed", event -> {
			boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
			uploadButton.setEnabled(!maxFilesReached);
		}).addEventData("event.detail.value");

		formLayout2.add(upload);

		formLayout1.setResponsiveSteps(
				// Use one column by default
				new ResponsiveStep("0", 1),
				// Use two columns, if layout's width exceeds 500px
				new ResponsiveStep("500px", 2));
		formLayout1.setColspan(cnp, 2);
		formLayout1.setColspan(DenumireProprietar, 2);
		formLayout1.setColspan(adresaLocalitate, 2);
		formLayout1.setColspan(adresaStrada, 2);

		formLayout2.setResponsiveSteps(new ResponsiveStep("0", 1), new ResponsiveStep("500px", 2));

		HorizontalLayout fields4 = new HorizontalLayout();
		fields4.setAlignItems(Alignment.CENTER);

		Button submit = new Button("Insert");
		submit.setEnabled(false);
		submit.addClickListener(evt -> {

			PROPRIETARI Proprietar = new PROPRIETARI();
			Proprietar.setID(Integer.parseInt(IdProprietar.getValue().trim()));
			Proprietar.setNumeProprietar(DenumireProprietar.getValue().trim());
			Proprietar.setCNP_CUI(cnp.getValue().trim());
			Proprietar.setADRESA_LOCALITATE(adresaLocalitate.getValue().trim());
			Proprietar.setADRESA_STRADA(adresaStrada.getValue().trim());
			Proprietar.setADRESA_NUMAR(adresaNumar.getValue());
			if (!adresaBloc.isEmpty())
				Proprietar.setADRESA_BLOC(adresaBloc.getValue().trim());

			if (!adresaEtaj.isEmpty())
				Proprietar.setADRESA_ETAJ(adresaEtaj.getValue().trim());

			if (!adresaApartament.isEmpty())
				Proprietar.setADRESA_APARTAMENT(adresaApartament.getValue().trim());

			Proprietar.setTELEFON(telefon.getValue().trim());
			Proprietar.setROL(rol.getValue());
			Proprietar.setPF_PJ(pf_pj.getValue().trim());
			if (fileData != null) {
				Blob fd = null;
				try {
					fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

						@Override
						public Exception interceptException(Exception sqlEx) {
							System.out.println("Erroare1 la upload PF!");
							return null;
						}

						@Override
						public ExceptionInterceptor init(Properties props, Log log) {
							System.out.println("Erroare2 la upload PF!");
							return null;
						}

						@Override
						public void destroy() {
							System.out.println("Erroare3 la upload PF!");

						}
					});
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}

				Proprietar.setDOSAR(fd);
			}
			proprietarRepo.save(Proprietar);
			clear();
			Notification notification = Notification.show("Insertion(PF)...Done !", 5000,
					Notification.Position.BOTTOM_END);
			notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
		});
		Button clear = new Button("Clear");
		clear.addClickListener(event -> {

			clear();
			Notification.show("Field Reset...Done!", 5000, Notification.Position.BOTTOM_END);

		});
		binder.addStatusChangeListener(event -> {

			if (binder.isValid()) {
				submit.setEnabled(true);
			} else {
				submit.setEnabled(false);
			}
		});
		fields4.add(submit);
		fields4.add(clear);

		add(content);
		add(fields4);
		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private void clear() {
		IdProprietar.clear();
		cnp.clear();
		DenumireProprietar.clear();
		adresaLocalitate.clear();
		adresaStrada.clear();
		adresaNumar.clear();
		adresaBloc.clear();
		adresaEtaj.clear();
		adresaApartament.clear();
		telefon.clear();
		rol.clear();
	}

}
