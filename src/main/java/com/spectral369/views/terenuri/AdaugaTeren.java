package com.spectral369.views.terenuri;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.spectral369.db.terenuri.TERENURI;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.ValueProvider;

public class AdaugaTeren extends VerticalLayout {

	private static final long serialVersionUID = 2823182027395074635L;
	private InputStream fileData = null;
	private Binder<AdaugaTeren> binder;
	private TextField numeProprietar;
	private TextField tarpar;
	private TextField suprafataTotala;
	private ComboBox<String> categorie;
	private Upload upload;
	private TextField cota;
	private TextField cf;
	private PROPRIETARI proprietar;
	private IntegerField blocFizic;

	public AdaugaTeren(TerenuriRepository terenRepo, ProprietariRepository proprietariRepo) {
		binder = new Binder<AdaugaTeren>(AdaugaTeren.class);
		H2 title = new H2("Adauga Teren");
		add(title);

		HorizontalLayout content = new HorizontalLayout();
		VerticalLayout panel1 = new VerticalLayout();
		FormLayout formLayout1 = new FormLayout();
		panel1.add(formLayout1);
		content.add(panel1);

		Button searchProprietarProprietar = new Button(VaadinIcon.SEARCH.create());
		searchProprietarProprietar.addClickListener(evt -> {
			Dialog dialog = new Dialog();

			dialog.setHeaderTitle("Alege proprietar");

			VerticalLayout contentLayout = new VerticalLayout();
			contentLayout.setAlignItems(Alignment.CENTER);
			contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

			HorizontalLayout searchLayout = new HorizontalLayout();
			searchLayout.setAlignItems(Alignment.CENTER);
			searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
			TextField searchField = new TextField("Cauta CNP/CUI");
			searchField.setPlaceholder("CNP sau CUI");
			searchField.setRequiredIndicatorVisible(true);
			searchField.setErrorMessage("CNP/CUI Required !");
			searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
			searchField.setValueChangeTimeout(1000);
			searchField.setWidth("100%");
			searchField.setMaxLength(14);

			ListBox<String> listbox = new ListBox<String>();

			List<PROPRIETARI> props = proprietariRepo.findAll();
			List<String> numep = new ArrayList<String>();
			for (PROPRIETARI p : props) {
				numep.add(p.getID() + ":" + p.getNumeProprietar());

			}
			listbox.setItems(numep);

			Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
			searchbtn.setEnabled(false);
			searchbtn.addClickListener(event -> {
				listbox.clear();
				numep.clear();
				List<PROPRIETARI> propByCnpCui = proprietariRepo
						.findByCNPCUIContainingIgnoreCase(searchField.getValue().trim());
				for (PROPRIETARI p : propByCnpCui) {
					numep.add(p.getID() + ":" + p.getNumeProprietar());
				}
				listbox.setItems(numep);

			});

			searchLayout.add(searchField, searchbtn);
			contentLayout.add(searchLayout, listbox);
			dialog.add(contentLayout);
			Button ok = new Button("OK", (e) -> {
				int pid = Integer.parseInt(listbox.getValue().substring(0, listbox.getValue().indexOf(":")));
				proprietar = proprietariRepo.findProprietarByID(pid);
				dialog.close();
				numeProprietar.setValue(proprietar.getNumeProprietar());

			});
			ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			ok.getStyle().set("margin-right", "auto");
			dialog.getFooter().add(ok);
			Button cancel = new Button("Cancel", (e) -> {
				dialog.close();

			});
			cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

			dialog.getFooter().add(cancel);
			dialog.setModal(true);
			dialog.setCloseOnEsc(true);
			dialog.setCloseOnOutsideClick(false);
			UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
				dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
				dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

			});
			searchField.addValueChangeListener(vcl -> {
				if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
					searchbtn.setEnabled(true);
				} else {
					searchbtn.setEnabled(false);
				}
			});

			dialog.open();

		});

		numeProprietar = new TextField("Nume Poprietar");
		numeProprietar.setEnabled(false);
		numeProprietar.setRequiredIndicatorVisible(true);
		binder.forField(numeProprietar).asRequired().bind(new ValueProvider<AdaugaTeren, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaTeren source) {
				return null;
			}
		}, new Setter<AdaugaTeren, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaTeren bean, String fieldvalue) {

			}
		});
		numeProprietar.setErrorMessage("Alegerea Proprietarului Required !");
		numeProprietar.setPlaceholder("Nume Proprietar");

		formLayout1.add(searchProprietarProprietar, numeProprietar);

		tarpar = new TextField("Tarla/Parcela");
		tarpar.setRequiredIndicatorVisible(true);
		binder.forField(tarpar).asRequired().bind(new ValueProvider<AdaugaTeren, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaTeren source) {
				return null;
			}
		}, new Setter<AdaugaTeren, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaTeren bean, String fieldvalue) {

			}
		});
		tarpar.setErrorMessage("Tar/Par Required !");
		tarpar.setWidth("100%");
		tarpar.setMaxLength(35);
		tarpar.setPlaceholder("Tarla/Parcela");
		tarpar.setMinWidth(12f, Unit.EM);
		formLayout1.add(tarpar);

		cf = new TextField("Nr. C.F");
		cf.setWidth("100%");
		cf.setMaxLength(8);
		cf.setPlaceholder("Nr. C.F");
		cf.setMinWidth(12f, Unit.EM);
		formLayout1.add(cf);

		blocFizic = new IntegerField("Bloc Fizic");
		blocFizic.setWidth("100%");
		// blocFizic.setMaxLength(8);
		blocFizic.setPlaceholder("Nr. Bloc Fizic");
		blocFizic.setMinWidth(12f, Unit.EM);
		formLayout1.add(blocFizic);

		suprafataTotala = new TextField("Suprafata Totala");
		suprafataTotala.setRequiredIndicatorVisible(true);
		binder.forField(suprafataTotala).asRequired()
				.withConverter(new StringToDoubleConverter("Suprafata poate contine doar cifre"))
				.bind(new ValueProvider<AdaugaTeren, Double>() {

					private static final long serialVersionUID = 1L;

					@Override
					public Double apply(AdaugaTeren source) {
						return null;
					}
				}, new Setter<AdaugaTeren, Double>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaTeren bean, Double fieldvalue) {

					}
				});
		suprafataTotala.setErrorMessage("Suprafata Totala Required !");
		suprafataTotala.setWidth("100%");
		suprafataTotala.setMaxLength(35);
		suprafataTotala.setPlaceholder("Suprafata Totala");
		suprafataTotala.setMinWidth(15f, Unit.EM);
		formLayout1.add(suprafataTotala);

		cota = new TextField("Cota Prop.");
		cota.setRequiredIndicatorVisible(true);
		binder.forField(cota).asRequired().withConverter(new StringToDoubleConverter("Cota poate contine doar cifre"))
				.bind(new ValueProvider<AdaugaTeren, Double>() {

					private static final long serialVersionUID = 1L;

					@Override
					public Double apply(AdaugaTeren source) {
						return null;
					}
				}, new Setter<AdaugaTeren, Double>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaTeren bean, Double fieldvalue) {

					}
				});
		cota.setErrorMessage("Cota Required !");
		cota.setWidth("100%");
		cota.setValueChangeMode(ValueChangeMode.TIMEOUT);
		cota.setValueChangeTimeout(1000);
		cota.setPlaceholder("Cota");
		formLayout1.add(cota);

		/*
		 * suprafataUtilizata = new TextField("Suprafata Utilizata");
		 * suprafataUtilizata.setWidth("100%"); suprafataUtilizata.setMaxLength(35);
		 * suprafataUtilizata.setPlaceholder("Suprafata Utilizata");
		 * suprafataUtilizata.setMinWidth(15f, Unit.EM);
		 * formLayout1.add(suprafataUtilizata);
		 */

		categorie = new ComboBox<String>("Categorie Teren");
		categorie.setItems("ARABIL", "PASUNE", "FANEATA", "PADURE", "VIE", "NEPRODUCTIV");
		categorie.setValue("ARABIL");
		categorie.setRequiredIndicatorVisible(true);
		binder.forField(categorie).asRequired().bind(new ValueProvider<AdaugaTeren, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaTeren source) {
				return null;
			}
		}, new Setter<AdaugaTeren, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaTeren bean, String fieldvalue) {

			}
		});
		categorie.setErrorMessage("Categorie Required !");
		categorie.setWidth("100%");
		categorie.setPlaceholder("Categorie Teren");
		categorie.setMinWidth(12f, Unit.EM);
		formLayout1.add(categorie);

		FileBuffer FileMemoryBuffer = new FileBuffer();
		upload = new Upload(FileMemoryBuffer);
		Button uploadButton = new Button("Upload PDF...");
		uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		upload.setUploadButton(uploadButton);
		upload.setAcceptedFileTypes("application/pdf", ".pdf");
		upload.setMaxFiles(1);
		upload.setDropAllowed(false);
		int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
		upload.setMaxFileSize(maxFileSizeInBytes);
		upload.setMinWidth(15, Unit.EM);

		upload.addSucceededListener(event -> {

			fileData = FileMemoryBuffer.getInputStream();
			String fileName = event.getFileName();
			String mimeType = event.getMIMEType();
			System.out.println(fileName + " " + mimeType);
		});
		upload.addFileRejectedListener(event -> {
			String errorMessage = event.getErrorMessage();

			Notification notification = Notification.show(errorMessage, 5000, Notification.Position.BOTTOM_CENTER);
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
		});

		upload.getElement().addEventListener("max-files-reached-changed", event -> {
			boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
			uploadButton.setEnabled(!maxFilesReached);
		}).addEventData("event.detail.value");

		formLayout1.add(upload);

		formLayout1.setResponsiveSteps(
				// Use one column by default
				new ResponsiveStep("0", 1),
				// Use two columns, if layout's width exceeds 500px
				new ResponsiveStep("500px", 2));

		HorizontalLayout btnFields = new HorizontalLayout();
		btnFields.setAlignItems(Alignment.CENTER);

		Button submit = new Button("Insert");
		submit.setEnabled(false);
		submit.addClickListener(evt -> {
			TERENURI teren = new TERENURI();

			teren.setPoprietarID(proprietar);
			teren.setTarPar(tarpar.getValue().trim());
			teren.setCF(cf.getValue().trim());
			double supTotala = Double.parseDouble(suprafataTotala.getValue().trim());
			teren.setSuprafataTotala(supTotala);
			if (!blocFizic.isEmpty())
				teren.setBlocFizic(blocFizic.getValue());
			double ncota = Double.parseDouble(cota.getValue().trim());
			teren.setCota(ncota);
			double fixCota = ncota / 100;
			double sup_util = supTotala * fixCota;
			teren.setSuprafataUtilizata(sup_util);
			teren.setContract();
			teren.setCategorie(categorie.getValue().trim());
			if (fileData != null) {
				Blob fd = null;
				try {
					fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

						@Override
						public Exception interceptException(Exception sqlEx) {
							System.out.println("Erroare1 la upload PF!");
							return null;
						}

						@Override
						public ExceptionInterceptor init(Properties props, Log log) {
							System.out.println("Erroare2 la upload PF!");
							return null;
						}

						@Override
						public void destroy() {
							System.out.println("Erroare3 la upload PF!");

						}
					});
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}

				teren.setDOSAR(fd);
			}
			terenRepo.save(teren);

			clear();

		});

		Button clear = new Button("Clear");
		clear.addClickListener(event -> {
			clear();

			Notification.show("Field Reset...Done!", 5000, Notification.Position.BOTTOM_END);

		});

		binder.addStatusChangeListener(event -> {

			if (binder.isValid()) {
				submit.setEnabled(true);
			} else {
				submit.setEnabled(false);
			}
		});

		btnFields.add(submit, clear);

		add(content);
		add(btnFields);
		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private void clear() {
		numeProprietar.clear();
		tarpar.clear();
		cf.clear();
		suprafataTotala.clear();
		cota.clear();
		categorie.clear();
		blocFizic.clear();
		upload.clearFileList();
	}

}
