package com.spectral369.views.rapoarte.fermieri;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.fermieri.FERMIERI;
import com.spectral369.db.fermieri.FermieriRepository;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

public class RaportSimpluFermieri extends VerticalLayout {

	private static final long serialVersionUID = 2578351231621597521L;

	public RaportSimpluFermieri(FermieriRepository fermieriRepo) {
		H2 title = new H2("Raport Simplu Fermieri");
		add(title);
		List<FERMIERI> list = fermieriRepo.findAll();
		Grid<FERMIERI> fermieriGrid = new Grid<FERMIERI>(FERMIERI.class, false);
		fermieriGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
		fermieriGrid.setDetailsVisibleOnClick(false);
		Grid.Column<FERMIERI> DenumireColumn = fermieriGrid.addColumn(FERMIERI::getDENUMIRE_FERMIER)
				.setTooltipGenerator(fermier -> fermier.getDENUMIRE_FERMIER()).setAutoWidth(true).setFlexGrow(0)
				.setFooter(String.format("Nr. Total Fermieri %s", list.size()));

		Grid.Column<FERMIERI> CNPCUIColumn = fermieriGrid.addColumn(FERMIERI::getCNP_CUI)
				.setTooltipGenerator(fermier -> fermier.getCNP_CUI()).setAutoWidth(true).setFlexGrow(0);
		Grid.Column<FERMIERI> IDAPIAColumn = fermieriGrid.addColumn(FERMIERI::getID_APIA);
		Grid.Column<FERMIERI> LocalitateColumn = fermieriGrid.addColumn(FERMIERI::getADRESA_LOCALITATE);
		Grid.Column<FERMIERI> TelefonColumn = fermieriGrid.addColumn(FERMIERI::getTELEFON)
				.setTooltipGenerator(fermier -> fermier.getTELEFON());
		Grid.Column<FERMIERI> VolumColumn = fermieriGrid.addColumn(FERMIERI::getVOLUM);
		Grid.Column<FERMIERI> PozitieColumn = fermieriGrid.addColumn(FERMIERI::getPOZITIE);
		// Grid.Column<FERMIERI> TipColumn = fermieriGrid.addColumn(FERMIERI::getTIP);
		Grid.Column<FERMIERI> ReprezentantColumn = fermieriGrid.addColumn(FERMIERI::getREPREZENTANT_PJ)
				.setTooltipGenerator(fermier -> fermier.getREPREZENTANT_PJ());
		Column<FERMIERI> details = fermieriGrid.addColumn(createToggleDetailsRenderer(fermieriGrid)).setAutoWidth(true);
		/// Column<FERMIERI> edit =
		/// fermieriGrid.addColumn(createEditRenderer(fermieriGrid)).setAutoWidth(true);
		Column<FERMIERI> down = fermieriGrid.addColumn(createActionRenderer()).setFrozenToEnd(true).setAutoWidth(true)
				.setFlexGrow(0);

		GridListDataView<FERMIERI> dataView = fermieriGrid.setItems(list);

		FermieriFilter fermieriFilter = new FermieriFilter(dataView);

		fermieriGrid.getHeaderRows().clear();
		HeaderRow headerRow = fermieriGrid.appendHeaderRow();

		headerRow.getCell(DenumireColumn)
				.setComponent(createFilterHeader("Denumire Fermier", fermieriFilter::setDenumireFermier));
		headerRow.getCell(CNPCUIColumn).setComponent(createFilterHeader("CNP/CUI", fermieriFilter::setCNP_CUI));
		headerRow.getCell(IDAPIAColumn).setComponent(createFilterHeader("ID APIA", fermieriFilter::setID_APIA));
		headerRow.getCell(LocalitateColumn)
				.setComponent(createFilterHeader("Localitate", fermieriFilter::setADRESA_LOCALITATE));
		headerRow.getCell(TelefonColumn).setComponent(createFilterHeader("Telefon", fermieriFilter::setTELEFON));
		headerRow.getCell(VolumColumn).setComponent(createFilterHeader("Volum", fermieriFilter::setVOLUM));
		headerRow.getCell(PozitieColumn).setComponent(createFilterHeader("Pozitie", fermieriFilter::setPOZITIE));
		// headerRow.getCell(TipColumn).setComponent(createFilterHeader("Tip",
		// fermieriFilter::setTIP));
		headerRow.getCell(ReprezentantColumn)
				.setComponent(createFilterHeader("Reprezentant PJ", fermieriFilter::setREPREZENTANT_PJ));
		headerRow.getCell(details).setComponent(new Span("Detalii"));
		// headerRow.getCell(edit).setComponent(new Span("Editor"));
		headerRow.getCell(down).setComponent(new Span("Download"));
		fermieriGrid.setItemDetailsRenderer(createFermierDetailsRenderer());
		new FermierContextMenu(fermieriGrid, fermieriRepo);

		add(fermieriGrid);

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private static ComponentRenderer<FermierDetailsFormLayout, FERMIERI> createFermierDetailsRenderer() {
		return new ComponentRenderer<>(FermierDetailsFormLayout::new, FermierDetailsFormLayout::setFERMIER);
	}

	private static Renderer<FERMIERI> createToggleDetailsRenderer(Grid<FERMIERI> grid) {
		return LitRenderer
				.<FERMIERI>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Detalii</vaadin-button>")
				.withFunction("handleClick",
						fermieri -> grid.setDetailsVisible(fermieri, !grid.isDetailsVisible(fermieri)));
	}

	/*
	 * private static Renderer<FERMIERI> createEditRenderer(Grid<FERMIERI> grid) {
	 * return LitRenderer .<FERMIERI>
	 * of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Edit</vaadin-button>"
	 * ) .withFunction("handleClick", fermieri ->
	 * Notification.show("Not yet implemented", 2500, Position.BOTTOM_END)); }
	 */

	private static Renderer<FERMIERI> createActionRenderer() {
		return LitRenderer
				.<FERMIERI>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Download</vaadin-button>")
				.withFunction("handleClick", t -> {
					if (t.getDOSAR() != null) {

						java.sql.Blob dosar = t.getDOSAR();
						try {
							InputStream is = dosar.getBinaryStream();

							File targetFile = new File(
									System.getProperty("user.home") + File.separatorChar + "dosar.pdf");
							OutputStream outStream = new FileOutputStream(targetFile);

							byte[] buffer = new byte[8 * 1024];
							int bytesRead;
							while ((bytesRead = is.read(buffer)) != -1) {
								outStream.write(buffer, 0, bytesRead);
							}
							IOUtils.closeQuietly(is);
							IOUtils.closeQuietly(outStream);

							final StreamResource resource = new StreamResource(targetFile.getName(), () -> {
								try {
									return new ByteArrayInputStream(FileUtils.readFileToByteArray(targetFile));
								} catch (IOException e) {
									System.out.println(e.getLocalizedMessage());
								}
								return is;
							});

							final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry()
									.registerResource(resource);
							UI.getCurrent().getPage().open(registration.getResourceUri().toString());

						} catch (Exception e) {
							System.out.println(e.getLocalizedMessage());
						}
					} else {
						Notification.show("Nothing to download", 2500, Position.BOTTOM_END);
					}
				});

	}

	private static Component createFilterHeader(String labelText, Consumer<String> filterChangeConsumer) {
		NativeLabel label = new NativeLabel(labelText);
		label.getStyle().set("padding-top", "var(--lumo-space-m)").set("font-size", "var(--lumo-font-size-xs)");
		TextField textField = new TextField();
		textField.setValueChangeMode(ValueChangeMode.EAGER);
		textField.setClearButtonVisible(true);
		textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
		textField.setWidthFull();
		textField.getStyle().set("max-width", "100%");
		textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
		VerticalLayout layout = new VerticalLayout(label, textField);
		layout.getThemeList().clear();
		layout.getThemeList().add("spacing-xs");

		return layout;
	}

	private static class FermieriFilter {
		private final GridListDataView<FERMIERI> dataView;

		private String DENUMIRE_FERMIER;;
		private String CNP_CUI;
		private String ID_APIA;
		private String ADRESA_LOCALITATE;
		private String TELEFON;
		private String VOLUM;
		private int POZITIE;
		private String TIP;
		private String REPREZENTANT_PJ;

		public FermieriFilter(GridListDataView<FERMIERI> dataView) {
			this.dataView = dataView;
			this.dataView.addFilter(this::test);
		}

		public void setDenumireFermier(String denumireFermier) {
			this.DENUMIRE_FERMIER = denumireFermier;
			this.dataView.refreshAll();
		}

		public void setCNP_CUI(String cnp_cui) {
			this.CNP_CUI = cnp_cui;
			this.dataView.refreshAll();
		}

		public void setID_APIA(String idapia) {
			this.ID_APIA = idapia;
			this.dataView.refreshAll();
		}

		public void setADRESA_LOCALITATE(String localitate) {
			this.ADRESA_LOCALITATE = localitate;
			this.dataView.refreshAll();
		}

		public void setTELEFON(String telefon) {
			this.TELEFON = telefon;
			this.dataView.refreshAll();
		}

		public void setVOLUM(String volum) {
			this.VOLUM = volum;
			this.dataView.refreshAll();
		}

		public void setPOZITIE(String pozitie) {
			try {
				this.POZITIE = Integer.parseInt(pozitie);
			} catch (Exception e) {
				this.POZITIE = 0;
			}
			this.dataView.refreshAll();
		}

		@SuppressWarnings("unused")
		public void setTIP(String tip) {
			this.TIP = tip;
			this.dataView.refreshAll();
		}

		public void setREPREZENTANT_PJ(String reprezentant) {
			this.REPREZENTANT_PJ = reprezentant;
			this.dataView.refreshAll();
		}

		public boolean test(FERMIERI fermier) {
			boolean matchesDenumireFermier = matches(fermier.getDENUMIRE_FERMIER(), DENUMIRE_FERMIER);
			boolean matchesCNP_CUI = matches(fermier.getCNP_CUI(), CNP_CUI);
			boolean matchesID_APIA = matches(fermier.getID_APIA(), ID_APIA);
			boolean matchesAresaLocalitate = matches(fermier.getADRESA_LOCALITATE(), ADRESA_LOCALITATE);
			boolean matchesTelefon = matches(fermier.getTELEFON(), TELEFON);
			boolean matchesVolum = matches(fermier.getVOLUM(), VOLUM);
			boolean matchesPozitie = matchesInt(fermier.getPOZITIE(), POZITIE);
			boolean matchesTip = matches(fermier.getTIP(), TIP);
			boolean matchesReprezentPJ = matches(fermier.getREPREZENTANT_PJ(), REPREZENTANT_PJ);

			return matchesDenumireFermier && matchesCNP_CUI && matchesID_APIA && matchesAresaLocalitate
					&& matchesTelefon && matchesVolum && matchesReprezentPJ && matchesPozitie && matchesTip;
		}

		private boolean matches(String value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesInt(int value, int searchTerm) {

			return searchTerm == 0 || value == 0
					|| String.valueOf(value).toLowerCase().contains(String.valueOf(searchTerm));
		}
	}

	private static class FermierDetailsFormLayout extends FormLayout {
		private static final long serialVersionUID = 6425872388237011077L;
		private final IntegerField ID = new IntegerField("ID");
		private final IntegerField ID_FERMIER = new IntegerField("ID FERMIER");
		private final TextField ID_APIA = new TextField("ID APIA");
		private final TextField CNP_CUI = new TextField("CNP/CUI");
		private final TextField DENUMIRE_FERMIER = new TextField("DENUMIRE FERMIER");
		private final TextField LOCALITATE = new TextField("LOCALITATE");
		private final TextField STRADA = new TextField("STRADA");
		private final TextField NUMAR = new TextField("NUMAR");
		private final TextField BLOC = new TextField("BLOC");
		private final TextField ETAJ = new TextField("ETAJ");
		private final TextField APARTAMENT = new TextField("APARTAMENT");
		private final TextField JUDET = new TextField("JUDET");
		private final TextField TELEFON = new TextField("TELEFON");
		private final TextField VOLUM = new TextField("VOLUM");
		private final IntegerField POZITIE = new IntegerField("POZITIE");
		private final ComboBox<String> TIP = new ComboBox<String>("LOCALITATE");
		private final IntegerField ROL = new IntegerField("ROL");
		private final TextField PF_PJ = new TextField("PF/PJ");

		/*
		 * FileBuffer FileMemoryBuffer = new FileBuffer(); Upload upload = new
		 * Upload(FileMemoryBuffer);
		 */

		public FermierDetailsFormLayout() {
			/* Bug nu functioneaza cu stream !! */
			/*
			 * Stream.of(ID, ID_FERMIER, ID_APIA,CNP_CUI, DENUMIRE_FERMIER,
			 * LOCALITATE,STRADA, NUMAR, BLOC,ETAJ,APARTAMENT,JUDET
			 * ,TELEFON,VOLUM,POZITIE,TIP,ROL,PF_PJ).forEach(field -> {
			 * field.setReadOnly(false); add(field); });
			 */
			add(ID, ID_FERMIER, ID_APIA, CNP_CUI, DENUMIRE_FERMIER, LOCALITATE, STRADA, NUMAR, BLOC, ETAJ, APARTAMENT,
					JUDET, TELEFON, VOLUM, POZITIE, TIP, ROL, PF_PJ);
			ID_FERMIER.setReadOnly(true);
			ID_APIA.setReadOnly(true);
			CNP_CUI.setReadOnly(true);
			DENUMIRE_FERMIER.setReadOnly(true);
			LOCALITATE.setReadOnly(true);
			STRADA.setReadOnly(true);
			NUMAR.setReadOnly(true);
			BLOC.setReadOnly(true);
			ETAJ.setReadOnly(true);
			APARTAMENT.setReadOnly(true);
			JUDET.setReadOnly(true);
			TELEFON.setReadOnly(true);
			VOLUM.setReadOnly(true);
			POZITIE.setReadOnly(true);
			TIP.setReadOnly(true);
			ROL.setReadOnly(true);
			PF_PJ.setReadOnly(true);

			setResponsiveSteps(new ResponsiveStep("0", 3));
			ID.setEnabled(false);

		}

		public void setFERMIER(final FERMIERI fermier) {
			ID.setValue(fermier.getID());
			if (!(fermier.getID_APIA() == null))
				ID_APIA.setValue(fermier.getID_APIA());
			ID_FERMIER.setValue(fermier.getID_FERMIER());
			CNP_CUI.setValue(fermier.getCNP_CUI());
			DENUMIRE_FERMIER.setValue(fermier.getDENUMIRE_FERMIER());
			LOCALITATE.setValue(fermier.getADRESA_LOCALITATE());
			if (!(fermier.getADRESA_STRADA() == null))
				STRADA.setValue(fermier.getADRESA_STRADA());
			if (!(fermier.getADRESA_NUMAR() == null))
				NUMAR.setValue(fermier.getADRESA_NUMAR());
			if (!(fermier.getADRESA_BLOC() == null))
				BLOC.setValue(fermier.getADRESA_BLOC());
			if (!(fermier.getADRESA_ETAJ() == null))
				ETAJ.setValue(fermier.getADRESA_ETAJ());
			if (!(fermier.getADRESA_APARTAMENT() == null))
				APARTAMENT.setValue(fermier.getADRESA_APARTAMENT());
			JUDET.setValue(fermier.getADRESA_JUDET());
			if (!(fermier.getADRESA_STRADA() == null))
				TELEFON.setValue(fermier.getTELEFON());
			if (!(fermier.getVOLUM() == null))
				VOLUM.setValue(fermier.getVOLUM());
			if (!(fermier.getPOZITIE() == 0))
				POZITIE.setValue(fermier.getPOZITIE());
			TIP.setItems("PF Locala", "PF Externa", "PJ Locala", "PJ Externa");
			TIP.setValue(fermier.getTIP());
			ROL.setValue(fermier.getROL());
			if (!(fermier.getPF_PJ().isEmpty() || fermier.getPF_PJ().isBlank()))
				PF_PJ.setValue(fermier.getPF_PJ());

		}

	}

	private static class FermierContextMenu extends GridContextMenu<FERMIERI> {
		private static final long serialVersionUID = 8883017315048714541L;
		private InputStream fileData;

		public FermierContextMenu(Grid<FERMIERI> target, FermieriRepository fermieriRepo) {
			super(target);

			addItem("Edit", e -> e.getItem().ifPresent(fermier -> {
				Dialog editDialog = new Dialog();
				editDialog.setHeaderTitle("Update Fermier");

				IntegerField idFermier = new IntegerField("ID Fermier");
				idFermier.setValue(fermier.getID_FERMIER());
				idFermier.setRequiredIndicatorVisible(true);
				idFermier.setErrorMessage("ID Fermier required!");

				TextField idApia = new TextField("ID Apia");
				if (fermier.getID_APIA() != null)
					idApia.setValue(fermier.getID_APIA());

				TextField denumireFermier = new TextField("Denumire Fermier");
				denumireFermier.setValue(fermier.getDENUMIRE_FERMIER());
				denumireFermier.setRequiredIndicatorVisible(true);
				denumireFermier.setErrorMessage("Denumire Fermier required!");

				TextField cnp_cuiFermier = new TextField("CNP/CUI Fermier");
				cnp_cuiFermier.setValue(fermier.getCNP_CUI());
				cnp_cuiFermier.setRequiredIndicatorVisible(true);
				cnp_cuiFermier.setErrorMessage("CNP/CUI required!");

				TextField localitateFermier = new TextField("Localitate Fermier");
				localitateFermier.setValue(fermier.getADRESA_LOCALITATE());
				localitateFermier.setRequiredIndicatorVisible(true);
				localitateFermier.setErrorMessage("Localitate required!");

				TextField stradaFermier = new TextField("Strada Fermier");
				if (fermier.getADRESA_STRADA() != null)
					stradaFermier.setValue(fermier.getADRESA_STRADA());

				TextField nrFermier = new TextField("Numar Adresa Fermier");
				nrFermier.setValue(fermier.getADRESA_NUMAR());
				nrFermier.setRequiredIndicatorVisible(true);
				nrFermier.setErrorMessage("Nr. Adresa required!");

				TextField blocFermier = new TextField("Bloc Fermier");
				if (fermier.getADRESA_BLOC() != null)
					blocFermier.setValue(fermier.getADRESA_BLOC());

				TextField etajFermier = new TextField("Etaj Fermier");
				if (fermier.getADRESA_ETAJ() != null)
					etajFermier.setValue(fermier.getADRESA_ETAJ());

				TextField apartamentFermier = new TextField("Apartament Fermier");
				if (fermier.getADRESA_APARTAMENT() != null)
					apartamentFermier.setValue(fermier.getADRESA_APARTAMENT());

				TextField judetFermier = new TextField("Judet Fermier");
				judetFermier.setValue(fermier.getADRESA_JUDET());
				judetFermier.setRequiredIndicatorVisible(true);
				judetFermier.setErrorMessage("Nr. Adresa required!");

				TextField telefonFermier = new TextField("Telefon Fermier");
				telefonFermier.setValue(fermier.getTELEFON());
				telefonFermier.setRequiredIndicatorVisible(true);
				telefonFermier.setErrorMessage("Telefon required!");

				IntegerField rolFermier = new IntegerField("Rol Fermier");
				if (fermier.getROL() != 0)
					rolFermier.setValue(fermier.getROL());

				TextField volumFermier = new TextField("Volum");
				if (fermier.getVOLUM() != null)
					volumFermier.setValue(fermier.getVOLUM());

				IntegerField pozitieFermier = new IntegerField("Pozitie");
				if (fermier.getPOZITIE() != 0)
					pozitieFermier.setValue(fermier.getPOZITIE());

				ComboBox<String> tipFermier = new ComboBox<String>("Tip Fermier");
				tipFermier.setItems("PF Locala", "PF Externa", "PJ Locala", "PJ Externa");
				tipFermier.setRequiredIndicatorVisible(true);
				tipFermier.setErrorMessage("Tip Fermier required!");
				tipFermier.setValue(fermier.getTIP());

				TextField pf_pjFermier = new TextField("PF/PJ");
				pf_pjFermier.setValue(fermier.getPF_PJ());
				pf_pjFermier.setEnabled(false);

				TextField reprezentantFermier = new TextField("Reprezentant PJ");
				if (fermier.getREPREZENTANT_PJ() != null)
					reprezentantFermier.setValue(fermier.getREPREZENTANT_PJ());

				FileBuffer FileMemoryBuffer = new FileBuffer();
				Upload upload = new Upload(FileMemoryBuffer);
				Button uploadButton = new Button("Upload PDF...");
				uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				upload.setUploadButton(uploadButton);
				upload.setAcceptedFileTypes("application/pdf", ".pdf");
				upload.setMaxFiles(1);
				upload.setDropAllowed(false);
				int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
				upload.setMaxFileSize(maxFileSizeInBytes);
				upload.setMinWidth(15, Unit.EM);

				upload.addSucceededListener(event -> {

					fileData = FileMemoryBuffer.getInputStream();
					String fileName = event.getFileName();

					String mimeType = event.getMIMEType();

					System.out.println(fileName + " " + mimeType);
				});
				upload.addFileRejectedListener(event -> {
					String errorMessage = event.getErrorMessage();

					Notification notification = Notification.show(errorMessage, 5000,
							Notification.Position.BOTTOM_CENTER);
					notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
				});

				upload.getElement().addEventListener("max-files-reached-changed", event -> {
					boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
					uploadButton.setEnabled(!maxFilesReached);
				}).addEventData("event.detail.value");

				FormLayout editLayout = new FormLayout();
				editLayout.setResponsiveSteps(

						new ResponsiveStep("0", 1),

						new ResponsiveStep("500px", 2));

				VerticalLayout dialogEditLayout = new VerticalLayout(editLayout);
				editLayout.add(idFermier, idApia, denumireFermier, cnp_cuiFermier, localitateFermier, stradaFermier,
						nrFermier, blocFermier, etajFermier, apartamentFermier, judetFermier, telefonFermier,
						rolFermier, volumFermier, pozitieFermier, tipFermier, pf_pjFermier, reprezentantFermier,
						upload);
				dialogEditLayout.setAlignItems(Alignment.CENTER);
				dialogEditLayout.setJustifyContentMode(JustifyContentMode.CENTER);
				dialogEditLayout.setPadding(false);
				dialogEditLayout.setSpacing(false);

				editDialog.add(dialogEditLayout);
				Button saveButton = new Button("Update!");
				saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				saveButton.addClickListener(evt -> {
					if (idFermier.getValue() == 0 && fermier.getID_FERMIER() != idFermier.getValue())
						fermier.setID_FERMIER(idFermier.getValue());
					if (!idApia.getValue().isEmpty() && fermier.getID_APIA() != idApia.getValue().trim())
						fermier.setID_APIA(idApia.getValue().trim());
					if (!denumireFermier.getValue().isEmpty()
							&& fermier.getDENUMIRE_FERMIER() != denumireFermier.getValue().trim())
						fermier.setDENUMIRE_FERMIER(denumireFermier.getValue().trim());
					if (!cnp_cuiFermier.getValue().isEmpty()
							&& fermier.getCNP_CUI() != cnp_cuiFermier.getValue().trim())
						fermier.setCNP_CUI(cnp_cuiFermier.getValue().trim());
					if (!localitateFermier.getValue().isEmpty()
							&& fermier.getADRESA_LOCALITATE() != localitateFermier.getValue().trim())
						fermier.setADRESA_LOCALITATE(localitateFermier.getValue().trim());
					if (!stradaFermier.getValue().isEmpty()
							&& fermier.getADRESA_STRADA() != stradaFermier.getValue().trim())
						fermier.setADRESA_STRADA(stradaFermier.getValue().trim());
					if (!nrFermier.getValue().isEmpty() && fermier.getADRESA_NUMAR() != nrFermier.getValue().trim())
						fermier.setADRESA_NUMAR(nrFermier.getValue().trim());
					if (!blocFermier.getValue().isEmpty() && fermier.getADRESA_BLOC() != blocFermier.getValue().trim())
						fermier.setADRESA_BLOC(blocFermier.getValue().trim());
					if (!etajFermier.getValue().isEmpty() && fermier.getADRESA_ETAJ() != etajFermier.getValue().trim())
						fermier.setADRESA_ETAJ(etajFermier.getValue().trim());
					if (!idApia.getValue().isEmpty() && fermier.getID_APIA() != idApia.getValue().trim())
						fermier.setID_APIA(idApia.getValue().trim());
					if (!apartamentFermier.getValue().isEmpty()
							&& fermier.getADRESA_APARTAMENT() != apartamentFermier.getValue().trim())
						fermier.setADRESA_APARTAMENT(apartamentFermier.getValue().trim());
					if (!judetFermier.getValue().isEmpty()
							&& fermier.getADRESA_JUDET() != judetFermier.getValue().trim())
						fermier.setADRESA_JUDET(judetFermier.getValue().trim());
					if (!telefonFermier.getValue().isEmpty()
							&& fermier.getTELEFON() != telefonFermier.getValue().trim())
						fermier.setTELEFON(telefonFermier.getValue().trim());
					if (rolFermier.getValue() == 0 && fermier.getROL() != rolFermier.getValue())
						fermier.setROL(rolFermier.getValue());
					if (!volumFermier.getValue().isEmpty() && fermier.getVOLUM() != volumFermier.getValue().trim())
						fermier.setVOLUM(volumFermier.getValue().trim());
					if (pozitieFermier.getValue() == 0 && fermier.getPOZITIE() != pozitieFermier.getValue())
						fermier.setPOZITIE(pozitieFermier.getValue());
					if (!tipFermier.getValue().isEmpty() && fermier.getTIP() != tipFermier.getValue().trim())
						fermier.setTIP(tipFermier.getValue().trim());
					if (!pf_pjFermier.getValue().isEmpty() && fermier.getPF_PJ() != pf_pjFermier.getValue().trim())
						fermier.setPF_PJ(pf_pjFermier.getValue().trim());
					if (!reprezentantFermier.getValue().isEmpty()
							&& fermier.getREPREZENTANT_PJ() != reprezentantFermier.getValue().trim())
						fermier.setREPREZENTANT_PJ(reprezentantFermier.getValue().trim());
					if (fileData != null) {
						Blob fd = null;
						try {
							fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

								@Override
								public Exception interceptException(Exception sqlEx) {
									System.out.println("Erroare1 la upload PJ!");
									return null;
								}

								@Override
								public ExceptionInterceptor init(Properties props, Log log) {
									System.out.println("Erroare2 la upload PJ!");
									return null;
								}

								@Override
								public void destroy() {
									System.out.println("Erroare3 la upload PJ!");

								}
							});
						} catch (IOException ex) {
							System.out.println(ex.getMessage());
						}

						fermier.setDOSAR(fd);
					}
					System.out.println(fermier.getADRESA_LOCALITATE());
					fermieriRepo.saveAndFlush(fermier);
					target.getDataProvider().refreshAll();
					editDialog.close();

				});
				Button cancelButton = new Button("Cancel", evt -> editDialog.close());
				editDialog.getFooter().add(cancelButton);
				editDialog.getFooter().add(saveButton);
				editDialog.open();

			}));
			addItem("Delete", e -> e.getItem().ifPresent(fermier -> {
				ConfirmDialog deleteDialog = new ConfirmDialog();
				deleteDialog.setHeader("Stergere Fermier");
				deleteDialog.setText("Sigur doriti sa stergeti " + fermier.getDENUMIRE_FERMIER() + " ?");
				deleteDialog.setCancelable(true);
				// deleteDialog.setRejectable(true);
				deleteDialog.setConfirmText("Delete");
				deleteDialog.setConfirmButtonTheme("error primary");
				deleteDialog.addConfirmListener(event -> {
					fermieriRepo.delete(fermier);
					target.getDataProvider().refreshAll();
					this.close();
				});
				deleteDialog.open();

			}));

			add(new Hr());

			GridMenuItem<FERMIERI> telefonItem = addItem("Telefon", e -> e.getItem().ifPresent(fermier -> {
				UI.getCurrent().getPage().executeJs("navigator.clipboard.writeText(" + fermier.getTELEFON() + ")");
			}));
			GridMenuItem<FERMIERI> CNPItem = addItem("CNP", e -> e.getItem().ifPresent(fermier -> {

				UI.getCurrent().getPage()
						.executeJs("navigator.clipboard.writeText(String.raw`" + fermier.getCNP_CUI() + "`)");
			}));

			setDynamicContentHandler(fermier -> {
				// Do not show context menu when header is clicked
				if (fermier == null)
					return false;
				telefonItem.setText(String.format("Telefon: %s", fermier.getTELEFON()));
				CNPItem.setText(String.format("CNP: %s", fermier.getCNP_CUI()));
				return true;
			});
		}

	}
}
