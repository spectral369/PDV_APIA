package com.spectral369.views.rapoarte;

import com.spectral369.db.contracte.ContracteRepository;
import com.spectral369.db.fermieri.FermieriRepository;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.spectral369.utils.Utils;
import com.spectral369.views.MainLayout;
import com.spectral369.views.home.HomeView;
import com.spectral369.views.rapoarte.Apia1Raport.RaportApia;
import com.spectral369.views.rapoarte.contracte.RaportSimpluContracte;
import com.spectral369.views.rapoarte.fermieri.RaportSimpluFermieri;
import com.spectral369.views.rapoarte.proprietari.RaportSimpluProprietari;
import com.spectral369.views.rapoarte.terenuri.RaportSimpluTerenuri;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Rapoarte")
@Route(value = "rapoarte", layout = MainLayout.class)
public class RapoarteView extends VerticalLayout {

	private static final long serialVersionUID = 3211226971477590084L;
	private HorizontalLayout content = null;

	public RapoarteView(FermieriRepository fermierRepo, ProprietariRepository proprietariRepo,
			TerenuriRepository terenuriRepo, ContracteRepository contracteRepo) {
		setSpacing(false);
		HorizontalLayout topButtons = new HorizontalLayout();

		Button raportFermieriSimplu = new Button("Raport Fermieri Simplu");
		raportFermieriSimplu.addClickListener(event -> {
			adaugaFermierImpl(event, fermierRepo);
		});
		raportFermieriSimplu.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		Button raportProprietariSimplu = new Button("Raport Proprietari Simplu");
		raportProprietariSimplu.addClickListener(event -> {
			adaugaProprietariImpl(event, proprietariRepo);
		});
		raportProprietariSimplu.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		Button raportTerenuriSimplu = new Button("Raport Terenuri Simplu");
		raportTerenuriSimplu.addClickListener(event -> {
			adaugaTerenuriImpl(event, terenuriRepo);
		});
		raportTerenuriSimplu.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		Button raportContracteSimplu = new Button("Raport Contracte Simplu");
		raportContracteSimplu.addClickListener(event -> {
			adaugaContracteImpl(event, contracteRepo, fermierRepo, proprietariRepo, terenuriRepo);
		});
		raportContracteSimplu.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		Button raportApiaSimplu = new Button("Raport Apia 1");
		raportApiaSimplu.addClickListener(event -> {
			raportApiaImpl(event, contracteRepo, fermierRepo, proprietariRepo, terenuriRepo);
		});
		raportApiaSimplu.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		topButtons.add(raportFermieriSimplu, raportProprietariSimplu, raportTerenuriSimplu, raportContracteSimplu,
				raportApiaSimplu);
		topButtons.setAlignItems(Alignment.CENTER);
		add(topButtons);

		content = new HorizontalLayout();
		content.setSizeFull();
		add(content);
		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private void raportApiaImpl(ClickEvent<Button> event, ContracteRepository contracteRepo,
			FermieriRepository fermierRepo, ProprietariRepository proprietariRepo, TerenuriRepository terenuriRepo) {
		if (Utils.CODE < 2) {
			if (content.getComponentCount() > 0) {
				content.removeAll();
				content.add(new RaportApia(proprietariRepo, fermierRepo, terenuriRepo, contracteRepo));
			} else {

				content.add(new RaportApia(proprietariRepo, fermierRepo, terenuriRepo, contracteRepo));
			}
		} else {
			UI.getCurrent().navigate(HomeView.class);
		}

	}

	private void adaugaContracteImpl(ClickEvent<Button> event, ContracteRepository contracteRepo,
			FermieriRepository fermierRepo, ProprietariRepository proprietariRepo, TerenuriRepository terenuriRepo2) {
		if (content.getComponentCount() > 0) {
			content.removeAll();
			content.add(new RaportSimpluContracte(contracteRepo, proprietariRepo, terenuriRepo2, fermierRepo));
		} else {

			content.add(new RaportSimpluContracte(contracteRepo, proprietariRepo, terenuriRepo2, fermierRepo));
		}

	}

	private void adaugaTerenuriImpl(ClickEvent<Button> event, TerenuriRepository terenuriRepo) {
		if (content.getComponentCount() > 0) {
			content.removeAll();
			content.add(new RaportSimpluTerenuri(terenuriRepo));
		} else {

			content.add(new RaportSimpluTerenuri(terenuriRepo));
		}

	}

	private void adaugaProprietariImpl(ClickEvent<Button> event, ProprietariRepository proprietariRepo) {
		if (content.getComponentCount() > 0) {
			content.removeAll();
			content.add(new RaportSimpluProprietari(proprietariRepo));
		} else {

			content.add(new RaportSimpluProprietari(proprietariRepo));
		}
	}

	private void adaugaFermierImpl(ClickEvent<Button> event, FermieriRepository fermierRepo) {
		if (content.getComponentCount() > 0) {
			content.removeAll();
			content.add(new RaportSimpluFermieri(fermierRepo));
		} else {

			content.add(new RaportSimpluFermieri(fermierRepo));
		}

	}

}
