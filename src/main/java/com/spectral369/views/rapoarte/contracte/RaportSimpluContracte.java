package com.spectral369.views.rapoarte.contracte;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.contracte.CONTRACTE;
import com.spectral369.db.contracte.ContracteRepository;
import com.spectral369.db.fermieri.FERMIERI;
import com.spectral369.db.fermieri.FermieriRepository;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.spectral369.db.terenuri.TERENURI;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.spectral369.utils.Utils;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.lumo.LumoUtility;

@CssImport("./themes/pdvapia/components/vaadin-grid.css")

public class RaportSimpluContracte extends VerticalLayout {

	private static final long serialVersionUID = 2578351231621597521L;

	public RaportSimpluContracte(ContracteRepository contracteRepo, ProprietariRepository proprietariRepo,
			TerenuriRepository terenuriRepo, FermieriRepository fermierRepo) {
		H2 title = new H2("Raport Simplu Contracte");
		add(title);
		List<CONTRACTE> list = contracteRepo.findAll();
		Grid<CONTRACTE> contracteGrid = new Grid<CONTRACTE>(CONTRACTE.class, false);
		contracteGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
		contracteGrid.setDetailsVisibleOnClick(false);
		Column<CONTRACTE> DenumireProprietarColumn = contracteGrid.addColumn(RaportSimpluContracte::generateProprietari)
				.setTooltipGenerator(RaportSimpluContracte::generateProprietari).setFlexGrow(0)
				.setFooter(String.format("Nr. Total Contracte %s", list.size())).setAutoWidth(true).setResizable(false);

		Grid.Column<CONTRACTE> DenumireFermierColumn = contracteGrid
				.addColumn(fermier -> fermier.getFERMIER().getDENUMIRE_FERMIER())
				.setTooltipGenerator(contract -> contract.getFERMIER().getCNP_CUI()).setAutoWidth(true).setFlexGrow(0);
		Grid.Column<CONTRACTE> TerenuriColumn = contracteGrid.addColumn(contract -> {

			return generateTerenuri(contract);
		}).setTooltipGenerator(RaportSimpluContracte::generateTerenuri).setAutoWidth(true);

		Grid.Column<CONTRACTE> DataContractColumn = contracteGrid
				.addColumn(RaportSimpluContracte::formatDate_Data_Contract);
		Grid.Column<CONTRACTE> ValabilitateContractColumn = contracteGrid.addColumn(CONTRACTE::getVALABILITATE_CONTRACT)
				.setTooltipGenerator(
						contract -> contract.getDATA_SFARSIT_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER));
		Grid.Column<CONTRACTE> NumarContractFermierColumn = contracteGrid
				.addColumn(CONTRACTE::getNUMAR_CONTRACT_FERMIER).setTooltipGenerator(contract -> {
					return "Nr. Contract Fermier:" + String.valueOf(contract.getNUMAR_CONTRACT_FERMIER());
				});
		Grid.Column<CONTRACTE> DataContractFermierColumn = contracteGrid
				.addColumn(RaportSimpluContracte::formatDate_Data_Contract_Fermier);
		Grid.Column<CONTRACTE> DataInceputContractColumn = contracteGrid
				.addColumn(RaportSimpluContracte::formatDate_Data_Inceput_Contract);
		Grid.Column<CONTRACTE> PerioadaContractColumn = contracteGrid.addColumn(CONTRACTE::getPERIOADA_CONTRACT)
				.setTooltipGenerator(contract -> contract.getDATA_SFARSIT_CONTRACT().toString());
		Column<CONTRACTE> details = contracteGrid.addColumn(createToggleDetailsRenderer(contracteGrid))
				.setAutoWidth(true);
		Column<CONTRACTE> down = contracteGrid.addColumn(createActionRenderer()).setFrozenToEnd(true).setAutoWidth(true)
				.setFlexGrow(0);

		GridListDataView<CONTRACTE> dataView = contracteGrid.setItems(list);

		contractFilter contractFilter = new contractFilter(dataView);

		contracteGrid.getHeaderRows().clear();
		HeaderRow headerRow = contracteGrid.appendHeaderRow();

		headerRow.getCell(DenumireProprietarColumn)
				.setComponent(createFilterHeader("Denumire Proprietari", contractFilter::setDenumireProprietari));
		headerRow.getCell(DenumireFermierColumn)
				.setComponent(createFilterHeader("Denumire Fermier", contractFilter::setDenumireFermier));
		headerRow.getCell(TerenuriColumn).setComponent(createFilterHeader("Terenuri", contractFilter::setTerenuri));
		headerRow.getCell(DataContractColumn)
				.setComponent(createFilterHeader("Data Contract", contractFilter::setDataContract));
		headerRow.getCell(ValabilitateContractColumn)
				.setComponent(createFilterHeader("Valabilitate Contract", contractFilter::setValabilitateContract));
		headerRow.getCell(NumarContractFermierColumn)
				.setComponent(createFilterHeader("Nr. Contract Fermier", contractFilter::setNumarContractFermier));
		headerRow.getCell(DataContractFermierColumn)
				.setComponent(createFilterHeader("Data Contract Fermier", contractFilter::setDataContractFermier));
		headerRow.getCell(DataInceputContractColumn)
				.setComponent(createFilterHeader("Data Inceput", contractFilter::setInceputContract));
		headerRow.getCell(PerioadaContractColumn)
				.setComponent(createFilterHeader("Perioada", contractFilter::setPerioadaContract));
		headerRow.getCell(details).setComponent(new Span("Detalii"));
		headerRow.getCell(down).setComponent(new Span("Download"));
		contracteGrid.setItemDetailsRenderer(createContractDetailsRenderer());
		new contractContextMenu(contracteGrid, contracteRepo, proprietariRepo, terenuriRepo, fermierRepo);

		add(contracteGrid);
		contracteGrid.getColumns().get(2).setFooter(createTerenSupTotalFooterText(generateTerenuriSupUtil(list)));

		contracteGrid.setPartNameGenerator(contract -> {
			LocalDate st = contract.getDATA_INCEPUT_CONTRACT().toLocalDate();
			st.format(DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate sf = contract.getDATA_SFARSIT_CONTRACT().toLocalDate();
			sf.format(DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate now = LocalDate.now();
			boolean df = (now.isAfter(st) && now.isBefore(sf));
			if ((df || (now.isEqual(st) && now.isBefore(sf))) && !contract.getINCETAT()) {
				return "valabill spacewrap";

			} else {
				return "expiratt spacewrap";
			}

		});

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private static String formatDate_Data_Contract(CONTRACTE contract) {
		return contract.getDATA_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER);
	}

	private static String formatDate_Data_Contract_Fermier(CONTRACTE contract) {
		return contract.getDATA_CONTRACT_FERMIER().toLocalDate().format(Utils.DATEFORMATTER);
	}

	private static String formatDate_Data_Inceput_Contract(CONTRACTE contract) {
		return contract.getDATA_INCEPUT_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER);
	}

	private static String createTerenSupTotalFooterText(double str) {

		return String.format("Total:%s ha", str / 10000);
	}

	private static String generateProprietari(CONTRACTE contract) {
		StringBuilder sb = new StringBuilder();
		for (PROPRIETARI p : contract.getProprietarID()) {
			sb.append(p.getNumeProprietar());
			sb.append("\n");
		}
		return sb.toString();
	}

	private static String generateTerenuri(CONTRACTE contract) {
		StringBuilder sb = new StringBuilder();
		for (TERENURI t : contract.getID_TEREN()) {
			sb.append(t.getTarPar());
			sb.append("\n");

		}
		return sb.toString();
	}

	private static double generateTerenuriSupUtil(List<CONTRACTE> contracts) {

		double supTotal = 0;
		for (CONTRACTE c : contracts)
			for (TERENURI t : c.getID_TEREN()) {

				supTotal += t.getSuprafataUtilizata();
			}
		return supTotal;
	}

	private static ComponentRenderer<contractDetailsFormLayout, CONTRACTE> createContractDetailsRenderer() {
		return new ComponentRenderer<>(contractDetailsFormLayout::new, contractDetailsFormLayout::setcontract);
	}

	private static Renderer<CONTRACTE> createToggleDetailsRenderer(Grid<CONTRACTE> grid) {
		return LitRenderer
				.<CONTRACTE>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Detalii</vaadin-button>")
				.withFunction("handleClick",
						contracti -> grid.setDetailsVisible(contracti, !grid.isDetailsVisible(contracti)));
	}

	private static Renderer<CONTRACTE> createActionRenderer() {
		return LitRenderer
				.<CONTRACTE>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Download</vaadin-button>")
				.withFunction("handleClick", t -> {
					if (t.getDOSAR() != null) {

						java.sql.Blob dosar = t.getDOSAR();
						try {
							InputStream is = dosar.getBinaryStream();

							File targetFile = new File(
									System.getProperty("user.home") + File.separatorChar + "dosar.pdf");
							OutputStream outStream = new FileOutputStream(targetFile);

							byte[] buffer = new byte[8 * 1024];
							int bytesRead;
							while ((bytesRead = is.read(buffer)) != -1) {
								outStream.write(buffer, 0, bytesRead);
							}
							IOUtils.closeQuietly(is);
							IOUtils.closeQuietly(outStream);

							final StreamResource resource = new StreamResource(targetFile.getName(), () -> {
								try {
									return new ByteArrayInputStream(FileUtils.readFileToByteArray(targetFile));
								} catch (IOException e) {
									System.out.println(e.getLocalizedMessage());
								}
								return is;
							});

							final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry()
									.registerResource(resource);
							UI.getCurrent().getPage().open(registration.getResourceUri().toString());

						} catch (Exception e) {
							System.out.println(e.getLocalizedMessage());
						}
					} else {
						Notification.show("Nothing to download", 2500, Position.BOTTOM_END);
					}
				});

	}

	private static Component createFilterHeader(String labelText, Consumer<String> filterChangeConsumer) {
		NativeLabel label = new NativeLabel(labelText);
		label.getStyle().set("padding-top", "var(--lumo-space-m)").set("font-size", "var(--lumo-font-size-xs)");
		TextField textField = new TextField();
		textField.setValueChangeMode(ValueChangeMode.EAGER);
		textField.setClearButtonVisible(true);
		textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
		textField.setWidthFull();
		textField.getStyle().set("max-width", "100%");
		textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
		VerticalLayout layout = new VerticalLayout(label, textField);
		layout.getThemeList().clear();
		layout.getThemeList().add("spacing-xs");

		return layout;
	}

	private static class contractFilter {
		private final GridListDataView<CONTRACTE> dataView;

		private String DenumireProprietari;
		private String DenumireFermier;
		private String Terenuri;
		private String DataContract;
		private String ValabilitateContract;
		private int NumarContractFermier;
		private String DataContractFermier;
		private String InceputContract;
		private String PerioadaContract;

		public contractFilter(GridListDataView<CONTRACTE> dataView) {
			this.dataView = dataView;
			this.dataView.addFilter(this::test);
		}

		public void setDenumireProprietari(String denumireProprietari) {
			this.DenumireProprietari = denumireProprietari;
			this.dataView.refreshAll();
		}

		public void setDenumireFermier(String denumireFermier) {
			this.DenumireFermier = denumireFermier;
			this.dataView.refreshAll();
		}

		public void setTerenuri(String terenuri) {
			this.Terenuri = terenuri;
			this.dataView.refreshAll();
		}

		public void setDataContract(String dataContract) {
			this.DataContract = dataContract;
			this.dataView.refreshAll();
		}

		public void setValabilitateContract(String valabilitateContract) {
			this.ValabilitateContract = valabilitateContract;
			this.dataView.refreshAll();
		}

		public void setNumarContractFermier(String numarContractFermier) {
			try {
				this.NumarContractFermier = Integer.parseInt(numarContractFermier);
			} catch (Exception e) {
				this.NumarContractFermier = 0;
			}
			this.dataView.refreshAll();
		}

		public void setDataContractFermier(String dataContractFermier) {
			this.DataContractFermier = dataContractFermier;
			this.dataView.refreshAll();
		}

		public void setInceputContract(String inceputContract) {
			this.InceputContract = inceputContract;
			this.dataView.refreshAll();
		}

		public void setPerioadaContract(String perioadaContract) {
			this.PerioadaContract = perioadaContract;
			this.dataView.refreshAll();
		}

		public boolean test(CONTRACTE contract) {
			LocalDate st = contract.getDATA_INCEPUT_CONTRACT().toLocalDate();
			st.format(DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate sf = contract.getDATA_SFARSIT_CONTRACT().toLocalDate();
			sf.format(DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate now = LocalDate.now();
			boolean df = (now.isAfter(st) && now.isBefore(sf));
			String valabilitate;
			if (df || (now.isEqual(st) && now.isBefore(sf))) {
				valabilitate = "Valabil";

			} else {
				valabilitate = "Expirat";
			}

			boolean matchesDenumireProprietari = matchesProprietari(contract.getProprietarID(), DenumireProprietari);
			boolean matchesDenumireFermier = matchesFermieri(contract.getFERMIER(), DenumireFermier);
			boolean matchesTerenuri = matchesTerenuri(contract.getID_TEREN(), Terenuri);
			boolean matchesDataContract = matches(contract.getDATA_CONTRACT().toString(), DataContract);
			boolean matchesValabilitateContract = matches(valabilitate, ValabilitateContract);
			boolean matchesNumarContractFermier = matchesInt(contract.getNUMAR_CONTRACT_FERMIER(),
					NumarContractFermier);
			boolean matchesDataContractFermier = matches(contract.getDATA_CONTRACT_FERMIER().toString(),
					DataContractFermier);
			boolean matchesInceputContract = matches(contract.getDATA_INCEPUT_CONTRACT().toString(), InceputContract);
			boolean matchesPerioadaContract = matches(contract.getPERIOADA_CONTRACT(), PerioadaContract);

			return matchesDenumireProprietari && matchesDenumireFermier && matchesTerenuri && matchesDataContract
					&& matchesValabilitateContract && matchesNumarContractFermier && matchesDataContractFermier
					&& matchesInceputContract && matchesPerioadaContract;
		}

		private boolean matchesProprietari(List<PROPRIETARI> value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.get(0).getNumeProprietar().toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesTerenuri(List<TERENURI> value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.get(0).getTarPar().toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesFermieri(FERMIERI value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.getDENUMIRE_FERMIER().toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matches(String value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesInt(int value, int searchTerm) {

			return searchTerm == 0 || value == 0
					|| String.valueOf(value).toLowerCase().contains(String.valueOf(searchTerm));
		}
	}

	private static class contractDetailsFormLayout extends FormLayout {
		private static final long serialVersionUID = 6425872388237011077L;
		private final IntegerField ID = new IntegerField("ID");
		private final ListBox<String> Proprietari = new ListBox<String>();
		private final IntegerField NumarContract = new IntegerField("Numar Contract");
		private final TextField DataContract = new TextField("Data Contract");
		private final TextField ValabilitateContract = new TextField("Valabilitate Contract");
		private final TextField Fermier = new TextField("Fermier");
		private final IntegerField NumarContractFermier = new IntegerField("Numar Contract Fermier");
		private final TextField DataContractFermier = new TextField("Data Contract Fermier");
		private final ListBox<String> Terenuri = new ListBox<String>();
		private final TextField totalSupContract = new TextField("Total Suprafata Contract");
		private final TextField TipContract = new TextField("Tip Contract");
		private final TextField DataInceputContract = new TextField("Data Inceput Contract");
		private final TextField DataSfarsitContract = new TextField("Data Sfarsit Contract");
		private final TextField PerioadaContract = new TextField("Perioada Contract");

		public contractDetailsFormLayout() {

			add(ID, Proprietari, NumarContract, DataContract, ValabilitateContract, Fermier, NumarContractFermier,
					DataContractFermier, Terenuri, totalSupContract, TipContract, DataInceputContract,
					DataSfarsitContract, PerioadaContract);
			Proprietari.setReadOnly(true);
			NumarContract.setReadOnly(true);
			DataContract.setReadOnly(true);
			ValabilitateContract.setReadOnly(true);
			Fermier.setReadOnly(true);
			Fermier.addThemeVariants(TextFieldVariant.LUMO_ALIGN_CENTER);
			NumarContractFermier.setReadOnly(true);
			DataContractFermier.setReadOnly(true);
			Terenuri.setReadOnly(true);
			totalSupContract.setReadOnly(true);
			totalSupContract.addThemeVariants(TextFieldVariant.LUMO_ALIGN_CENTER);
			TipContract.setReadOnly(true);
			DataInceputContract.setReadOnly(true);
			DataSfarsitContract.setReadOnly(true);
			PerioadaContract.setReadOnly(true);
			PerioadaContract.addThemeVariants(TextFieldVariant.LUMO_ALIGN_CENTER);
			setResponsiveSteps(new ResponsiveStep("0", 3));
			this.setColspan(ID, 3);
			this.setColspan(Proprietari, 3);
			this.setColspan(Fermier, 3);
			this.setColspan(Terenuri, 3);
			this.setColspan(totalSupContract, 3);
			this.setColspan(PerioadaContract, 3);

			ID.setEnabled(false);
			ID.addThemeVariants(TextFieldVariant.LUMO_ALIGN_CENTER);

		}

		public void setcontract(final CONTRACTE contract) {
			ID.setValue(contract.getID());
			List<String> propList = new LinkedList<>();
			for (PROPRIETARI prop : contract.getProprietarID())
				propList.add(prop.getNumeProprietar() + " CNP: " + prop.getCNP_CUI() + " Localitate "
						+ prop.getADRESA_LOCALITATE() + " Nr. " + prop.getADRESA_NUMAR());
			Proprietari.setItems(propList);
			NumarContract.setValue(contract.getNUMAR_CONTRACT());
			NumarContract.setReadOnly(true);
			DataContract.setValue(contract.getDATA_CONTRACT().toString());
			// ValabilitateContract.setValue(contract.getVALABILITATE_CONTRACT());
			Fermier.setValue(contract.getFERMIER().getDENUMIRE_FERMIER() + " CNP/CUI: "
					+ contract.getFERMIER().getCNP_CUI() + "" + " Localitate: "
					+ contract.getFERMIER().getADRESA_LOCALITATE() + " Nr. " + contract.getFERMIER().getADRESA_NUMAR()
					+ "" + " ID APIA: " + contract.getFERMIER().getID_APIA());

			NumarContractFermier.setValue(contract.getNUMAR_CONTRACT_FERMIER());

			DataContractFermier.setValue(contract.getDATA_CONTRACT_FERMIER().toLocalDate().format(Utils.DATEFORMATTER));
			double totalSupT = 0;
			List<String> terenuri = new LinkedList<>();
			for (TERENURI teren : contract.getID_TEREN()) {
				terenuri.add(teren.getTarPar() + " Sup.Util: " + teren.getSuprafataUtilizata() + "" + " Cota: "
						+ teren.getCota());
				totalSupT += teren.getSuprafataUtilizata();
			}
			Terenuri.setItems(terenuri);
			totalSupContract
					.setValue(String.valueOf(totalSupT) + " m\u00B2 - " + String.valueOf(totalSupT / 10000) + " ha");
			TipContract.setValue(contract.getTIP_CONTRACT());
			DataInceputContract.setValue(contract.getDATA_INCEPUT_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER));
			DataSfarsitContract.setValue(contract.getDATA_SFARSIT_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER));
			PerioadaContract.setValue(contract.getPERIOADA_CONTRACT());
		}

	}

	private static class contractContextMenu extends GridContextMenu<CONTRACTE> {
		private static final long serialVersionUID = 8883017315048714541L;
		private InputStream fileData;
		private List<PROPRIETARI> proprietar = new LinkedList<>();
		private FERMIERI fermier;
		private List<TERENURI> teren;
		private Grid<TERENURI> gridtpcfsup;
		private Grid<PROPRIETARI> gridprops;
		private Grid<FERMIERI> gridferms;
		DatePicker.DatePickerI18n singleFormatI18n = null;

		public contractContextMenu(Grid<CONTRACTE> target, ContracteRepository contracteRepo,
				ProprietariRepository proprietariRepo, TerenuriRepository terenuriRepo,
				FermieriRepository fermierRepo) {
			super(target);
			singleFormatI18n = new DatePicker.DatePickerI18n();
			singleFormatI18n.setDateFormat(Utils.DATEFORMATTER.toString());

			addItem("Edit", e -> e.getItem().ifPresent(contract -> {

				gridprops = new Grid<PROPRIETARI>(PROPRIETARI.class);
				gridprops.setMaxHeight("15vh");
				gridprops.removeAllColumns();
				gridprops.addColumn(PROPRIETARI::getNumeProprietar).setHeader("Proprietari");
				gridprops.setItems(contract.getProprietarID());
				proprietar.addAll(contract.getProprietarID());

				gridferms = new Grid<FERMIERI>(FERMIERI.class);
				gridferms.setMaxHeight("15vh");
				gridferms.removeAllColumns();
				gridferms.addColumn(FERMIERI::getDENUMIRE_FERMIER).setHeader("Fermieri");
				gridferms.setItems(contract.getFERMIER());

				gridtpcfsup = new Grid<TERENURI>(TERENURI.class);
				gridtpcfsup.setMaxHeight("15vh");
				gridtpcfsup.removeAllColumns();
				gridtpcfsup.addColumn(TERENURI::getTarPar).setHeader("Terenuri");
				gridtpcfsup.setItems(contract.getID_TEREN());

				Dialog editDialog = new Dialog();
				editDialog.setHeaderTitle("Update Contract");

				IntegerField NumarContract = new IntegerField("Numar Contract");
				NumarContract.setValue(contract.getID());
				NumarContract.setRequiredIndicatorVisible(true);
				NumarContract.setReadOnly(true);
				NumarContract.setErrorMessage("Numar Contract required!");

				DatePicker dataContract = new DatePicker("Data Contract");
				dataContract.setI18n(singleFormatI18n);
				dataContract.setValue(contract.getDATA_CONTRACT().toLocalDate());

				TextField ValabilitateContract = new TextField("Valabilitate Contract");
				ValabilitateContract.setValue(contract.getVALABILITATE_CONTRACT());
				ValabilitateContract.setRequiredIndicatorVisible(true);
				ValabilitateContract.setReadOnly(true);
				ValabilitateContract.setErrorMessage("Vlabilitate Contract required!");

				Button searchProprietar = new Button("Cauta Proprietar", VaadinIcon.SEARCH.create());
				searchProprietar.addClickListener(evt -> {
					Dialog dialog = new Dialog();

					dialog.setHeaderTitle("Alege proprietar");

					VerticalLayout contentLayout = new VerticalLayout();
					contentLayout.setAlignItems(Alignment.CENTER);
					contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

					HorizontalLayout searchLayout = new HorizontalLayout();
					searchLayout.setAlignItems(Alignment.CENTER);
					searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
					TextField searchField = new TextField("Cauta CNP/CUI");
					searchField.setPlaceholder("CNP sau CUI");
					searchField.setRequiredIndicatorVisible(true);
					searchField.setErrorMessage("CNP/CUI Required !");
					searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
					searchField.setValueChangeTimeout(1000);
					searchField.setWidth("100%");
					searchField.setMaxLength(14);

					MultiSelectListBox<String> listbox = new MultiSelectListBox<String>();

					List<PROPRIETARI> props = proprietariRepo.findAll();
					List<String> numep = new ArrayList<String>();
					for (PROPRIETARI p : props) {
						numep.add(p.getID() + ":" + p.getNumeProprietar());

					}
					listbox.setItems(numep);

					Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
					searchbtn.setEnabled(false);
					searchbtn.addClickListener(event -> {
						listbox.clear();
						numep.clear();
						List<PROPRIETARI> propByCnpCui = proprietariRepo
								.findByCNPCUIContainingIgnoreCase(searchField.getValue().trim());
						for (PROPRIETARI p : propByCnpCui) {
							numep.add(p.getID() + ":" + p.getNumeProprietar());
						}
						listbox.setItems(numep);

					});

					searchLayout.add(searchField, searchbtn);
					contentLayout.add(searchLayout, listbox);
					dialog.add(contentLayout);
					Button ok = new Button("OK", (en) -> {
						Set<String> lpids = listbox.getValue();
						Set<Integer> pids = new HashSet<Integer>();
						proprietar.clear();
						for (String lp : lpids) {

							int idss = Integer.parseInt(lp.substring(0, lp.indexOf(":")));
							pids.add(idss);

							proprietar.add(proprietariRepo.findProprietarByID(idss));
						}

						gridprops.setItems(proprietar);
						gridprops.select(proprietar.get(0));
						dialog.close();

					});
					ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
					ok.getStyle().set("margin-right", "auto");
					dialog.getFooter().add(ok);
					Button cancel = new Button("Cancel", (ex) -> {
						dialog.close();

					});
					cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

					dialog.getFooter().add(cancel);
					dialog.setModal(true);
					dialog.setCloseOnEsc(true);
					dialog.setCloseOnOutsideClick(false);
					UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
						dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
						dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

					});
					searchField.addValueChangeListener(vcl -> {
						if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
							searchbtn.setEnabled(true);
						} else {
							searchbtn.setEnabled(false);
						}
					});

					dialog.open();

				});

				// end proprietar search

				// alege fermier
				Button searchFermier = new Button("Cauta Fermier", VaadinIcon.SEARCH.create());
				searchFermier.addClickListener(evt -> {
					Dialog dialog = new Dialog();

					dialog.setHeaderTitle("Alege Fermier");

					VerticalLayout contentLayout = new VerticalLayout();
					contentLayout.setAlignItems(Alignment.CENTER);
					contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

					HorizontalLayout searchLayout = new HorizontalLayout();
					searchLayout.setAlignItems(Alignment.CENTER);
					searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
					TextField searchField = new TextField("Cauta CNP/CUI");
					searchField.setPlaceholder("CNP sau CUI");
					searchField.setRequiredIndicatorVisible(true);
					searchField.setErrorMessage("CNP/CUI Required !");
					searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
					searchField.setValueChangeTimeout(1000);
					searchField.setWidth("100%");
					searchField.setMaxLength(14);

					ListBox<String> listbox = new ListBox<String>();

					List<FERMIERI> ferms = fermierRepo.findAll();
					List<String> fermsp = new ArrayList<String>();
					for (FERMIERI f : ferms) {
						fermsp.add(f.getID() + ":" + f.getDENUMIRE_FERMIER());

					}

					listbox.setItems(fermsp.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList()));

					Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
					searchbtn.setEnabled(false);
					searchbtn.addClickListener(event -> {
						listbox.clear();
						fermsp.clear();
						List<FERMIERI> fermsByCnpCui = fermierRepo
								.findByCNP_CUIContainingIgnoreCase(searchField.getValue().trim());
						for (FERMIERI p : fermsByCnpCui) {
							fermsp.add(p.getID() + ":" + p.getDENUMIRE_FERMIER());
						}
						listbox.setItems(fermsp);

					});

					searchLayout.add(searchField, searchbtn);
					contentLayout.add(searchLayout, listbox);
					dialog.add(contentLayout);
					Button ok = new Button("OK", (en) -> {
						int pid = Integer.parseInt(listbox.getValue().substring(0, listbox.getValue().indexOf(":")));
						fermier = fermierRepo.findByIDIgnoreCase(pid);
						gridferms.setItems(fermier);
						dialog.close();

					});
					ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
					ok.getStyle().set("margin-right", "auto");
					dialog.getFooter().add(ok);
					Button cancel = new Button("Cancel", (ex) -> {
						dialog.close();

					});
					cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

					dialog.getFooter().add(cancel);
					dialog.setModal(true);
					dialog.setCloseOnEsc(true);
					dialog.setCloseOnOutsideClick(false);
					UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
						dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
						dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

					});
					searchField.addValueChangeListener(vcl -> {
						if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
							searchbtn.setEnabled(true);
						} else {
							searchbtn.setEnabled(false);
						}
					});

					dialog.open();

				});
				// end alege fermier

				IntegerField NumarContractFermier = new IntegerField("Numar Contract Fermier");
				NumarContractFermier.setValue(contract.getNUMAR_CONTRACT_FERMIER());

				DatePicker DataContractFermier = new DatePicker("Data Contract Fermier");
				DataContractFermier.setI18n(singleFormatI18n);
				DataContractFermier.setValue(contract.getDATA_CONTRACT_FERMIER().toLocalDate());
				DataContractFermier.setRequiredIndicatorVisible(true);
				DataContractFermier.setErrorMessage("Nr. Adresa required!");

				Button searchTeren = new Button("Cauta Teren", VaadinIcon.SEARCH.create());
				searchTeren.addClickListener(evt -> {
					Dialog dialog = new Dialog();

					dialog.setHeaderTitle("Alege Teren");

					VerticalLayout contentLayout = new VerticalLayout();
					contentLayout.setAlignItems(Alignment.CENTER);
					contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

					HorizontalLayout searchLayout = new HorizontalLayout();
					searchLayout.setAlignItems(Alignment.CENTER);
					searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
					TextField searchField = new TextField("Cauta Tarla/parcela");
					searchField.setPlaceholder("Tarla/Parcela");
					searchField.setRequiredIndicatorVisible(true);
					searchField.setErrorMessage("Tar/Par Required !");
					searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
					searchField.setValueChangeTimeout(1000);
					searchField.setWidth("100%");
					searchField.setMaxLength(14);

					MultiSelectListBox<String> listbox = new MultiSelectListBox<String>();
					if (proprietar == null)
						return;

					List<String> terens = new ArrayList<String>();

					for (PROPRIETARI idprop : proprietar) {
						List<TERENURI> tere = terenuriRepo.findTerenFromProp2(idprop);
						for (TERENURI tr : tere) {
							terens.add(tr.getId() + ":" + tr.getTarPar() + " " + tr.getCF() + " "
									+ tr.getSuprafataUtilizata());
						}

					}

					listbox.setItems(terens);

					Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
					searchbtn.setEnabled(false);
					searchbtn.addClickListener(event -> {
						listbox.clear();
						terens.clear();

						for (PROPRIETARI pr : proprietar) {
							List<TERENURI> terenuriByID = terenuriRepo.findTerenFromPropAndTarPar(pr,
									searchField.getValue().trim());
							for (TERENURI tr : terenuriByID) {
								terens.add(tr.getId() + ":" + tr.getTarPar() + " " + tr.getCF() + " "
										+ tr.getSuprafataUtilizata());
							}
						}

						listbox.setItems(terens);

					});

					searchLayout.add(searchField, searchbtn);
					contentLayout.add(searchLayout, listbox);
					dialog.add(contentLayout);
					Button ok = new Button("OK", (en) -> {
						Set<String> lpids = listbox.getValue();
						Set<Integer> pids = new HashSet<Integer>();
						teren = new LinkedList<>();
						for (String lp : lpids) {
							int idss = Integer.parseInt(lp.substring(0, lp.indexOf(":")));
							pids.add(idss);
							teren.add(terenuriRepo.findByIDIgnoreCase(idss));
						}

						gridtpcfsup.setItems(teren);

						dialog.close();

					});
					ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
					ok.getStyle().set("margin-right", "auto");
					dialog.getFooter().add(ok);
					Button cancel = new Button("Cancel", (ex) -> {
						dialog.close();

					});
					cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

					dialog.getFooter().add(cancel);
					dialog.setModal(true);
					dialog.setCloseOnEsc(true);
					dialog.setCloseOnOutsideClick(false);
					UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
						dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
						dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

					});
					searchField.addValueChangeListener(vcl -> {
						if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
							searchbtn.setEnabled(true);
						} else {
							searchbtn.setEnabled(false);
						}
					});

					dialog.open();

				});

				ComboBox<String> tipContract = new ComboBox<String>("Tip Contract");
				tipContract.setItems("Contract de arendare", "Contract de comodat", "Declaratie", "Schimb lucrativ");
				tipContract.setValue(contract.getTIP_CONTRACT());
				tipContract.setPlaceholder("Selectare Tip Contract");
				tipContract.setRequiredIndicatorVisible(true);
				tipContract.setErrorMessage("Selectare Tip Contract required !");

				DatePicker dataInceputContract = new DatePicker("Data Inceput Contract");
				dataInceputContract.setI18n(singleFormatI18n);
				dataInceputContract.setValue(contract.getDATA_INCEPUT_CONTRACT().toLocalDate());

				DatePicker dataSfarsitContract = new DatePicker("Data Sfarsit Contract");
				dataSfarsitContract.setI18n(singleFormatI18n);
				dataSfarsitContract.setValue(contract.getDATA_SFARSIT_CONTRACT().toLocalDate());
				dataSfarsitContract.addValueChangeListener(eventsc -> {
					if (!dataInceputContract.isEmpty() && !dataSfarsitContract.isEmpty()) {
						LocalDate st = dataInceputContract.getValue();
						st.format(DateTimeFormatter.ISO_LOCAL_DATE);
						LocalDate sf = dataSfarsitContract.getValue();
						sf.format(DateTimeFormatter.ISO_LOCAL_DATE);
						LocalDate now = LocalDate.now();

						boolean df = (now.isAfter(st) && now.isBefore(sf));
						if (df) {
							ValabilitateContract.setValue("Valabil");

						} else {
							ValabilitateContract.setValue("Expirat");
						}

					}

				});

				FileBuffer FileMemoryBuffer = new FileBuffer();
				Upload upload = new Upload(FileMemoryBuffer);
				Button uploadButton = new Button("Upload PDF...");
				uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				upload.setUploadButton(uploadButton);
				upload.setAcceptedFileTypes("application/pdf", ".pdf");
				upload.setMaxFiles(1);
				upload.setDropAllowed(false);
				int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
				upload.setMaxFileSize(maxFileSizeInBytes);
				upload.setMinWidth(15, Unit.EM);

				upload.addSucceededListener(event -> {

					fileData = FileMemoryBuffer.getInputStream();
					String fileName = event.getFileName();

					String mimeType = event.getMIMEType();

					System.out.println(fileName + " " + mimeType);
				});
				upload.addFileRejectedListener(event -> {
					String errorMessage = event.getErrorMessage();

					Notification notification = Notification.show(errorMessage, 5000,
							Notification.Position.BOTTOM_CENTER);
					notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
				});

				upload.getElement().addEventListener("max-files-reached-changed", event -> {
					boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
					uploadButton.setEnabled(!maxFilesReached);
				}).addEventData("event.detail.value");

				Checkbox rezileaza = new Checkbox("Rezileaza", contract.getINCETAT());
				rezileaza.addClassName(LumoUtility.Background.ERROR_10);

				FormLayout editLayout = new FormLayout();
				editLayout.setResponsiveSteps(

						new ResponsiveStep("0", 1),

						new ResponsiveStep("500px", 2));
				editLayout.setColspan(searchProprietar, 2);
				editLayout.setColspan(gridprops, 2);
				editLayout.setColspan(searchFermier, 2);
				editLayout.setColspan(gridferms, 2);
				editLayout.setColspan(searchTeren, 2);
				editLayout.setColspan(gridtpcfsup, 2);

				VerticalLayout dialogEditLayout = new VerticalLayout(editLayout);
				editLayout.add(NumarContract, dataContract, ValabilitateContract, searchProprietar, gridprops,
						searchFermier, gridferms, NumarContractFermier, DataContractFermier, searchTeren, gridtpcfsup,
						tipContract, dataInceputContract, dataSfarsitContract, upload, rezileaza);
				dialogEditLayout.setAlignItems(Alignment.CENTER);
				dialogEditLayout.setJustifyContentMode(JustifyContentMode.CENTER);
				dialogEditLayout.setPadding(false);
				dialogEditLayout.setSpacing(false);

				editDialog.add(dialogEditLayout);
				Button saveButton = new Button("Update!");
				saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				saveButton.addClickListener(evt -> {

					if (fermier == null || proprietar.isEmpty() || teren.isEmpty())
						return;

					contract.setProprietarID(proprietar);
					contract.setID_TEREN(teren);
					contract.setFERMIER(fermier);

					if (contract.getDATA_CONTRACT() != Date.valueOf(dataContract.getValue()))
						contract.setDATA_CONTRACT(Date.valueOf(dataContract.getValue()));

					if (NumarContractFermier.getValue() == 0
							&& contract.getNUMAR_CONTRACT_FERMIER() != NumarContractFermier.getValue())
						contract.setNUMAR_CONTRACT_FERMIER(NumarContractFermier.getValue());
					if (contract.getDATA_CONTRACT_FERMIER() != Date.valueOf(DataContractFermier.getValue()))
						contract.setDATA_CONTRACT_FERMIER(Date.valueOf(DataContractFermier.getValue()));
					if (!tipContract.getValue().isEmpty()
							&& contract.getTIP_CONTRACT() != tipContract.getValue().trim())
						contract.setTIP_CONTRACT(tipContract.getValue());
					if (contract.getDATA_INCEPUT_CONTRACT() != Date.valueOf(dataInceputContract.getValue()))
						contract.setDATA_INCEPUT_CONTRACT(Date.valueOf(dataInceputContract.getValue()));
					if (contract.getDATA_SFARSIT_CONTRACT() != Date.valueOf(dataSfarsitContract.getValue()))
						contract.setDATA_SFARSIT_CONTRACT(Date.valueOf(dataSfarsitContract.getValue()));
					contract.setPERIOADA_CONTRACT();
					contract.setINCETAT(rezileaza.getValue());
					if (rezileaza.getValue()) {
						contract.setVALABILITATE_CONTRACT("Reziliat");
						Notification.show("Contractul a fost reziliat", 2500, Position.BOTTOM_CENTER);
					} else
						contract.setVALABILITATE_CONTRACT(ValabilitateContract.getValue().trim());
					if (fileData != null) {
						Blob fd = null;
						try {
							fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

								@Override
								public Exception interceptException(Exception sqlEx) {
									System.out.println("Erroare1 la upload PJ!");
									return null;
								}

								@Override
								public ExceptionInterceptor init(Properties props, Log log) {
									System.out.println("Erroare2 la upload PJ!");
									return null;
								}

								@Override
								public void destroy() {
									System.out.println("Erroare3 la upload PJ!");

								}
							});
						} catch (IOException ex) {
							System.out.println(ex.getMessage());
						}

						contract.setDOSAR(fd);
					}
					contracteRepo.saveAndFlush(contract);
					target.getDataProvider().refreshAll();
					editDialog.close();

				});
				Button cancelButton = new Button("Cancel", evt -> editDialog.close());
				editDialog.getFooter().add(cancelButton);
				editDialog.getFooter().add(saveButton);
				editDialog.open();

			}));
			addItem("Delete", e -> e.getItem().ifPresent(contract -> {
				ConfirmDialog deleteDialog = new ConfirmDialog();
				deleteDialog.setHeader("Stergere contract");
				deleteDialog.setText("Sigur doriti sa stergeti contractul numarul: " + contract.getNUMAR_CONTRACT()
						+ "/" + contract.getDATA_CONTRACT() + " ?");
				deleteDialog.setCancelable(true);
				// deleteDialog.setRejectable(true);
				deleteDialog.setConfirmText("Delete");
				deleteDialog.setConfirmButtonTheme("error primary");
				deleteDialog.addConfirmListener(event -> {
					contracteRepo.delete(contract);

					UI.getCurrent().access(() -> {
						new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									TimeUnit.SECONDS.sleep(2);
									target.getDataProvider().refreshAll();
								} catch (InterruptedException e) {
									System.out.println("ERRROR: " + e.getLocalizedMessage());

								}
							}
						}

						).start();

					});

					this.close();
				});
				deleteDialog.open();

			}));

			add(new Hr());

			GridMenuItem<CONTRACTE> telefonItem = addItem("Numar Contract", e -> e.getItem().ifPresent(contract -> {
				UI.getCurrent().getPage()
						.executeJs("navigator.clipboard.writeText(" + contract.getNUMAR_CONTRACT() + ")");
			}));
			GridMenuItem<CONTRACTE> CNPItem = addItem("Data Contract", e -> e.getItem().ifPresent(contract -> {

				UI.getCurrent().getPage().executeJs("navigator.clipboard.writeText(String.raw`"
						+ contract.getDATA_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER) + "`)");
			}));

			setDynamicContentHandler(contract -> {
				// Do not show context menu when header is clicked
				if (contract == null)
					return false;
				telefonItem.setText(String.format("Numar Contract: %s", contract.getNUMAR_CONTRACT()));
				CNPItem.setText(String.format("Data Contract: %s",
						contract.getDATA_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER)));
				return true;
			});
		}

	}

}
