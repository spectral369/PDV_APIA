package com.spectral369.views.rapoarte.pdfs;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Iterator;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.font.PdfFontFactory.EmbeddingStrategy;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.event.PdfDocumentEvent;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.AreaBreakType;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.Leading;
import com.itextpdf.layout.properties.Property;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.spectral369.db.apia.ApiaInfo;
import com.spectral369.utils.FooterEvt;
import com.spectral369.utils.PDFHelper;
import com.spectral369.utils.Utils;

public class AdeverintaApia implements Serializable {

	private static final long serialVersionUID = -3869145632028530546L;

	File pdff = null;
	private transient PdfDocument document;
	private transient PdfWriter writer;
	private final String fileLoc;
	private final String fileName;
	private PdfFont font;
	private PdfFont bold;
	private Double supTotala = 0d;

	public AdeverintaApia(HashSet<ApiaInfo> apiaInfo) {

		fileName = "Adeverinta_apia_" + apiaInfo.iterator().next().NrCrt() + "_"
				+ LocalDateTime.now().format(Utils.DATETIMEFORMATTER) + ".pdf";

		fileLoc = Utils.getSaveFileLocation(fileName);
		pdff = new File(fileLoc);
		generatePdf(apiaInfo, pdff);
	}

	private void generatePdf(HashSet<ApiaInfo> apiaInfo, File pdfFile) {
		try {
			font = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN, PdfEncodings.CP1250,
					EmbeddingStrategy.PREFER_NOT_EMBEDDED, true);
			bold = PdfFontFactory.createFont(StandardFonts.TIMES_BOLD, PdfEncodings.CP1250,
					EmbeddingStrategy.PREFER_NOT_EMBEDDED, true);
			writer = new PdfWriter(pdfFile);
			document = new PdfDocument(writer);
			document.getDocumentInfo().addCreationDate();
			document.getDocumentInfo().setAuthor("spectral369");
			document.getDocumentInfo().setCreator("spectral369");
			document.getDocumentInfo().setMoreInfo("Created for ", "Primaria Dudestii-Vechi");
			document.getDocumentInfo().setTitle("Adeverinta_Apia_" + apiaInfo.iterator().next().Utilizator() + "_"
					+ LocalDateTime.now().format(Utils.DATETIMEFORMATTER).toString());
			document.setDefaultPageSize(PageSize.A4);

			Image antetLogo = PDFHelper.getAntetLogo();
			Document doc = new Document(document);
			doc.setProperty(Property.LEADING, new Leading(Leading.MULTIPLIED, 1f));
			float width = doc.getPageEffectiveArea(PageSize.A4).getWidth();
			doc.setFontSize(9);
			doc.setFont(font);
			document.addEventHandler(PdfDocumentEvent.START_PAGE, new FooterEvt(width));

			// svg stema :
			// https://upload.wikimedia.org/wikipedia/commons/7/70/Coat_of_arms_of_Romania.svg

			Paragraph p1 = new Paragraph();
			p1.setMultipliedLeading(0.1f);
			p1.setFontSize(8f);
			p1.add(new Tab());
			p1.add(new Text("ANEXA nr.1: ADEVERINTA").setFont(bold));
			doc.add(p1);

			Paragraph p2 = new Paragraph();
			p2.setMultipliedLeading(0.1f);
			p2.setMargin(0.1f);
			p2.setFontSize(8f);
			p2.add(PDFHelper.getEmptySpace(15));
			p2.add(new Text("Nr. Inregistrare Desfiintat"));
			p2.add(PDFHelper.getEmptySpace(110));
			p2.add(new Text("(Anexa nr. 1 la Ordinul nr. 619/2015)"));
			doc.add(p2);

			Paragraph p3 = new Paragraph("\n");
			p3.setFontSize(8f);

			antetLogo.scaleAbsolute(48, 45);
			p3.add(PDFHelper.getEmptySpace(45));

			p3.add(antetLogo);
			p3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			doc.add(p3);
			Paragraph title = new Paragraph("ROMANIA\nJUDETUL TIMIS\nPRIMARUL COMUNEI\nDUDESTII VECHI").setFont(bold);
			title.setFixedPosition((width / 2) - 35, 728, 100);
			title.setTextAlignment(TextAlignment.CENTER);
			doc.add(title);

			Paragraph p4 = new Paragraph();
			p4.setFontSize(10f);
			p4.add(new Tab());
			p4.add("Prin prezenta adeverinta se atesta faptul ca domnul ");
			p4.add(PDFHelper.createAdjustableParagraph(45, new Paragraph(apiaInfo.iterator().next().Proprietar())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			p4.add(", avand CNP ");
			p4.add(PDFHelper.createAdjustableParagraph(25, new Paragraph(apiaInfo.iterator().next().CNP())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			p4.add(", figureaza pe anul " + LocalDate.now().getYear()
					+ ", inregistrat la numarul de rol nominal unic ");
			p4.add(PDFHelper.createAdjustableParagraph(14, new Paragraph(apiaInfo.iterator().next().Rol().toString())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			p4.add(", precum si inscris in registrul agricol tipul");
			p4.add(PDFHelper
					.createAdjustableParagraph(16,
							new Paragraph(Utils.ProcesareTipProprietar(apiaInfo.iterator().next().Domiciliu(),
									apiaInfo.iterator().next().Tip())).simulateBold()
									.setTextAlignment(TextAlignment.CENTER)));
			p4.add(", volumul ");
			if (apiaInfo.iterator().next().Volum().isBlank() || apiaInfo.iterator().next().Volum().isEmpty())
				p4.add(PDFHelper.createAdjustableParagraph(8,
						new Paragraph("0").simulateBold().setTextAlignment(TextAlignment.CENTER)));
			else
				p4.add(PDFHelper.createAdjustableParagraph(8, new Paragraph(apiaInfo.iterator().next().Volum())
						.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			p4.add(", pozitia ");
			p4.add(PDFHelper.createAdjustableParagraph(8, new Paragraph(apiaInfo.iterator().next().Pozitie().toString())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			p4.add(", satul ");
			p4.add(PDFHelper.createAdjustableParagraph(48, new Paragraph(apiaInfo.iterator().next().Domiciliu())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			p4.add(".\n");
			doc.add(p4);

			Paragraph p5 = new Paragraph();
			p5.setFont(bold);
			p2.setFontSize(10f);
			p5.add("-Capitolul II b): Identificarea pe parcele a terenurilor aflate in proprietate\n");
			doc.add(p5);

			Table tabel1 = new Table(UnitValue.createPercentArray(new float[] { 1, 2, 2, 1 }));
			tabel1.setHorizontalAlignment(HorizontalAlignment.CENTER);

			Cell suprafata = new Cell(1, 2);
			suprafata.add(new Paragraph("Suprafata"));
			suprafata.setTextAlignment(TextAlignment.CENTER);
			suprafata.setHorizontalAlignment(HorizontalAlignment.CENTER);
			suprafata.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel1.addCell(suprafata);

			Cell categorie = new Cell(2, 1);
			categorie.add(new Paragraph("Categorie de folosinta"));
			categorie.setTextAlignment(TextAlignment.CENTER);
			categorie.setHorizontalAlignment(HorizontalAlignment.CENTER);
			categorie.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel1.addCell(categorie);

			Cell blocFizic = new Cell(2, 1);
			blocFizic.add(new Paragraph("Bloc fizic"));
			blocFizic.setTextAlignment(TextAlignment.CENTER);
			blocFizic.setHorizontalAlignment(HorizontalAlignment.CENTER);
			blocFizic.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel1.addCell(blocFizic);

			Cell ha = new Cell();
			ha.add(new Paragraph("ha"));
			ha.setTextAlignment(TextAlignment.CENTER);
			ha.setHorizontalAlignment(HorizontalAlignment.CENTER);
			ha.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel1.addCell(ha);

			Cell ari = new Cell();
			ari.add(new Paragraph("ari"));
			ari.setTextAlignment(TextAlignment.CENTER);
			ari.setHorizontalAlignment(HorizontalAlignment.CENTER);
			ari.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel1.addCell(ari);

			// data

			Iterator<ApiaInfo> it0 = apiaInfo.iterator();

			while (it0.hasNext()) {
				ApiaInfo item = it0.next();

				Cell ha1 = new Cell();
				ha1.add(new Paragraph(
						String.valueOf(Double.valueOf(Math.floor(item.SupfrafataUtilizata() / 10000)).intValue())));
				ha1.setTextAlignment(TextAlignment.CENTER);
				ha1.setHorizontalAlignment(HorizontalAlignment.CENTER);
				ha1.setVerticalAlignment(VerticalAlignment.MIDDLE);
				tabel1.addCell(ha1);

				Cell ari1 = new Cell();
				ari1.add(new Paragraph(
						String.valueOf(Double.valueOf(item.SupfrafataUtilizata() % 10000).intValue()).substring(0, 1)));
				ari1.setTextAlignment(TextAlignment.CENTER);
				ari1.setHorizontalAlignment(HorizontalAlignment.CENTER);
				ari1.setVerticalAlignment(VerticalAlignment.MIDDLE);
				tabel1.addCell(ari1);

				Cell cat1 = new Cell();
				cat1.add(new Paragraph(item.Categorie()));
				cat1.setTextAlignment(TextAlignment.CENTER);
				cat1.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cat1.setVerticalAlignment(VerticalAlignment.MIDDLE);
				tabel1.addCell(cat1);

				Cell bloc1 = new Cell();
				bloc1.add(new Paragraph(String.valueOf(item.BlocFizic())));
				bloc1.setTextAlignment(TextAlignment.CENTER);
				bloc1.setHorizontalAlignment(HorizontalAlignment.CENTER);
				bloc1.setVerticalAlignment(VerticalAlignment.MIDDLE);
				tabel1.addCell(bloc1);

			}

			doc.add(tabel1);

			Paragraph p6 = new Paragraph();
			p6.add("Perioada de pasunat 7) 23.04." + LocalDate.now().getYear() + "-26.10." + LocalDate.now().getYear());
			doc.add(p6);

			Paragraph p7 = new Paragraph();
			p7.setFont(bold);
			p7.add("Capitolul III: Modul de utilizare a suprafetelor agricole situate pe raza localitatii\n");
			doc.add(p7);

			Table tabel2 = new Table(8);
			tabel2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			// header start
			Cell H1 = new Cell();
			H1.add(new Paragraph("             "));
			H1.setTextAlignment(TextAlignment.CENTER);
			H1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H1);

			Cell H2 = new Cell();
			H2.add(new Paragraph("Cod rand"));
			H2.setTextAlignment(TextAlignment.CENTER);
			H2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H2);

			Cell H3 = new Cell();
			H3.add(new Paragraph("ha"));
			H3.setTextAlignment(TextAlignment.CENTER);
			H3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H3);

			Cell H4 = new Cell();
			H4.add(new Paragraph("ari"));
			H4.setTextAlignment(TextAlignment.CENTER);
			H4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H4);

			Cell H5 = new Cell();
			H5.add(new Paragraph("            "));
			H5.setTextAlignment(TextAlignment.CENTER);
			H5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H5);

			Cell H6 = new Cell();
			H6.add(new Paragraph("Cod rand"));
			H6.setTextAlignment(TextAlignment.CENTER);
			H6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H6);

			Cell H7 = new Cell();
			H7.add(new Paragraph("ha"));
			H7.setTextAlignment(TextAlignment.CENTER);
			H7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H7);

			Cell H8 = new Cell();
			H8.add(new Paragraph("ari"));
			H8.setTextAlignment(TextAlignment.CENTER);
			H8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			H8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(H8);
			// header end
			// row1

			Cell r1c1 = new Cell();
			r1c1.add(new Paragraph("Suprafaţa agricolă în proprietate = cap. II lit. a)Terenuri aflate în"
					+ " proprietate, cod 10, coloanele 2, 5, 8, 11, 14"));
			r1c1.setTextAlignment(TextAlignment.CENTER);
			r1c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c1);

			Cell r1c2 = new Cell();
			r1c2.add(new Paragraph("1"));
			r1c2.setTextAlignment(TextAlignment.CENTER);
			r1c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c2);

			Iterator<ApiaInfo> it1 = apiaInfo.iterator();

			while (it1.hasNext()) {
				ApiaInfo item = it1.next();
				supTotala += item.SupfrafataUtilizata();

			}

			Cell r1c3 = new Cell();
			r1c3.add(new Paragraph(String.valueOf(Double.valueOf(Math.floor(supTotala / 10000)).intValue())));
			r1c3.setTextAlignment(TextAlignment.CENTER);
			r1c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c3);

			Cell r1c4 = new Cell();
			r1c4.add(new Paragraph(String.valueOf(Double.valueOf(supTotala % 10000).intValue()).substring(0, 1)));
			r1c4.setTextAlignment(TextAlignment.CENTER);
			r1c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c4);

			Cell r1c5 = new Cell();
			r1c5.add(new Paragraph("Suprafaţa agricolă dată (cod 10 +...+ 15)"));
			r1c5.setTextAlignment(TextAlignment.CENTER);
			r1c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c5);

			Cell r1c6 = new Cell();
			r1c6.add(new Paragraph("9"));
			r1c6.setTextAlignment(TextAlignment.CENTER);
			r1c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c6);

			Cell r1c7 = new Cell();
			r1c7.add(new Paragraph(" sup data ?! ha "));
			r1c7.setTextAlignment(TextAlignment.CENTER);
			r1c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c7);

			Cell r1c8 = new Cell();
			r1c8.add(new Paragraph(" sup data ?! ari "));
			r1c8.setTextAlignment(TextAlignment.CENTER);
			r1c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r1c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r1c8);
			// endrow1

			// row 2
			Cell r2c1 = new Cell();
			r2c1.add(new Paragraph("Suprafaţa agricolă primită (cod 03 +...+ 08)"));
			r2c1.setTextAlignment(TextAlignment.CENTER);
			r2c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c1);

			Cell r2c2 = new Cell();
			r2c2.add(new Paragraph("2"));
			r2c2.setTextAlignment(TextAlignment.CENTER);
			r2c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c2);

			Cell r2c3 = new Cell();
			r2c3.add(new Paragraph("0"));
			r2c3.setTextAlignment(TextAlignment.CENTER);
			r2c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c3);

			Cell r2c4 = new Cell();
			r2c4.add(new Paragraph("0"));
			r2c4.setTextAlignment(TextAlignment.CENTER);
			r2c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c4);

			Cell r2c5 = new Cell();
			r2c5.add(new Paragraph("- in arenda"));
			r2c5.setTextAlignment(TextAlignment.CENTER);
			r2c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c5);

			Cell r2c6 = new Cell();
			r2c6.add(new Paragraph("10"));
			r2c6.setTextAlignment(TextAlignment.CENTER);
			r2c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c6);

			Cell r2c7 = new Cell();
			r2c7.add(new Paragraph("ha"));
			r2c7.setTextAlignment(TextAlignment.CENTER);
			r2c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c7);

			Cell r2c8 = new Cell();
			r2c8.add(new Paragraph("ari"));
			r2c8.setTextAlignment(TextAlignment.CENTER);
			r2c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r2c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r2c8);
			// endrow2
			// row 3
			Cell r3c1 = new Cell();
			r3c1.add(new Paragraph("- în arendă"));
			r3c1.setTextAlignment(TextAlignment.CENTER);
			r3c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c1);

			Cell r3c2 = new Cell();
			r3c2.add(new Paragraph("3"));
			r3c2.setTextAlignment(TextAlignment.CENTER);
			r3c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c2);

			Cell r3c3 = new Cell();
			r3c3.add(new Paragraph("0"));
			r3c3.setTextAlignment(TextAlignment.CENTER);
			r3c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c3);

			Cell r3c4 = new Cell();
			r3c4.add(new Paragraph("0"));
			r3c4.setTextAlignment(TextAlignment.CENTER);
			r3c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c4);

			Cell r3c5 = new Cell();
			r3c5.add(new Paragraph("- în parte"));
			r3c5.setTextAlignment(TextAlignment.CENTER);
			r3c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c5);

			Cell r3c6 = new Cell();
			r3c6.add(new Paragraph("11"));
			r3c6.setTextAlignment(TextAlignment.CENTER);
			r3c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c6);

			Cell r3c7 = new Cell();
			r3c7.add(new Paragraph("ha"));
			r3c7.setTextAlignment(TextAlignment.CENTER);
			r3c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c7);

			Cell r3c8 = new Cell();
			r3c8.add(new Paragraph("ari"));
			r3c8.setTextAlignment(TextAlignment.CENTER);
			r3c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r3c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r3c8);
			// endrow3
			// row 4
			Cell r4c1 = new Cell();
			r4c1.add(new Paragraph("- în parte"));
			r4c1.setTextAlignment(TextAlignment.CENTER);
			r4c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c1);

			Cell r4c2 = new Cell();
			r4c2.add(new Paragraph("4"));
			r4c2.setTextAlignment(TextAlignment.CENTER);
			r4c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c2);

			Cell r4c3 = new Cell();
			r4c3.add(new Paragraph("0"));
			r4c3.setTextAlignment(TextAlignment.CENTER);
			r4c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c3);

			Cell r4c4 = new Cell();
			r4c4.add(new Paragraph("0"));
			r4c4.setTextAlignment(TextAlignment.CENTER);
			r4c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c4);

			Cell r4c5 = new Cell();
			r4c5.add(new Paragraph("- cu titlu gratuit"));
			r4c5.setTextAlignment(TextAlignment.CENTER);
			r4c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c5);

			Cell r4c6 = new Cell();
			r4c6.add(new Paragraph("12"));
			r4c6.setTextAlignment(TextAlignment.CENTER);
			r4c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c6);

			Cell r4c7 = new Cell();
			r4c7.add(new Paragraph("ha"));
			r4c7.setTextAlignment(TextAlignment.CENTER);
			r4c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c7);

			Cell r4c8 = new Cell();
			r4c8.add(new Paragraph("ari"));
			r4c8.setTextAlignment(TextAlignment.CENTER);
			r4c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r4c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r4c8);
			// endrow4
			// row 5
			Cell r5c1 = new Cell();
			r5c1.add(new Paragraph("- cu titlu gratuit"));
			r5c1.setTextAlignment(TextAlignment.CENTER);
			r5c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c1);

			Cell r5c2 = new Cell();
			r5c2.add(new Paragraph("5"));
			r5c2.setTextAlignment(TextAlignment.CENTER);
			r5c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c2);

			Cell r5c3 = new Cell();
			r5c3.add(new Paragraph("0"));
			r5c3.setTextAlignment(TextAlignment.CENTER);
			r5c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c3);

			Cell r5c4 = new Cell();
			r5c4.add(new Paragraph("0"));
			r5c4.setTextAlignment(TextAlignment.CENTER);
			r5c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c4);

			Cell r5c5 = new Cell();
			r5c5.add(new Paragraph("- în concesiune"));
			r5c5.setTextAlignment(TextAlignment.CENTER);
			r5c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c5);

			Cell r5c6 = new Cell();
			r5c6.add(new Paragraph("13"));
			r5c6.setTextAlignment(TextAlignment.CENTER);
			r5c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c6);

			Cell r5c7 = new Cell();
			r5c7.add(new Paragraph("ha"));
			r5c7.setTextAlignment(TextAlignment.CENTER);
			r5c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c7);

			Cell r5c8 = new Cell();
			r5c8.add(new Paragraph("ari"));
			r5c8.setTextAlignment(TextAlignment.CENTER);
			r5c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r5c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r5c8);
			// endrow5
			// row 6
			Cell r6c1 = new Cell();
			r6c1.add(new Paragraph("- în concesiune"));
			r6c1.setTextAlignment(TextAlignment.CENTER);
			r6c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c1);

			Cell r6c2 = new Cell();
			r6c2.add(new Paragraph("6"));
			r6c2.setTextAlignment(TextAlignment.CENTER);
			r6c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c2);

			Cell r6c3 = new Cell();
			r6c3.add(new Paragraph("0"));
			r6c3.setTextAlignment(TextAlignment.CENTER);
			r6c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c3);

			Cell r6c4 = new Cell();
			r6c4.add(new Paragraph("0"));
			r6c4.setTextAlignment(TextAlignment.CENTER);
			r6c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c4);

			Cell r6c5 = new Cell();
			r6c5.add(new Paragraph("- în asociere"));
			r6c5.setTextAlignment(TextAlignment.CENTER);
			r6c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c5);

			Cell r6c6 = new Cell();
			r6c6.add(new Paragraph("14"));
			r6c6.setTextAlignment(TextAlignment.CENTER);
			r6c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c6);

			Cell r6c7 = new Cell();
			r6c7.add(new Paragraph("ha"));
			r6c7.setTextAlignment(TextAlignment.CENTER);
			r6c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c7);

			Cell r6c8 = new Cell();
			r6c8.add(new Paragraph("ari"));
			r6c8.setTextAlignment(TextAlignment.CENTER);
			r6c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r6c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r6c8);
			// endrow6
			// row 7
			Cell r7c1 = new Cell();
			r7c1.add(new Paragraph("- în asociere"));
			r7c1.setTextAlignment(TextAlignment.CENTER);
			r7c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c1);

			Cell r7c2 = new Cell();
			r7c2.add(new Paragraph("7"));
			r7c2.setTextAlignment(TextAlignment.CENTER);
			r7c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c2);

			Cell r7c3 = new Cell();
			r7c3.add(new Paragraph("0"));
			r7c3.setTextAlignment(TextAlignment.CENTER);
			r7c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c3);

			Cell r7c4 = new Cell();
			r7c4.add(new Paragraph("0"));
			r7c4.setTextAlignment(TextAlignment.CENTER);
			r7c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c4);

			Cell r7c5 = new Cell();
			r7c5.add(new Paragraph("- sub alte forme"));
			r7c5.setTextAlignment(TextAlignment.CENTER);
			r7c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c5);

			Cell r7c6 = new Cell();
			r7c6.add(new Paragraph("15"));
			r7c6.setTextAlignment(TextAlignment.CENTER);
			r7c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c6);

			Cell r7c7 = new Cell();
			r7c7.add(new Paragraph("ha"));
			r7c7.setTextAlignment(TextAlignment.CENTER);
			r7c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c7);

			Cell r7c8 = new Cell();
			r7c8.add(new Paragraph("ari"));
			r7c8.setTextAlignment(TextAlignment.CENTER);
			r7c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r7c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r7c8);
			// endrow7
			// row 8
			Cell r8c1 = new Cell();
			r8c1.add(new Paragraph("- sub alte forme"));
			r8c1.setTextAlignment(TextAlignment.CENTER);
			r8c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c1);

			Cell r8c2 = new Cell();
			r8c2.add(new Paragraph("8"));
			r8c2.setTextAlignment(TextAlignment.CENTER);
			r8c2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c2);

			Cell r8c3 = new Cell();
			r8c3.add(new Paragraph("0"));
			r8c3.setTextAlignment(TextAlignment.CENTER);
			r8c3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c3);

			Cell r8c4 = new Cell();
			r8c4.add(new Paragraph("0"));
			r8c4.setTextAlignment(TextAlignment.CENTER);
			r8c4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c4);

			Cell r8c5 = new Cell();
			r8c5.add(new Paragraph("din randul 9 - la unităţi cu personalitate juridică"));
			r8c5.setTextAlignment(TextAlignment.CENTER);
			r8c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c5);

			Cell r8c6 = new Cell();
			r8c6.add(new Paragraph("16"));
			r8c6.setTextAlignment(TextAlignment.CENTER);
			r8c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c6);

			Cell r8c7 = new Cell();
			r8c7.add(new Paragraph("ha"));
			r8c7.setTextAlignment(TextAlignment.CENTER);
			r8c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c7);

			Cell r8c8 = new Cell();
			r8c8.add(new Paragraph("ari"));
			r8c8.setTextAlignment(TextAlignment.CENTER);
			r8c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r8c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r8c8);
			// endrow8
			// row 9
			Cell r9c1 = new Cell(1, 4);
			r9c1.add(new Paragraph(""));
			r9c1.setTextAlignment(TextAlignment.CENTER);
			r9c1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r9c1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r9c1);

			Cell r9c5 = new Cell();
			r9c5.add(new Paragraph("Suprafaţa agricolă utilizată (cod 01 + 02 - 09)"));
			r9c5.setTextAlignment(TextAlignment.CENTER);
			r9c5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r9c5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r9c5);

			Cell r9c6 = new Cell();
			r9c6.add(new Paragraph("17"));
			r9c6.setTextAlignment(TextAlignment.CENTER);
			r9c6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r9c6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r9c6);

			Cell r9c7 = new Cell();
			r9c7.add(new Paragraph(String.valueOf(Double.valueOf(Math.floor(supTotala / 10000)).intValue())));
			r9c7.setTextAlignment(TextAlignment.CENTER);
			r9c7.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r9c7.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r9c7);

			Cell r9c8 = new Cell();
			r9c8.add(new Paragraph(String.valueOf(Double.valueOf(supTotala % 10000).intValue()).substring(0, 1)));
			r9c8.setTextAlignment(TextAlignment.CENTER);
			r9c8.setHorizontalAlignment(HorizontalAlignment.CENTER);
			r9c8.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel2.addCell(r9c8);
			// endrow8

			doc.add(tabel2);
			// the end

			Paragraph p8 = new Paragraph();
			p8.setFontSize(8.5f);
			p8.add(PDFHelper.getEmptySpace(4));
			p8.add("Prezenta adeverinţă s-a eliberat cu respectarea prevederilor Ordonanţei Guvernului nr. 33/2002  privind"
					+ " reglementarea eliberării certificatelor şi adeverinţelor de către autorităţile publice centrale şi locale, "
					+ "aprobată cu modificări prin Legea nr. 223/2002, fiind necesară pentru depunerea cererii unice de plata în anul "
					+ LocalDate.now().getYear() + ".");

			doc.add(p8);

			Table tabel3 = new Table(3);
			tabel3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			tabel3.setWidth(width / 1.11f);
			tabel3.setBorder(Border.NO_BORDER);

			Cell primar = new Cell();
			primar.setBorder(Border.NO_BORDER);
			primar.add(new Paragraph("Primar,"));
			primar.setTextAlignment(TextAlignment.CENTER);
			primar.setHorizontalAlignment(HorizontalAlignment.CENTER);
			primar.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(primar);

			Cell space1 = new Cell();
			space1.setBorder(Border.NO_BORDER);
			Paragraph spaceP = new Paragraph();
			spaceP.add(PDFHelper.getEmptySpace(300));
			space1.add(spaceP);
			space1.setTextAlignment(TextAlignment.CENTER);
			space1.setHorizontalAlignment(HorizontalAlignment.CENTER);
			space1.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(space1);

			Cell taxe = new Cell();
			taxe.setBorder(Border.NO_BORDER);
			taxe.add(new Paragraph("Inspector cu atribuţii în domeniul administrării impozitelor si taxelor locale,"));
			taxe.setTextAlignment(TextAlignment.CENTER);
			taxe.setHorizontalAlignment(HorizontalAlignment.CENTER);
			taxe.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(taxe);

			Cell dotsPrimar = new Cell();
			dotsPrimar.setBorder(Border.NO_BORDER);
			Paragraph primarPara = new Paragraph();
			primarPara.add(
					new Text(PDFHelper.getStrWithSpace(22, "(primar)")).setTextRise(-10).setUnderline().setFontSize(8));
			dotsPrimar.add(primarPara);
			dotsPrimar.setTextAlignment(TextAlignment.CENTER);
			dotsPrimar.setHorizontalAlignment(HorizontalAlignment.CENTER);
			dotsPrimar.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(dotsPrimar);

			Cell space2 = new Cell();
			space2.setBorder(Border.NO_BORDER);
			Paragraph spaceP2 = new Paragraph();
			spaceP2.add(PDFHelper.getEmptySpace(300));
			space2.add(spaceP2);
			space2.setTextAlignment(TextAlignment.CENTER);
			space2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			space2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(space2);

			Cell taxeDots = new Cell();
			taxeDots.setBorder(Border.NO_BORDER);
			Paragraph taxePara = new Paragraph();

			taxePara.add(new Text(PDFHelper.getStrWithSpace(22, "")).setTextRise(-10).setUnderline().setFontSize(8));
			taxeDots.add(taxePara);
			taxeDots.setTextAlignment(TextAlignment.CENTER);
			taxeDots.setHorizontalAlignment(HorizontalAlignment.CENTER);
			taxeDots.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(taxeDots);

			Cell ls = new Cell();
			ls.setBorder(Border.NO_BORDER);
			ls.setTextAlignment(TextAlignment.CENTER);
			ls.setHorizontalAlignment(HorizontalAlignment.CENTER);
			ls.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(ls);

			Cell space3 = new Cell();
			space3.setBorder(Border.NO_BORDER);
			Paragraph spaceP3 = new Paragraph();
			spaceP3.add("L.S");
			spaceP3.add(PDFHelper.getEmptySpace(300));
			space3.add(spaceP3);
			space3.setTextAlignment(TextAlignment.LEFT);
			space3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			space3.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(space3);

			Cell space4 = new Cell();
			space4.setBorder(Border.NO_BORDER);
			Paragraph spaceP4 = new Paragraph();
			spaceP4.add(PDFHelper.getEmptySpace(120));
			space4.add(spaceP4);
			space4.setTextAlignment(TextAlignment.CENTER);
			space4.setHorizontalAlignment(HorizontalAlignment.CENTER);
			space4.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(space4);

			Cell secretar = new Cell();
			secretar.setBorder(Border.NO_BORDER);
			secretar.add(new Paragraph("Secretar,"));
			secretar.setTextAlignment(TextAlignment.CENTER);
			secretar.setHorizontalAlignment(HorizontalAlignment.CENTER);
			secretar.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(secretar);

			Cell space5 = new Cell();
			space5.setBorder(Border.NO_BORDER);
			Paragraph spaceP5 = new Paragraph();
			spaceP5.add(PDFHelper.getEmptySpace(300));
			space5.add(spaceP5);
			space5.setTextAlignment(TextAlignment.CENTER);
			space5.setHorizontalAlignment(HorizontalAlignment.CENTER);
			space5.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(space5);

			Cell regagr = new Cell();
			regagr.setBorder(Border.NO_BORDER);
			regagr.add(new Paragraph(
					"Inspector cu atribuţii privind completarea, ţinerea la zi si centralizarea datelor din registrele agricole"));
			regagr.setTextAlignment(TextAlignment.CENTER);
			regagr.setHorizontalAlignment(HorizontalAlignment.CENTER);
			regagr.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(regagr);
			///

			Cell dotsSecretar = new Cell();
			dotsSecretar.setBorder(Border.NO_BORDER);
			Paragraph secretarPara = new Paragraph();
			secretarPara.add(new Text(PDFHelper.getStrWithSpace(22, "(Secretar)")).setTextRise(-10).setUnderline()
					.setFontSize(8));
			dotsSecretar.add(secretarPara);
			dotsSecretar.setTextAlignment(TextAlignment.CENTER);
			dotsSecretar.setHorizontalAlignment(HorizontalAlignment.CENTER);
			dotsSecretar.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(dotsSecretar);

			Cell space6 = new Cell();
			space6.setBorder(Border.NO_BORDER);
			Paragraph spaceP6 = new Paragraph();
			spaceP6.add(PDFHelper.getEmptySpace(300));
			space6.add(spaceP6);
			space6.setTextAlignment(TextAlignment.CENTER);
			space6.setHorizontalAlignment(HorizontalAlignment.CENTER);
			space6.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(space6);

			Cell dotsAgr = new Cell();
			dotsAgr.setBorder(Border.NO_BORDER);
			Paragraph agrPara = new Paragraph();
			agrPara.add(new Text(PDFHelper.getStrWithSpace(22, "(Reg agricol)")).setTextRise(-10).setUnderline()
					.setFontSize(8));
			dotsAgr.add(agrPara);
			dotsAgr.setTextAlignment(TextAlignment.CENTER);
			dotsAgr.setHorizontalAlignment(HorizontalAlignment.CENTER);
			dotsAgr.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tabel3.addCell(dotsAgr);

			doc.add(tabel3);

			// next page
			doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

			Paragraph p2p1 = new Paragraph(PDFHelper.getStrWithSpace(4, "") + "1) Se înscrie denumirea judeţului.");
			doc.add(p2p1);
			Paragraph p2p2 = new Paragraph(
					PDFHelper.getStrWithSpace(4, "") + "2)Se înscrie categoria unităţii administrativ-teritoriale.");
			doc.add(p2p2);
			Paragraph p2p3 = new Paragraph(
					PDFHelper.getStrWithSpace(4, "") + "3)Se înscrie denumirea unităţii administrativ-teritoriale.");
			doc.add(p2p3);
			Paragraph p2p4 = new Paragraph(PDFHelper.getStrWithSpace(4, "")
					+ "4)Se înscrie codul de identificare fiscală, respectiv: codul de înregistrare fiscală, codul numeric personal, numărul de identificare fiscală sau codul unic de înregistrare, după caz.");
			doc.add(p2p4);
			Paragraph p2p5 = new Paragraph(PDFHelper.getStrWithSpace(4, "")
					+ "5)Se înscrie numărul de rol nominal unic de către persoana cu atribuţii în domeniul administrării impozitelor şi taxelor locale.");
			doc.add(p2p5);
			Paragraph p2p6 = new Paragraph(PDFHelper.getStrWithSpace(4, "")
					+ "6)Se înscriu datele corespunzătoare de către persoana cu atribuţii privind completarea, ţinerea la zi şi centralizarea datelor din registrele agricole.");
			doc.add(p2p6);
			Paragraph p2p7 = new Paragraph(PDFHelper.getStrWithSpace(4, "")
					+ "7)În cazul categoriei de folosință “pajiști permanente” (pășuni+fânețe)  se completează perioada de pășunat aprobată pentru fiecare UAT, conform prevederilor art.6 alin. (5) din Ordonanţa de urgenţă nr. 34/2013 privind organizarea, administrarea si exploatarea pajistilor permanente si pentru modificarea si completarea Legii fondului funciar nr.18/1991, cu modificarile si completari prin Legea nr. 86/2014, cu modificarile si completarile ulterioare.");
			doc.add(p2p7);
			Paragraph p2p8 = new Paragraph(PDFHelper.getStrWithSpace(4, "")
					+ "8)Conform anexei nr. 1 la Hotărârea Guvernului nr. 985/2019 privind registrul agricol pentru perioada 2020-2024.\n");
			doc.add(p2p8);
			Paragraph p2p9 = new Paragraph(PDFHelper.getStrWithSpace(4, "")
					+ "NOTĂ: Eliberarea adeverinţei nu se condiţionează de plata impozitelor şi taxelor locale. ");
			p2p9.setMarginTop(15f);
			doc.add(p2p9);

			doc.close();
			document.close();
			writer.flush();

		} catch (Exception e) {

		}

	}

	public String getFileLoc() {
		return fileLoc;
	}

	public String getFileName() {
		return fileName;
	}
}
