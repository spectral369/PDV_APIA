package com.spectral369.views.rapoarte.pdfs;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.event.PdfDocumentEvent;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.spectral369.db.apia.ApiaInfo;
import com.spectral369.utils.FooterEvt;
import com.spectral369.utils.PDFHelper;
import com.spectral369.utils.Utils;

import oshi.util.tuples.Pair;

public class RaportApiaPDF implements Serializable {
	private static final long serialVersionUID = 2256907571636627077L;
	File pdff = null;
	private transient PdfDocument document;
	private transient PdfWriter writer;
	private final String fileLoc;
	private final String fileName;
	private double total1 = 0d;
	Color headerBg = new DeviceRgb(0, 255, 0);

	public RaportApiaPDF(HashSet<ApiaInfo> apiaInfo) {

		fileName = "Raport_Apia_" + apiaInfo.iterator().next().NrCrt() + "_"
				+ LocalDateTime.now().format(Utils.DATETIMEFORMATTER) + ".pdf";

		fileLoc = Utils.getSaveFileLocation(fileName);
		pdff = new File(fileLoc);
		generatePdf(apiaInfo, pdff);
	}

	@SuppressWarnings("static-access")
	private void generatePdf(HashSet<ApiaInfo> apiaInfo, File pdfFile) {
		try {
			writer = new PdfWriter(pdfFile);
			document = new PdfDocument(writer);
			document.getDocumentInfo().addCreationDate();
			document.getDocumentInfo().setAuthor("spectral369");
			document.getDocumentInfo().setCreator("spectral369");
			document.getDocumentInfo().setMoreInfo("Created for ", "Primaria Dudestii-Vechi");
			document.getDocumentInfo().setTitle("Raport_Apia_" + apiaInfo.iterator().next().Utilizator() + "_"
					+ LocalDateTime.now().format(Utils.DATETIMEFORMATTER).toString());
			document.setDefaultPageSize(PageSize.A4.rotate());

			Document doc = new Document(document);
			float width = doc.getPageEffectiveArea(PageSize.A4.rotate()).getWidth();
			doc.setFontSize(10.5f);
			document.addEventHandler(PdfDocumentEvent.START_PAGE, new FooterEvt(width));

			final Paragraph nrInreg = new Paragraph();
			nrInreg.add("\n\n");
			nrInreg.add(new Tab());
			Text nrI = new Text(
					"\tNr. " + PDFHelper.getStrWithDash(15, "") + " " + "data " + PDFHelper.getStrWithDash(20, ""));
			nrInreg.add(nrI);

			Table dosar = new Table(1);
			dosar.setFontSize(8f);
			dosar.setHorizontalAlignment(HorizontalAlignment.CENTER);

			Text dosarHeader = new Text("Nr. Dosar");
			Cell cellHeader = new Cell();
			cellHeader.setBackgroundColor(headerBg);
			cellHeader.add(new Paragraph(dosarHeader));
			cellHeader.setBorder(new SolidBorder(0.8f));
			cellHeader.setTextAlignment(TextAlignment.CENTER);
			cellHeader.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellHeader.setVerticalAlignment(VerticalAlignment.MIDDLE);
			dosar.addCell(cellHeader);

			Iterator<ApiaInfo> itDosar = apiaInfo.iterator();
			List<Integer> dosare = new ArrayList<>();

			while (itDosar.hasNext()) {
				ApiaInfo ai = itDosar.next();
				dosare.add(ai.NrCrt());
			}
			List<Integer> DosareUnice = dosare.stream().distinct().collect(Collectors.toList());
			StringBuilder sbd = new StringBuilder();
			for (int d : DosareUnice) {
				sbd.append(String.valueOf(d));
				sbd.append(";");

			}
			Text dosarNr = new Text(sbd.toString());
			Cell cellNr = new Cell();
			cellNr.add(new Paragraph(dosarNr));
			cellNr.setBorder(new SolidBorder(0.8f));
			cellNr.setTextAlignment(TextAlignment.CENTER);
			cellNr.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellNr.setVerticalAlignment(VerticalAlignment.MIDDLE);
			dosar.addCell(cellNr);

			nrInreg.add(PDFHelper.getEmptySpace(120));
			nrInreg.add(dosar);

			doc.add(nrInreg);

			final Paragraph titlu = new Paragraph();
			Text t1 = new Text("\nCATRE,\nPRIMARIA DUDESTII VECHI\n").simulateBold();
			titlu.setHorizontalAlignment(HorizontalAlignment.CENTER);
			titlu.setTextAlignment(TextAlignment.CENTER);
			titlu.add(t1).addStyle(PDFHelper.bold12nr);
			doc.add(titlu);

			Paragraph dec = new Paragraph();
			dec.add(new Tab());
			dec.add("Subsemnatul/a ");
			dec.add(PDFHelper.createAdjustableParagraph(35, new Paragraph(apiaInfo.iterator().next().Utilizator())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			dec.add(" avand C.N.P ");
			dec.add(PDFHelper.createAdjustableParagraph(27, new Paragraph(apiaInfo.iterator().next().CNP())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			dec.add(" cu domiciliul in ");
			dec.add(PDFHelper.createAdjustableParagraph(26, new Paragraph(apiaInfo.iterator().next().Domiciliu())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			dec.add(" Nr. ");
			dec.add(PDFHelper.createAdjustableParagraph(10, new Paragraph(apiaInfo.iterator().next().NrCasa())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			dec.add(" Judet ");
			dec.add(PDFHelper.createAdjustableParagraph(12, new Paragraph(apiaInfo.iterator().next().Judet())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			dec.add(" telefon ");
			dec.add(PDFHelper.createAdjustableParagraph(20, new Paragraph(apiaInfo.iterator().next().Telefon())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			dec.add(" . Rog prin prezenta eliberarea unei adeverinte fiindu-mi necesara in vederea intocmirii dosarului pentru subventia APIA.\n");

			doc.add(dec);
			float[] pointColumnWidths = { 200F, 300F, 200F };
			Table rootTab1 = new Table(pointColumnWidths);
			rootTab1.setBorder(Border.NO_BORDER);
			rootTab1.setHorizontalAlignment(HorizontalAlignment.CENTER);

			Table tabel1 = new Table(2);

			tabel1.setHorizontalAlignment(HorizontalAlignment.CENTER);

			Text forma = new Text("Forma");
			Cell cellForma = new Cell();
			cellForma.setBackgroundColor(headerBg);
			cellForma.add(new Paragraph(forma));
			cellForma.setBorder(new SolidBorder(0.8f));
			cellForma.setTextAlignment(TextAlignment.CENTER);
			cellForma.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellForma.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text suprafata = new Text("Suprafata");
			Cell cellSuprafata = new Cell();
			cellSuprafata.setBackgroundColor(headerBg);
			cellSuprafata.add(new Paragraph(suprafata));
			cellSuprafata.setBorder(new SolidBorder(0.8f));
			cellSuprafata.setTextAlignment(TextAlignment.CENTER);
			cellSuprafata.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellSuprafata.setVerticalAlignment(VerticalAlignment.MIDDLE);

			tabel1.addCell(cellForma);
			tabel1.addCell(cellSuprafata);

			// rows
			Iterator<ApiaInfo> it = apiaInfo.iterator();
			Map<String, Double> formaSiSuprafata = new HashMap<String, Double>(apiaInfo.size());

			while (it.hasNext()) {
				ApiaInfo ai = it.next();
				if (!formaSiSuprafata.keySet().contains(ai.Forma())) {
					formaSiSuprafata.put(ai.Forma(), ai.SupfrafataUtilizata());
				} else {
					formaSiSuprafata.merge(ai.Forma(), ai.SupfrafataUtilizata(), Double::sum);
				}
			}

			Set<Entry<String, Double>> fit = formaSiSuprafata.entrySet();
			fit.forEach(item -> {
				Text formaText = new Text(item.getKey());
				Cell itemFormaCell = new Cell();
				itemFormaCell.add(new Paragraph(formaText));
				itemFormaCell.setBorder(new SolidBorder(0.8f));
				itemFormaCell.setTextAlignment(TextAlignment.CENTER);
				itemFormaCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemFormaCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				total1 += (item.getValue() / 10000d);
				Text suprafataText = new Text(String.valueOf((item.getValue() / 10000d)));
				Cell itemsuprafataCell = new Cell();
				itemsuprafataCell.add(new Paragraph(suprafataText));
				itemsuprafataCell.setBorder(new SolidBorder(0.8f));
				itemsuprafataCell.setTextAlignment(TextAlignment.CENTER);
				itemsuprafataCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemsuprafataCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				tabel1.addCell(itemFormaCell);
				tabel1.addCell(itemsuprafataCell);

			});
			Text totalText = new Text("Total");
			Cell itemTotalCell = new Cell();
			itemTotalCell.add(new Paragraph(totalText).simulateBold());
			itemTotalCell.setBorder(Border.NO_BORDER);
			itemTotalCell.setTextAlignment(TextAlignment.CENTER);
			itemTotalCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			itemTotalCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text totalSupText = new Text(String.valueOf(total1));
			Cell totalSupCell = new Cell();
			totalSupCell.add(new Paragraph(totalSupText).simulateBold());
			totalSupCell.setBorder(Border.NO_BORDER);
			totalSupCell.setTextAlignment(TextAlignment.CENTER);
			totalSupCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			totalSupCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

			tabel1.addCell(itemTotalCell);
			tabel1.addCell(totalSupCell);

			Table tabel2 = new Table(3);

			tabel2.setHorizontalAlignment(HorizontalAlignment.RIGHT);

			Text blocFizic = new Text("Bloc Fizic");
			Cell cellBlocFizic = new Cell();
			cellBlocFizic.setBackgroundColor(headerBg);
			cellBlocFizic.add(new Paragraph(blocFizic));
			cellBlocFizic.setBorder(new SolidBorder(0.8f));
			cellBlocFizic.setTextAlignment(TextAlignment.CENTER);
			cellBlocFizic.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellBlocFizic.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text categorieTeren = new Text("Categorie Teren");
			Cell cellCategorieTeren = new Cell();
			cellCategorieTeren.setBackgroundColor(headerBg);
			cellCategorieTeren.add(new Paragraph(categorieTeren));
			cellCategorieTeren.setBorder(new SolidBorder(0.8f));
			cellCategorieTeren.setTextAlignment(TextAlignment.CENTER);
			cellCategorieTeren.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellCategorieTeren.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text suprafata2 = new Text("Suprafata");
			Cell cellSuprafata2 = new Cell();
			cellSuprafata2.setBackgroundColor(headerBg);
			cellSuprafata2.add(new Paragraph(suprafata2));
			cellSuprafata2.setBorder(new SolidBorder(0.8f));
			cellSuprafata2.setTextAlignment(TextAlignment.CENTER);
			cellSuprafata2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellSuprafata2.setVerticalAlignment(VerticalAlignment.MIDDLE);

			tabel2.addCell(cellBlocFizic);
			tabel2.addCell(cellCategorieTeren);
			tabel2.addCell(cellSuprafata2);

			Iterator<ApiaInfo> it2 = apiaInfo.iterator();
			Map<String, Pair<String, Double>> blocFormaSuprafata = new HashMap<String, Pair<String, Double>>(
					apiaInfo.size());

			while (it2.hasNext()) {
				ApiaInfo ai2 = it2.next();
				String blocNull = "-";
				if (ai2.BlocFizic() != null)
					blocNull = ai2.BlocFizic().toString();
				if (!blocFormaSuprafata.keySet().contains(blocNull)) {
					blocFormaSuprafata.put(blocNull,
							new Pair<String, Double>(ai2.Categorie(), ai2.SupfrafataUtilizata()));
				} else {
					blocFormaSuprafata.merge(blocNull,
							new Pair<String, Double>(ai2.Categorie(), ai2.SupfrafataUtilizata()),
							(t, u) -> new Pair<String, Double>(u.getA(),
									u.getB().sum(u.getB(), ai2.SupfrafataUtilizata())));
				}
			}

			Set<Entry<String, Pair<String, Double>>> bcs = blocFormaSuprafata.entrySet();
			bcs.forEach(item -> {
				Text blocText = new Text(item.getKey());
				Cell itemblocCell = new Cell();
				itemblocCell.add(new Paragraph(blocText));
				itemblocCell.setBorder(new SolidBorder(0.8f));
				itemblocCell.setTextAlignment(TextAlignment.CENTER);
				itemblocCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemblocCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text categorieText = new Text(item.getValue().getA());
				Cell itemCategorieCell = new Cell();
				itemCategorieCell.add(new Paragraph(categorieText));
				itemCategorieCell.setBorder(new SolidBorder(0.8f));
				itemCategorieCell.setTextAlignment(TextAlignment.CENTER);
				itemCategorieCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemCategorieCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				double supInHA = item.getValue().getB() / 10000;
				Text suprafata2Text = new Text(String.valueOf(supInHA));
				Cell itemSuprafata2Cell = new Cell();
				itemSuprafata2Cell.add(new Paragraph(suprafata2Text));
				itemSuprafata2Cell.setBorder(new SolidBorder(0.8f));
				itemSuprafata2Cell.setTextAlignment(TextAlignment.CENTER);
				itemSuprafata2Cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemSuprafata2Cell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				tabel2.addCell(itemblocCell);
				tabel2.addCell(itemCategorieCell);
				tabel2.addCell(itemSuprafata2Cell);
			});

			Cell pt1 = new Cell();
			pt1.setBorder(Border.NO_BORDER);
			pt1.add(tabel1);
			rootTab1.addCell(pt1);

			Cell space1 = new Cell();
			space1.setBorder(Border.NO_BORDER);
			space1.add(new Paragraph(""));
			rootTab1.addCell(space1);

			Cell pt2 = new Cell();
			pt2.setWidth(width / 2.5f);
			pt2.setBorder(Border.NO_BORDER);
			pt2.add(tabel2);
			rootTab1.addCell(pt2);

			doc.add(rootTab1);
			doc.add(new Paragraph("\n"));

			// the big table

			Table tabel3 = new Table(10);
			tabel3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			float margins = doc.getLeftMargin() + doc.getRightMargin();
			tabel3.setWidth(width - margins);

			Text nrCrt = new Text("Nr. Crt");
			Cell cellNrCrt = new Cell();
			cellNrCrt.setBackgroundColor(headerBg);
			cellNrCrt.add(new Paragraph(nrCrt));
			cellNrCrt.setBorder(new SolidBorder(0.8f));
			cellNrCrt.setTextAlignment(TextAlignment.CENTER);
			cellNrCrt.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellNrCrt.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text utilizator3 = new Text("Utilizator");
			Cell cellUtilizator3 = new Cell();
			cellUtilizator3.setBackgroundColor(headerBg);
			cellUtilizator3.add(new Paragraph(utilizator3));
			cellUtilizator3.setBorder(new SolidBorder(0.8f));
			cellUtilizator3.setTextAlignment(TextAlignment.CENTER);
			cellUtilizator3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellUtilizator3.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text parcela = new Text("Parcela");
			Cell cellParcela = new Cell();
			cellParcela.setBackgroundColor(headerBg);
			cellParcela.add(new Paragraph(parcela));
			cellParcela.setBorder(new SolidBorder(0.8f));
			cellParcela.setTextAlignment(TextAlignment.CENTER);
			cellParcela.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellParcela.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text blocFizic2 = new Text("Bloc Fizic");
			Cell cellBlocFizic2 = new Cell();
			cellBlocFizic2.setBackgroundColor(headerBg);
			cellBlocFizic2.add(new Paragraph(blocFizic2));
			cellBlocFizic2.setBorder(new SolidBorder(0.8f));
			cellBlocFizic2.setTextAlignment(TextAlignment.CENTER);
			cellBlocFizic2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellBlocFizic2.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text suprafata3 = new Text("Suprafata");
			Cell cellSuprafata3 = new Cell();
			cellSuprafata3.setBackgroundColor(headerBg);
			cellSuprafata3.add(new Paragraph(suprafata3));
			cellSuprafata3.setBorder(new SolidBorder(0.8f));
			cellSuprafata3.setTextAlignment(TextAlignment.CENTER);
			cellSuprafata3.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellSuprafata3.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text forma2 = new Text("Forma");
			Cell cellForma2 = new Cell();
			cellForma2.setBackgroundColor(headerBg);
			cellForma2.add(new Paragraph(forma2));
			cellForma2.setBorder(new SolidBorder(0.8f));
			cellForma2.setTextAlignment(TextAlignment.CENTER);
			cellForma2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellForma2.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text categorie2 = new Text("Categorie Teren");
			Cell cellCategorie2 = new Cell();
			cellCategorie2.setBackgroundColor(headerBg);
			cellCategorie2.add(new Paragraph(categorie2));
			cellCategorie2.setBorder(new SolidBorder(0.8f));
			cellCategorie2.setTextAlignment(TextAlignment.CENTER);
			cellCategorie2.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellCategorie2.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text proprietar = new Text("Proprietar");
			Cell cellProprietar = new Cell();
			cellProprietar.setBackgroundColor(headerBg);
			cellProprietar.add(new Paragraph(proprietar));
			cellProprietar.setBorder(new SolidBorder(0.8f));
			cellProprietar.setTextAlignment(TextAlignment.CENTER);
			cellProprietar.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellProprietar.setVerticalAlignment(VerticalAlignment.MIDDLE);

			/*
			 * Text tipContract = new Text("Proprietar"); Cell cellTipContract = new Cell();
			 * cellTipContract.setBackgroundColor(headerBg); cellTipContract.add(new
			 * Paragraph(tipContract)); cellTipContract.setBorder(new SolidBorder(0.8f));
			 * cellTipContract.setTextAlignment(TextAlignment.CENTER);
			 * cellTipContract.setHorizontalAlignment(HorizontalAlignment.CENTER);
			 * cellTipContract.setVerticalAlignment(VerticalAlignment.MIDDLE);
			 */

			Text nr_dataContract = new Text("Nr./Data Contract");
			Cell cellNr_DataContract = new Cell();
			cellNr_DataContract.setBackgroundColor(headerBg);
			cellNr_DataContract.add(new Paragraph(nr_dataContract));
			cellNr_DataContract.setBorder(new SolidBorder(0.8f));
			cellNr_DataContract.setTextAlignment(TextAlignment.CENTER);
			cellNr_DataContract.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellNr_DataContract.setVerticalAlignment(VerticalAlignment.MIDDLE);

			Text expirareContract = new Text("Expirare Contract");
			Cell cellExpirareContract = new Cell();
			cellExpirareContract.setBackgroundColor(headerBg);
			cellExpirareContract.add(new Paragraph(expirareContract));
			cellExpirareContract.setBorder(new SolidBorder(0.8f));
			cellExpirareContract.setTextAlignment(TextAlignment.CENTER);
			cellExpirareContract.setHorizontalAlignment(HorizontalAlignment.CENTER);
			cellExpirareContract.setVerticalAlignment(VerticalAlignment.MIDDLE);

			tabel3.addCell(cellNrCrt);
			tabel3.addCell(cellUtilizator3);
			tabel3.addCell(cellParcela);
			tabel3.addCell(cellBlocFizic2);
			tabel3.addCell(cellSuprafata3);
			tabel3.addCell(cellForma2);
			tabel3.addCell(cellCategorie2);
			tabel3.addCell(cellProprietar);
			tabel3.addCell(cellNr_DataContract);
			tabel3.addCell(cellExpirareContract);

			Iterator<ApiaInfo> it3 = apiaInfo.iterator();

			while (it3.hasNext()) {
				ApiaInfo ai3 = it3.next();

				Text nrCrtItem = new Text(ai3.NrCrt().toString());
				Cell itemnrCrtCell = new Cell();
				itemnrCrtCell.setFontSize(9f);
				itemnrCrtCell.setKeepTogether(true);
				itemnrCrtCell.add(new Paragraph(nrCrtItem));
				itemnrCrtCell.setBorder(new SolidBorder(0.8f));
				itemnrCrtCell.setTextAlignment(TextAlignment.CENTER);
				itemnrCrtCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemnrCrtCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text UtilizatorItem = new Text(ai3.Utilizator());
				Cell itemUtilizatorCell = new Cell();
				itemUtilizatorCell.setFontSize(9f);
				itemUtilizatorCell.setKeepTogether(true);
				itemUtilizatorCell.add(new Paragraph(UtilizatorItem));
				itemUtilizatorCell.setBorder(new SolidBorder(0.8f));
				itemUtilizatorCell.setTextAlignment(TextAlignment.CENTER);
				itemUtilizatorCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemUtilizatorCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text parcelaItem = new Text(ai3.TarPar());
				Cell itemParcelaCell = new Cell();
				itemParcelaCell.setFontSize(9f);
				itemParcelaCell.setKeepTogether(true);
				itemParcelaCell.add(new Paragraph(parcelaItem));
				itemParcelaCell.setBorder(new SolidBorder(0.8f));
				itemParcelaCell.setTextAlignment(TextAlignment.CENTER);
				itemParcelaCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemParcelaCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text blocFizicItem = null;
				if (ai3.BlocFizic() == null)
					blocFizicItem = new Text("");
				else
					blocFizicItem = new Text(ai3.BlocFizic().toString());

				Cell itemBlocFizicCell = new Cell();
				itemBlocFizicCell.setFontSize(9f);
				itemBlocFizicCell.setKeepTogether(true);
				itemBlocFizicCell.add(new Paragraph(blocFizicItem));
				itemBlocFizicCell.setBorder(new SolidBorder(0.8f));
				itemBlocFizicCell.setTextAlignment(TextAlignment.CENTER);
				itemBlocFizicCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemBlocFizicCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				double supInHA2 = ai3.SupfrafataUtilizata() / 10000;
				Text suprafataItem = new Text(String.valueOf(supInHA2));
				Cell itemSuprafataCell = new Cell();
				itemSuprafataCell.setFontSize(9f);
				itemSuprafataCell.setKeepTogether(true);
				itemSuprafataCell.add(new Paragraph(suprafataItem));
				itemSuprafataCell.setBorder(new SolidBorder(0.8f));
				itemSuprafataCell.setTextAlignment(TextAlignment.CENTER);
				itemSuprafataCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemSuprafataCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text formaItem = new Text(ai3.Forma());
				Cell itemFormaCell = new Cell();
				itemFormaCell.setFontSize(9f);
				itemFormaCell.setKeepTogether(true);
				itemFormaCell.add(new Paragraph(formaItem));
				itemFormaCell.setBorder(new SolidBorder(0.8f));
				itemFormaCell.setTextAlignment(TextAlignment.CENTER);
				itemFormaCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemFormaCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text categorieTerenItem = new Text(ai3.Categorie());
				Cell itemCategorieTerenCell = new Cell();
				itemCategorieTerenCell.setFontSize(9f);
				itemCategorieTerenCell.setKeepTogether(true);
				itemCategorieTerenCell.add(new Paragraph(categorieTerenItem));
				itemCategorieTerenCell.setBorder(new SolidBorder(0.8f));
				itemCategorieTerenCell.setTextAlignment(TextAlignment.CENTER);
				itemCategorieTerenCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemCategorieTerenCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text proprietarItem = new Text(ai3.Proprietar());
				Cell itemProprietarCell = new Cell();
				itemProprietarCell.setFontSize(9f);
				itemProprietarCell.setKeepTogether(true);
				itemProprietarCell.add(new Paragraph(proprietarItem));
				itemProprietarCell.setBorder(new SolidBorder(0.8f));
				itemProprietarCell.setTextAlignment(TextAlignment.CENTER);
				itemProprietarCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemProprietarCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text nr_dataItem = new Text(ai3.NrDataContract());
				Cell itemNr_DataCell = new Cell();
				itemNr_DataCell.setFontSize(9f);
				itemNr_DataCell.setKeepTogether(true);
				itemNr_DataCell.add(new Paragraph(nr_dataItem));
				itemNr_DataCell.setBorder(new SolidBorder(0.8f));
				itemNr_DataCell.setTextAlignment(TextAlignment.CENTER);
				itemNr_DataCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemNr_DataCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				Text expirareContractItem = new Text(ai3.ExpirareContract());
				Cell itemExpirareContractCell = new Cell();
				itemExpirareContractCell.setFontSize(9f);
				itemExpirareContractCell.setKeepTogether(true);
				itemExpirareContractCell.add(new Paragraph(expirareContractItem));
				itemExpirareContractCell.setBorder(new SolidBorder(0.8f));
				itemExpirareContractCell.setTextAlignment(TextAlignment.CENTER);
				itemExpirareContractCell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				itemExpirareContractCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

				tabel3.addCell(itemnrCrtCell);
				tabel3.addCell(itemUtilizatorCell);
				tabel3.addCell(itemParcelaCell);
				tabel3.addCell(itemBlocFizicCell);
				tabel3.addCell(itemSuprafataCell);
				tabel3.addCell(itemFormaCell);
				tabel3.addCell(itemCategorieTerenCell);
				tabel3.addCell(itemProprietarCell);
				tabel3.addCell(itemNr_DataCell);
				tabel3.addCell(itemExpirareContractCell);

			}

			doc.add(tabel3);
			doc.add(new Paragraph("\n\n"));

			Table semnaturi = new Table(1);

			semnaturi.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph datasemn = new Paragraph();
			Text primar = new Text("Data");
			primar.setUnderline();
			datasemn.add(primar);
			datasemn.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));

			datasemn.add(new Tab());
			Text intocmit = new Text("Semnatura");
			intocmit.setUnderline();
			datasemn.add(intocmit);
			Cell cellsemn1 = new Cell();
			cellsemn1.setBorder(Border.NO_BORDER);
			cellsemn1.add(datasemn);
			semnaturi.addCell(cellsemn1);
			doc.add(semnaturi);

			Table semnaturiR = new Table(1);

			semnaturiR.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph semnR = new Paragraph();
			String dateNow = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			Text semnPrim = new Text(dateNow);
			semnR.add(semnPrim);
			semnR.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			semnR.add(new Tab());
			Text semnIntocmit = new Text(PDFHelper.getStrWithDash(18, ""));
			semnR.add(semnIntocmit);
			Cell cellsemn2 = new Cell();
			cellsemn2.setBorder(Border.NO_BORDER);
			cellsemn2.add(semnR);
			semnaturiR.addCell(cellsemn2);
			doc.add(semnaturiR);

			doc.close();
			document.close();
			writer.flush();

		} catch (Exception e) {

		}

	}

	public String getFileLoc() {

		// return fileLoc.substring(fileLoc.indexOf("META")-1, fileLoc.length());
		System.out.println("FILELOC: " + fileLoc);
		return fileLoc;
	}

	public String getFileName() {
		return fileName;
	}

}
