package com.spectral369.views.rapoarte.proprietari;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

public class RaportSimpluProprietari extends VerticalLayout {

	private static final long serialVersionUID = 2578351231621597521L;

	public RaportSimpluProprietari(ProprietariRepository proprietariRepo) {
		H2 title = new H2("Raport Simplu Proprietari");
		add(title);
		List<PROPRIETARI> list = proprietariRepo.findAll();
		Grid<PROPRIETARI> proprietariGrid = new Grid<PROPRIETARI>(PROPRIETARI.class, false);
		proprietariGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
		proprietariGrid.setDetailsVisibleOnClick(false);
		Grid.Column<PROPRIETARI> DenumireColumn = proprietariGrid.addColumn(PROPRIETARI::getNumeProprietar)
				.setTooltipGenerator(proprietar -> proprietar.getNumeProprietar()).setAutoWidth(true).setFlexGrow(0)
				.setFooter(String.format("Nr. Total Proprietari %s", list.size()));

		Grid.Column<PROPRIETARI> CNPCUIColumn = proprietariGrid.addColumn(PROPRIETARI::getCNP_CUI)
				.setTooltipGenerator(proprietar -> proprietar.getCNP_CUI()).setAutoWidth(true).setFlexGrow(0);
		Grid.Column<PROPRIETARI> LocalitateColumn = proprietariGrid.addColumn(PROPRIETARI::getADRESA_LOCALITATE);
		Grid.Column<PROPRIETARI> NrColumn = proprietariGrid.addColumn(PROPRIETARI::getADRESA_NUMAR);
		Grid.Column<PROPRIETARI> TelefonColumn = proprietariGrid.addColumn(PROPRIETARI::getTELEFON)
				.setTooltipGenerator(proprietar -> proprietar.getTELEFON());
		Grid.Column<PROPRIETARI> RolColumn = proprietariGrid.addColumn(PROPRIETARI::getROL);
		Grid.Column<PROPRIETARI> PF_PJColumn = proprietariGrid.addColumn(PROPRIETARI::getPF_PJ);
		Grid.Column<PROPRIETARI> ReprezentantColumn = proprietariGrid.addColumn(PROPRIETARI::getREPREZENTANT_PJ)
				.setTooltipGenerator(proprietar -> proprietar.getREPREZENTANT_PJ());
		Column<PROPRIETARI> details = proprietariGrid.addColumn(createToggleDetailsRenderer(proprietariGrid))
				.setAutoWidth(true);
		Column<PROPRIETARI> down = proprietariGrid.addColumn(createActionRenderer()).setFrozenToEnd(true)
				.setAutoWidth(true).setFlexGrow(0);

		GridListDataView<PROPRIETARI> dataView = proprietariGrid.setItems(list);

		ProprietariFilter proprietariFilter = new ProprietariFilter(dataView);

		proprietariGrid.getHeaderRows().clear();
		HeaderRow headerRow = proprietariGrid.appendHeaderRow();

		headerRow.getCell(DenumireColumn)
				.setComponent(createFilterHeader("Nume Proprietar", proprietariFilter::setNumeProprietar));
		headerRow.getCell(CNPCUIColumn).setComponent(createFilterHeader("CNP/CUI", proprietariFilter::setCNP_CUI));
		headerRow.getCell(LocalitateColumn)
				.setComponent(createFilterHeader("Localitate", proprietariFilter::setADRESA_LOCALITATE));
		headerRow.getCell(NrColumn).setComponent(createFilterHeader("Nr", proprietariFilter::setADRESA_NUMAR));
		headerRow.getCell(TelefonColumn).setComponent(createFilterHeader("Telefon", proprietariFilter::setTELEFON));
		headerRow.getCell(RolColumn).setComponent(createFilterHeader("ROL", proprietariFilter::setRol));
		headerRow.getCell(PF_PJColumn).setComponent(createFilterHeader("PF/PJ", proprietariFilter::setPF_PJ));
		headerRow.getCell(ReprezentantColumn)
				.setComponent(createFilterHeader("Reprezentant PJ", proprietariFilter::setREPREZENTANT_PJ));
		headerRow.getCell(details).setComponent(new Span("Detalii"));
		// headerRow.getCell(edit).setComponent(new Span("Editor"));
		headerRow.getCell(down).setComponent(new Span("Download"));
		proprietariGrid.setItemDetailsRenderer(createProprietariDetailsRenderer());
		new ProprietarContextMenu(proprietariGrid, proprietariRepo);

		add(proprietariGrid);

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private static ComponentRenderer<ProprietariDetailsFormLayout, PROPRIETARI> createProprietariDetailsRenderer() {
		return new ComponentRenderer<>(ProprietariDetailsFormLayout::new, ProprietariDetailsFormLayout::setProprietar);
	}

	private static Renderer<PROPRIETARI> createToggleDetailsRenderer(Grid<PROPRIETARI> grid) {
		return LitRenderer
				.<PROPRIETARI>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Detalii</vaadin-button>")
				.withFunction("handleClick",
						proprietari -> grid.setDetailsVisible(proprietari, !grid.isDetailsVisible(proprietari)));
	}

	private static Renderer<PROPRIETARI> createActionRenderer() {
		return LitRenderer
				.<PROPRIETARI>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Download</vaadin-button>")
				.withFunction("handleClick", t -> {
					if (t.getDOSAR() != null) {

						java.sql.Blob dosar = t.getDOSAR();
						try {
							InputStream is = dosar.getBinaryStream();

							File targetFile = new File(
									System.getProperty("user.home") + File.separatorChar + "dosar.pdf");
							OutputStream outStream = new FileOutputStream(targetFile);

							byte[] buffer = new byte[8 * 1024];
							int bytesRead;
							while ((bytesRead = is.read(buffer)) != -1) {
								outStream.write(buffer, 0, bytesRead);
							}
							IOUtils.closeQuietly(is);
							IOUtils.closeQuietly(outStream);

							final StreamResource resource = new StreamResource(targetFile.getName(), () -> {
								try {
									return new ByteArrayInputStream(FileUtils.readFileToByteArray(targetFile));
								} catch (IOException e) {
									System.out.println(e.getLocalizedMessage());
								}
								return is;
							});

							final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry()
									.registerResource(resource);
							UI.getCurrent().getPage().open(registration.getResourceUri().toString());

						} catch (Exception e) {
							System.out.println(e.getLocalizedMessage());
						}
					} else {
						Notification.show("Nothing to download", 2500, Position.BOTTOM_END);
					}
				});

	}

	private static Component createFilterHeader(String labelText, Consumer<String> filterChangeConsumer) {
		NativeLabel label = new NativeLabel(labelText);
		label.getStyle().set("padding-top", "var(--lumo-space-m)").set("font-size", "var(--lumo-font-size-xs)");
		TextField textField = new TextField();
		textField.setValueChangeMode(ValueChangeMode.EAGER);
		textField.setClearButtonVisible(true);
		textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
		textField.setWidthFull();
		textField.getStyle().set("max-width", "100%");
		textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
		VerticalLayout layout = new VerticalLayout(label, textField);
		layout.getThemeList().clear();
		layout.getThemeList().add("spacing-xs");

		return layout;
	}

	private static class ProprietariFilter {
		private final GridListDataView<PROPRIETARI> dataView;

		private String NumeProprietar;;
		private String CNP_CUI;
		private String ADRESA_LOCALITATE;
		private String ADRESA_NUMAR;
		private String TELEFON;
		private int ROL;
		private String PF_PJ;
		private String REPREZENTANT_PJ;

		public ProprietariFilter(GridListDataView<PROPRIETARI> dataView) {
			this.dataView = dataView;
			this.dataView.addFilter(this::test);
		}

		public void setNumeProprietar(String numeProprietar) {
			this.NumeProprietar = numeProprietar;
			this.dataView.refreshAll();
		}

		public void setCNP_CUI(String cnp_cui) {
			this.CNP_CUI = cnp_cui;
			this.dataView.refreshAll();
		}

		public void setADRESA_LOCALITATE(String localitate) {
			this.ADRESA_LOCALITATE = localitate;
			this.dataView.refreshAll();
		}

		public void setADRESA_NUMAR(String adresaNumar) {
			this.ADRESA_NUMAR = adresaNumar;
			this.dataView.refreshAll();
		}

		public void setTELEFON(String telefon) {
			this.TELEFON = telefon;
			this.dataView.refreshAll();
		}

		public void setRol(String rol) {
			try {
				this.ROL = Integer.parseInt(rol);
			} catch (Exception e) {
				this.ROL = 0;
			}
			this.dataView.refreshAll();
		}

		public void setPF_PJ(String pf_pj) {
			this.PF_PJ = pf_pj;
			this.dataView.refreshAll();
		}

		public void setREPREZENTANT_PJ(String reprezentant) {
			this.REPREZENTANT_PJ = reprezentant;
			this.dataView.refreshAll();
		}

		public boolean test(PROPRIETARI proprietar) {
			boolean matchesnumeProprietar = matches(proprietar.getNumeProprietar(), NumeProprietar);
			boolean matchesCNP_CUI = matches(proprietar.getCNP_CUI(), CNP_CUI);
			boolean matchesAresaLocalitate = matches(proprietar.getADRESA_LOCALITATE(), ADRESA_LOCALITATE);
			boolean matchesNr = matches(proprietar.getADRESA_NUMAR(), ADRESA_NUMAR);
			boolean matchesTelefon = matches(proprietar.getTELEFON(), TELEFON);
			boolean matchesRol = matchesInt(proprietar.getROL(), ROL);
			boolean matchesPfPj = matches(proprietar.getPF_PJ(), PF_PJ);
			boolean matchesReprezentPJ = matches(proprietar.getREPREZENTANT_PJ(), REPREZENTANT_PJ);

			return matchesnumeProprietar && matchesCNP_CUI && matchesAresaLocalitate && matchesNr && matchesTelefon
					&& matchesPfPj && matchesRol && matchesReprezentPJ;
		}

		private boolean matches(String value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesInt(int value, int searchTerm) {

			return searchTerm == 0 || value == 0
					|| String.valueOf(value).toLowerCase().contains(String.valueOf(searchTerm));
		}
	}

	private static class ProprietariDetailsFormLayout extends FormLayout {
		private static final long serialVersionUID = 6425872388237011077L;
		private final IntegerField ID = new IntegerField("ID");
		private final TextField NumeProprietar = new TextField("Nume Proprietar");
		private final TextField CNP_CUI = new TextField("CNP/CUI");
		private final TextField LOCALITATE = new TextField("LOCALITATE");
		private final TextField STRADA = new TextField("STRADA");
		private final TextField NUMAR = new TextField("NUMAR");
		private final TextField BLOC = new TextField("BLOC");
		private final TextField ETAJ = new TextField("ETAJ");
		private final TextField APARTAMENT = new TextField("APARTAMENT");
		private final TextField TELEFON = new TextField("TELEFON");
		private final IntegerField ROL = new IntegerField("ROL");
		private final TextField PF_PJ = new TextField("PF/PJ");
		private final TextField RepPJ = new TextField("Reprezentant PJ");

		/*
		 * FileBuffer FileMemoryBuffer = new FileBuffer(); Upload upload = new
		 * Upload(FileMemoryBuffer);
		 */

		public ProprietariDetailsFormLayout() {
			add(ID, NumeProprietar, CNP_CUI, LOCALITATE, STRADA, NUMAR, BLOC, ETAJ, APARTAMENT, TELEFON, ROL, PF_PJ);
			NumeProprietar.setReadOnly(true);
			CNP_CUI.setReadOnly(true);
			LOCALITATE.setReadOnly(true);
			STRADA.setReadOnly(true);
			NUMAR.setReadOnly(true);
			BLOC.setReadOnly(true);
			ETAJ.setReadOnly(true);
			APARTAMENT.setReadOnly(true);
			TELEFON.setReadOnly(true);
			ROL.setReadOnly(true);
			PF_PJ.setReadOnly(true);
			RepPJ.setReadOnly(true);
			setResponsiveSteps(new ResponsiveStep("0", 3));
			ID.setEnabled(false);

		}

		public void setProprietar(final PROPRIETARI proprietari) {
			ID.setValue(proprietari.getID());

			NumeProprietar.setValue(proprietari.getNumeProprietar());
			CNP_CUI.setValue(proprietari.getCNP_CUI());

			LOCALITATE.setValue(proprietari.getADRESA_LOCALITATE());
			if (!(proprietari.getADRESA_STRADA() == null))
				STRADA.setValue(proprietari.getADRESA_STRADA());
			if (!(proprietari.getADRESA_NUMAR() == null))
				NUMAR.setValue(proprietari.getADRESA_NUMAR());
			if (!(proprietari.getADRESA_BLOC() == null))
				BLOC.setValue(proprietari.getADRESA_BLOC());
			if (!(proprietari.getADRESA_ETAJ() == null))
				ETAJ.setValue(proprietari.getADRESA_ETAJ());
			if (!(proprietari.getADRESA_APARTAMENT() == null))
				APARTAMENT.setValue(proprietari.getADRESA_APARTAMENT());
			if (!(proprietari.getADRESA_STRADA() == null))
				TELEFON.setValue(proprietari.getTELEFON());
			if (!(proprietari.getROL() <= 0))
				ROL.setValue(proprietari.getROL());
			if (!(proprietari.getPF_PJ().isEmpty() || proprietari.getPF_PJ().isBlank()))
				PF_PJ.setValue(proprietari.getPF_PJ());
			if (!(proprietari.getREPREZENTANT_PJ() == null))
				RepPJ.setValue(proprietari.getREPREZENTANT_PJ());

		}

	}

	private static class ProprietarContextMenu extends GridContextMenu<PROPRIETARI> {
		private static final long serialVersionUID = 8883017315048714541L;
		private InputStream fileData;

		public ProprietarContextMenu(Grid<PROPRIETARI> target, ProprietariRepository proprietariRepo) {
			super(target);

			addItem("Edit", e -> e.getItem().ifPresent(proprietar -> {
				Dialog editDialog = new Dialog();
				editDialog.setHeaderTitle("Update Proprietar");

				TextField numeProprietar = new TextField("Nume Proprietar");
				numeProprietar.setValue(proprietar.getNumeProprietar());
				numeProprietar.setRequiredIndicatorVisible(true);
				numeProprietar.setErrorMessage("Nume proprietar required!");

				TextField cnp_cuiProprietar = new TextField("CNP/CUI Properietar");
				cnp_cuiProprietar.setValue(proprietar.getCNP_CUI());
				cnp_cuiProprietar.setRequiredIndicatorVisible(true);
				cnp_cuiProprietar.setErrorMessage("CNP/CUI required!");

				TextField localitateProprietar = new TextField("Localitate Proprietar");
				localitateProprietar.setValue(proprietar.getADRESA_LOCALITATE());
				localitateProprietar.setRequiredIndicatorVisible(true);
				localitateProprietar.setErrorMessage("Localitate required!");

				TextField stradaProprietar = new TextField("Strada proprietar");
				if (proprietar.getADRESA_STRADA() != null)
					stradaProprietar.setValue(proprietar.getADRESA_STRADA());

				TextField nrProprietar = new TextField("Numar Adresa Proprietar");
				nrProprietar.setValue(proprietar.getADRESA_NUMAR());
				nrProprietar.setRequiredIndicatorVisible(true);
				nrProprietar.setErrorMessage("Nr. Adresa required!");

				TextField blocProprietar = new TextField("Bloc Proprietar");
				if (proprietar.getADRESA_BLOC() != null)
					blocProprietar.setValue(proprietar.getADRESA_BLOC());

				TextField etajProprietar = new TextField("Etaj Proprietar");
				if (proprietar.getADRESA_ETAJ() != null)
					etajProprietar.setValue(proprietar.getADRESA_ETAJ());

				TextField apartamentProprietar = new TextField("Apartament Proprietar");
				if (proprietar.getADRESA_APARTAMENT() != null)
					apartamentProprietar.setValue(proprietar.getADRESA_APARTAMENT());

				TextField telefonProprietar = new TextField("Telefon Proprietar");
				telefonProprietar.setValue(proprietar.getTELEFON());
				telefonProprietar.setRequiredIndicatorVisible(true);
				telefonProprietar.setErrorMessage("Telefon required!");

				IntegerField rolProprietar = new IntegerField("Rol Proprietar");
				if (proprietar.getROL() != 0)
					rolProprietar.setValue(proprietar.getROL());

				TextField pf_pjProprietar = new TextField("PF/PJ");
				pf_pjProprietar.setValue(proprietar.getPF_PJ());
				pf_pjProprietar.setEnabled(false);

				TextField reprezentantProprietar = new TextField("Reprezentant PJ");
				if (proprietar.getREPREZENTANT_PJ() != null)
					reprezentantProprietar.setValue(proprietar.getREPREZENTANT_PJ());

				FileBuffer FileMemoryBuffer = new FileBuffer();
				Upload upload = new Upload(FileMemoryBuffer);
				Button uploadButton = new Button("Upload PDF...");
				uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				upload.setUploadButton(uploadButton);
				upload.setAcceptedFileTypes("application/pdf", ".pdf");
				upload.setMaxFiles(1);
				upload.setDropAllowed(false);
				int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
				upload.setMaxFileSize(maxFileSizeInBytes);
				upload.setMinWidth(15, Unit.EM);

				upload.addSucceededListener(event -> {

					fileData = FileMemoryBuffer.getInputStream();
					String fileName = event.getFileName();

					String mimeType = event.getMIMEType();

					System.out.println(fileName + " " + mimeType);
				});
				upload.addFileRejectedListener(event -> {
					String errorMessage = event.getErrorMessage();

					Notification notification = Notification.show(errorMessage, 5000,
							Notification.Position.BOTTOM_CENTER);
					notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
				});

				upload.getElement().addEventListener("max-files-reached-changed", event -> {
					boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
					uploadButton.setEnabled(!maxFilesReached);
				}).addEventData("event.detail.value");

				FormLayout editLayout = new FormLayout();
				editLayout.setResponsiveSteps(

						new ResponsiveStep("0", 1),

						new ResponsiveStep("500px", 2));

				VerticalLayout dialogEditLayout = new VerticalLayout(editLayout);
				editLayout.add(numeProprietar, cnp_cuiProprietar, localitateProprietar, stradaProprietar, nrProprietar,
						blocProprietar, etajProprietar, apartamentProprietar, telefonProprietar, rolProprietar,
						pf_pjProprietar, reprezentantProprietar, upload);
				dialogEditLayout.setAlignItems(Alignment.CENTER);
				dialogEditLayout.setJustifyContentMode(JustifyContentMode.CENTER);
				dialogEditLayout.setPadding(false);
				dialogEditLayout.setSpacing(false);

				editDialog.add(dialogEditLayout);
				Button saveButton = new Button("Update!");
				saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				saveButton.addClickListener(evt -> {

					if (!numeProprietar.getValue().isEmpty()
							&& proprietar.getNumeProprietar() != numeProprietar.getValue().trim())
						proprietar.setNumeProprietar(numeProprietar.getValue().trim());
					if (!cnp_cuiProprietar.getValue().isEmpty()
							&& proprietar.getCNP_CUI() != cnp_cuiProprietar.getValue().trim())
						proprietar.setCNP_CUI(cnp_cuiProprietar.getValue().trim());
					if (!localitateProprietar.getValue().isEmpty()
							&& proprietar.getADRESA_LOCALITATE() != localitateProprietar.getValue().trim())
						proprietar.setADRESA_LOCALITATE(localitateProprietar.getValue().trim());
					if (!stradaProprietar.getValue().isEmpty()
							&& proprietar.getADRESA_STRADA() != stradaProprietar.getValue().trim())
						proprietar.setADRESA_STRADA(stradaProprietar.getValue().trim());
					if (!nrProprietar.getValue().isEmpty()
							&& proprietar.getADRESA_NUMAR() != nrProprietar.getValue().trim())
						proprietar.setADRESA_NUMAR(nrProprietar.getValue().trim());
					if (!blocProprietar.getValue().isEmpty()
							&& proprietar.getADRESA_BLOC() != blocProprietar.getValue().trim())
						proprietar.setADRESA_BLOC(blocProprietar.getValue().trim());
					if (!etajProprietar.getValue().isEmpty()
							&& proprietar.getADRESA_ETAJ() != etajProprietar.getValue().trim())
						proprietar.setADRESA_ETAJ(etajProprietar.getValue().trim());

					if (!apartamentProprietar.getValue().isEmpty()
							&& proprietar.getADRESA_APARTAMENT() != apartamentProprietar.getValue().trim())
						proprietar.setADRESA_APARTAMENT(apartamentProprietar.getValue().trim());

					if (!telefonProprietar.getValue().isEmpty()
							&& proprietar.getTELEFON() != telefonProprietar.getValue().trim())
						proprietar.setTELEFON(telefonProprietar.getValue().trim());
					if (rolProprietar.getValue() == 0 && proprietar.getROL() != rolProprietar.getValue())
						proprietar.setROL(rolProprietar.getValue());

					if (!pf_pjProprietar.getValue().isEmpty()
							&& proprietar.getPF_PJ() != pf_pjProprietar.getValue().trim())
						proprietar.setPF_PJ(pf_pjProprietar.getValue().trim());
					if (!reprezentantProprietar.getValue().isEmpty()
							&& proprietar.getREPREZENTANT_PJ() != reprezentantProprietar.getValue().trim())
						proprietar.setREPREZENTANT_PJ(reprezentantProprietar.getValue().trim());
					if (fileData != null) {
						Blob fd = null;
						try {
							fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

								@Override
								public Exception interceptException(Exception sqlEx) {
									System.out.println("Erroare1 la upload PJ!");
									return null;
								}

								@Override
								public ExceptionInterceptor init(Properties props, Log log) {
									System.out.println("Erroare2 la upload PJ!");
									return null;
								}

								@Override
								public void destroy() {
									System.out.println("Erroare3 la upload PJ!");

								}
							});
						} catch (IOException ex) {
							System.out.println(ex.getMessage());
						}

						proprietar.setDOSAR(fd);
					}
					proprietariRepo.saveAndFlush(proprietar);
					target.getDataProvider().refreshAll();
					editDialog.close();

				});
				Button cancelButton = new Button("Cancel", evt -> editDialog.close());
				editDialog.getFooter().add(cancelButton);
				editDialog.getFooter().add(saveButton);
				editDialog.open();

			}));
			addItem("Delete", e -> e.getItem().ifPresent(proprietar -> {
				ConfirmDialog deleteDialog = new ConfirmDialog();
				deleteDialog.setHeader("Stergere Proprietar");
				deleteDialog.setText("Sigur doriti sa stergeti " + proprietar.getNumeProprietar() + " ?");
				deleteDialog.setCancelable(true);
				// deleteDialog.setRejectable(true);
				deleteDialog.setConfirmText("Delete");
				deleteDialog.setConfirmButtonTheme("error primary");
				deleteDialog.addConfirmListener(event -> {
					proprietariRepo.delete(proprietar);
					target.getDataProvider().refreshAll();
					this.close();
				});
				deleteDialog.open();

			}));

			add(new Hr());

			GridMenuItem<PROPRIETARI> telefonItem = addItem("Telefon", e -> e.getItem().ifPresent(proprietar -> {
				UI.getCurrent().getPage().executeJs("navigator.clipboard.writeText(" + proprietar.getTELEFON() + ")");
			}));
			GridMenuItem<PROPRIETARI> CNPItem = addItem("CNP", e -> e.getItem().ifPresent(proprietar -> {

				UI.getCurrent().getPage().executeJs("navigator.clipboard.writeText(" + proprietar.getCNP_CUI() + ")");
			}));

			setDynamicContentHandler(proprietar -> {
				// Do not show context menu when header is clicked
				if (proprietar == null)
					return false;
				telefonItem.setText(String.format("Telefon: %s", proprietar.getTELEFON()));
				CNPItem.setText(String.format("CNP: %s", proprietar.getCNP_CUI()));
				return true;
			});
		}

	}
}
