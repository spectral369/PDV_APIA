package com.spectral369.views.rapoarte.Apia1Raport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import com.spectral369.db.apia.ApiaInfo;
import com.spectral369.db.contracte.CONTRACTE;
import com.spectral369.db.contracte.ContracteRepository;
import com.spectral369.db.fermieri.FERMIERI;
import com.spectral369.db.fermieri.FermieriRepository;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.spectral369.db.terenuri.TERENURI;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.spectral369.utils.Utils;
import com.spectral369.views.rapoarte.pdfs.AdeverintaApia;
import com.spectral369.views.rapoarte.pdfs.RaportApiaPDF;
import com.vaadin.componentfactory.pdfviewer.PdfViewer;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamResource;

public class RaportApia extends VerticalLayout {

	private static final long serialVersionUID = -3052618892118023015L;

	private FERMIERI fermier;
	private HashSet<ApiaInfo> terenuriFermier;
	private HashSet<ApiaInfo> terenuriFermierPDF;
	private RaportApiaPDF pdf = null;
	private AdeverintaApia pdfADV = null;

	public RaportApia(ProprietariRepository proprietariRepo, FermieriRepository fermierRepo,
			TerenuriRepository terenuriRepo, ContracteRepository contracteRepo) {

		H2 title = new H2("Raport Apia");
		add(title);
		Dialog fermierDialog = new Dialog();
		fermierDialog.setHeaderTitle("Alege Fermierul");
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setAlignItems(Alignment.CENTER);
		contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setAlignItems(Alignment.CENTER);
		searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
		TextField searchFermierField = new TextField("Fermier");
		searchFermierField.setPlaceholder("CNP");
		searchFermierField.setHelperText("Alege fermierul, mai baietule mai !");
		searchFermierField.setRequiredIndicatorVisible(true);
		searchFermierField.setValueChangeMode(ValueChangeMode.TIMEOUT);
		searchFermierField.setValueChangeTimeout(1000);
		searchFermierField.setWidth("100%");
		searchFermierField.setMaxLength(14);
		searchFermierField.focus();

		ListBox<String> fermieriListBox = new ListBox<String>();
		List<FERMIERI> fermieriList = new LinkedList<FERMIERI>();
		List<String> fermieriNume = new LinkedList<String>();

		Button searchFermier = new Button("Cauta");
		searchFermier.setEnabled(false);
		searchFermier.addClickListener(event -> {
			fermieriList.clear();
			fermieriListBox.clear();
			fermieriList.addAll(fermierRepo.findByCNP_CUIContainingIgnoreCase(searchFermierField.getValue().trim()));
			for (FERMIERI f : fermieriList) {
				fermieriNume.clear();
				fermieriNume.add(f.getID() + ":" + f.getDENUMIRE_FERMIER());
			}
			fermieriListBox.setItems(fermieriNume);
		});

		searchFermierField.addValueChangeListener(vcl -> {
			if (!searchFermierField.isEmpty() && searchFermierField.getValue().trim().length() > 6) {
				searchFermier.setEnabled(true);
			} else {
				searchFermier.setEnabled(false);
			}
		});
		searchLayout.add(searchFermierField, searchFermier);
		contentLayout.add(searchLayout, fermieriListBox);
		fermierDialog.add(contentLayout);

		Button ok = new Button("OK");
		ok.setEnabled(false);
		ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		ok.getStyle().set("margin-right", "auto");
		ok.addClickListener(evt -> {
			int pid = Integer
					.parseInt(fermieriListBox.getValue().substring(0, fermieriListBox.getValue().indexOf(":")));
			fermier = fermierRepo.findByIDIgnoreCase(pid);
			title.setText("Raport Apia - " + fermier.getDENUMIRE_FERMIER());
			fermierDialog.close();
			createStuff(proprietariRepo, fermierRepo, terenuriRepo, contracteRepo);

		});
		fermieriListBox.addValueChangeListener(evel -> {
			if (fermieriListBox.getValue() != null)
				ok.setEnabled(true);
			else
				ok.setEnabled(false);
		});
		Button cancel = new Button("Cancel");
		cancel.addClickListener(canc -> {
			fermierDialog.close();
			fermierDialog.getParent().get().removeFromParent();
		});
		cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		fermierDialog.getFooter().add(ok);
		fermierDialog.getFooter().add(cancel);
		fermierDialog.setModal(true);
		fermierDialog.setCloseOnEsc(true);
		fermierDialog.setCloseOnOutsideClick(false);
		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
			fermierDialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
			fermierDialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

		});

		add(fermierDialog);
		fermierDialog.open();

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");

	}

	private void createStuff(ProprietariRepository proprietariRepo, FermieriRepository fermierRepo,
			TerenuriRepository terenuriRepo, ContracteRepository contracteRepo) {
		HorizontalLayout content = new HorizontalLayout();
		FormLayout formLayout1 = new FormLayout();
		content.add(formLayout1);

		System.out.println("NUME: " + fermier.getDENUMIRE_FERMIER());
		List<CONTRACTE> contracteFermieri = contracteRepo.getContracteByFermier(fermier);
		terenuriFermier = new HashSet<ApiaInfo>();

		for (CONTRACTE c : contracteFermieri) {
			for (TERENURI t : c.getID_TEREN()) {
				StringBuilder sbProp = new StringBuilder();
				for (PROPRIETARI p : c.getProprietarID()) {
					sbProp.append(p.getNumeProprietar());
					sbProp.append("\n");
				}

				ApiaInfo ap = new ApiaInfo(c.getID(), c.getFERMIER().getDENUMIRE_FERMIER(), t.getTarPar(),
						t.getBlocFizic(), t.getSuprafataUtilizata(), c.getTIP_CONTRACT(), t.getCategorie(),
						sbProp.toString(),
						String.valueOf(c.getNUMAR_CONTRACT())
								.concat(" / " + c.getDATA_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER)),
						c.getDATA_SFARSIT_CONTRACT().toLocalDate().format(Utils.DATEFORMATTER),
						c.getFERMIER().getCNP_CUI(), c.getFERMIER().getADRESA_LOCALITATE(),
						c.getFERMIER().getADRESA_NUMAR(), c.getFERMIER().getADRESA_JUDET(), c.getFERMIER().getTELEFON(),
						c.getProprietarID().get(0).getROL(), c.getFERMIER().getVOLUM(), c.getFERMIER().getPOZITIE(),
						c.getProprietarID().get(0).getPF_PJ());
				terenuriFermier.add(ap);

			}

		}

		Grid<ApiaInfo> apiaGrid = new Grid<ApiaInfo>(ApiaInfo.class, false);
		apiaGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
		apiaGrid.setDetailsVisibleOnClick(false);
		apiaGrid.setSelectionMode(Grid.SelectionMode.MULTI);
		terenuriFermierPDF = new HashSet<ApiaInfo>();

		Grid.Column<ApiaInfo> ID = apiaGrid.addColumn(ApiaInfo::NrCrt);
		Grid.Column<ApiaInfo> utilizator = apiaGrid.addColumn(ApiaInfo::Utilizator).setFlexGrow(0).setAutoWidth(true);
		Grid.Column<ApiaInfo> tarPar = apiaGrid.addColumn(ApiaInfo::TarPar);
		Grid.Column<ApiaInfo> blocFizic = apiaGrid.addColumn(ApiaInfo::BlocFizic);
		Grid.Column<ApiaInfo> suprafataUtilizata = apiaGrid.addColumn(ApiaInfo::SupfrafataUtilizata);
		Grid.Column<ApiaInfo> forma = apiaGrid.addColumn(ApiaInfo::Forma);
		Grid.Column<ApiaInfo> categorie = apiaGrid.addColumn(ApiaInfo::Categorie);
		Grid.Column<ApiaInfo> proprietari = apiaGrid.addColumn(ApiaInfo::Proprietar);
		Grid.Column<ApiaInfo> nrDataContract = apiaGrid.addColumn(ApiaInfo::NrDataContract);
		Grid.Column<ApiaInfo> expirareContract = apiaGrid.addColumn(ApiaInfo::ExpirareContract);

		GridListDataView<ApiaInfo> dataView = apiaGrid.setItems(terenuriFermier);

		ApiaFilter apiaFilter = new ApiaFilter(dataView);
		apiaGrid.getHeaderRows().clear();

		HeaderRow headerRow = apiaGrid.appendHeaderRow();
		headerRow.getCell(ID).setComponent(createFilterHeader("Nr. Contract", apiaFilter::setNrCrt));
		headerRow.getCell(utilizator).setComponent(createFilterHeader("Utilizatorr", apiaFilter::setUtilizator));
		headerRow.getCell(tarPar).setComponent(createFilterHeader("Tar/Par", apiaFilter::setTarPar));
		headerRow.getCell(blocFizic).setComponent(createFilterHeader("Bloc Fizic", apiaFilter::setBlocFizic));
		headerRow.getCell(suprafataUtilizata)
				.setComponent(createFilterHeader("Suprafata(Utilizata)", apiaFilter::setSuprafataUtilizata));
		headerRow.getCell(forma).setComponent(createFilterHeader("Forma", apiaFilter::setForma));
		headerRow.getCell(categorie).setComponent(createFilterHeader("Categorie", apiaFilter::setCatagorie));
		headerRow.getCell(proprietari).setComponent(createFilterHeader("Proprietari", apiaFilter::setProprietar));
		headerRow.getCell(nrDataContract)
				.setComponent(createFilterHeader("Nr/Data Contract", apiaFilter::setNrDataContract));
		headerRow.getCell(expirareContract)
				.setComponent(createFilterHeader("Data Expirare Contract", apiaFilter::setExpirareContract));

		apiaGrid.addSelectionListener(selection -> {
			if (selection.getAllSelectedItems().size() > 0) {
				content.setEnabled(true);
				terenuriFermierPDF.addAll(selection.getAllSelectedItems());
			} else {
				content.setEnabled(false);
			}
		});

		apiaGrid.setPartNameGenerator(contract -> {
			return "spacewrap";
		});

		Button GenerateCerere = new Button("Generare Cerere");

		Button GenerareFisaApia = new Button("Generare Adeverinta");
		GenerareFisaApia.addClickListener(evt -> {
			Dialog viewPDF = new Dialog();

			pdfADV = new AdeverintaApia(terenuriFermier);

			viewPDF.setCloseOnEsc(true);
			viewPDF.setCloseOnOutsideClick(false);
			viewPDF.setSizeFull();

			PdfViewer pdfViewer = new PdfViewer();
			pdfViewer.setHeight("100%");

			final StreamResource pdfResource = new StreamResource(pdfADV.getFileName(), () -> {
				try {
					return new FileInputStream(new File(pdfADV.getFileLoc()));
				} catch (final FileNotFoundException e) {
					e.printStackTrace();
					return null;
				}
			});

			pdfViewer.setSrc(pdfResource);
			pdfViewer.setAddPrintButton(true);
			viewPDF.add(pdfViewer);

			this.add(viewPDF);
			HorizontalLayout footer = new HorizontalLayout();
			footer.setAlignItems(Alignment.CENTER);
			footer.setJustifyContentMode(JustifyContentMode.CENTER);

			Button viewClose = new Button("Close");
			viewClose.addClickListener(eventt -> {
				viewPDF.close();
			});
			viewClose.getStyle().set("margin-inline-middle", "auto");
			footer.getStyle().set("flex-wrap", "wrap");
			footer.setSizeFull();
			footer.add(viewClose);
			viewPDF.getFooter().add(footer);
			viewPDF.open();
		});

		GenerateCerere.addClickListener(evnt -> {

			Dialog viewPDF = new Dialog();

			pdf = new RaportApiaPDF(terenuriFermierPDF);

			viewPDF.setCloseOnEsc(true);
			viewPDF.setSizeFull();

			PdfViewer pdfViewer = new PdfViewer();
			pdfViewer.setHeight("100%");

			final StreamResource pdfResource = new StreamResource(pdf.getFileName(), () -> {
				try {
					return new FileInputStream(new File(pdf.getFileLoc()));
				} catch (final FileNotFoundException e) {
					e.printStackTrace();
					return null;
				}
			});

			pdfViewer.setSrc(pdfResource);
			pdfViewer.setAddPrintButton(true);
			viewPDF.add(pdfViewer);

			this.add(viewPDF);
			HorizontalLayout footer = new HorizontalLayout();
			footer.setAlignItems(Alignment.CENTER);
			footer.setJustifyContentMode(JustifyContentMode.CENTER);

			Button viewClose = new Button("Close");
			viewClose.addClickListener(eventt -> {
				viewPDF.close();
			});
			viewClose.getStyle().set("margin-inline-middle", "auto");
			footer.getStyle().set("flex-wrap", "wrap");
			footer.setSizeFull();
			footer.add(viewClose);
			viewPDF.getFooter().add(footer);
			viewPDF.open();

		});
		content.add(GenerateCerere, GenerareFisaApia);
		content.setEnabled(false);
		add(apiaGrid);
		add(content);
	}

	private static class ApiaFilter {
		private final GridListDataView<ApiaInfo> dataView;

		private Integer NrCrt;
		private String Utilizator;
		private String TarPar;
		private Integer BlocFizic;
		private Integer SuprafataUtilizata;
		private String Forma;
		private String Categorie;
		private String Proprietar;
		private String NrDataContract;
		private String ExpirareContract;

		public ApiaFilter(GridListDataView<ApiaInfo> dataView) {
			this.dataView = dataView;
			this.dataView.addFilter(this::test);
		}

		public void setNrCrt(String NrCrt) {
			try {
				this.NrCrt = Integer.parseInt(NrCrt);
			} catch (Exception e) {
				this.NrCrt = null;
			}

			this.dataView.refreshAll();
		}

		public void setUtilizator(String Utilizator) {
			this.Utilizator = Utilizator;
			this.dataView.refreshAll();
		}

		public void setTarPar(String tarPar) {
			this.TarPar = tarPar;
			dataView.refreshAll();
		}

		public void setBlocFizic(String BlocFizic) {
			try {
				this.BlocFizic = Integer.parseInt(BlocFizic);
			} catch (Exception e) {
				this.BlocFizic = null;
			}

			dataView.refreshAll();
		}

		public void setSuprafataUtilizata(String SUprafataUtilizata) {
			try {
				this.SuprafataUtilizata = Integer.parseInt(SUprafataUtilizata);
			} catch (Exception e) {
				this.SuprafataUtilizata = null;
			}
			dataView.refreshAll();
		}

		public void setForma(String Forma) {
			this.Forma = Forma;
			dataView.refreshAll();
		}

		public void setCatagorie(String Categorie) {
			this.Categorie = Categorie;
			dataView.refreshAll();
		}

		public void setProprietar(String Proprietar) {
			this.Proprietar = Proprietar;
			dataView.refreshAll();
		}

		public void setNrDataContract(String NrDataContract) {
			this.NrDataContract = NrDataContract;
			dataView.refreshAll();
		}

		public void setExpirareContract(String ExpirareContract) {
			this.ExpirareContract = ExpirareContract;
			dataView.refreshAll();
		}

		public boolean test(ApiaInfo apiaInfo) {
			boolean matchesNrCrt = matchesInt(Integer.valueOf(apiaInfo.NrCrt()), NrCrt);
			boolean matchesUtilizator = matches(apiaInfo.Utilizator(), Utilizator);
			boolean matchesTarPar = matches(apiaInfo.TarPar(), TarPar);

			boolean matchesBlocFizic = matchesInt(apiaInfo.BlocFizic(), BlocFizic);

			boolean matchesSuprafataUtilizata = matchesInt(apiaInfo.SupfrafataUtilizata().intValue(),
					SuprafataUtilizata);
			boolean matchesForma = matches(apiaInfo.Forma(), Forma);
			boolean matchesCategorie = matches(apiaInfo.Categorie(), Categorie);
			boolean matchesProprietar = matches(apiaInfo.Proprietar(), Proprietar);
			boolean matchesNrDataContract = matches(apiaInfo.NrDataContract(), NrDataContract);
			boolean matchesExpirareContract = matches(apiaInfo.ExpirareContract(), ExpirareContract);

			return matchesNrCrt && matchesUtilizator && matchesTarPar && matchesBlocFizic && matchesSuprafataUtilizata
					&& matchesForma && matchesCategorie && matchesProprietar && matchesNrDataContract
					&& matchesExpirareContract;

		}

		private boolean matches(String value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesInt(Integer value, Integer searchTerm) {
			return searchTerm == null || value == null
					|| String.valueOf(value).toLowerCase().contains(String.valueOf(searchTerm));
		}

	}

	private static Component createFilterHeader(String labelText, Consumer<String> filterChangeConsumer) {
		NativeLabel label = new NativeLabel(labelText);
		label.getStyle().set("padding-top", "var(--lumo-space-m)").set("font-size", "var(--lumo-font-size-xs)");
		TextField textField = new TextField();
		textField.setValueChangeMode(ValueChangeMode.EAGER);
		textField.setClearButtonVisible(true);
		textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
		textField.setWidthFull();
		textField.getStyle().set("max-width", "100%");
		textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
		VerticalLayout layout = new VerticalLayout(label, textField);
		layout.getThemeList().clear();
		layout.getThemeList().add("spacing-xs");

		return layout;
	}

}
