package com.spectral369.views.rapoarte.terenuri;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.terenuri.TERENURI;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

public class RaportSimpluTerenuri extends VerticalLayout {

	private static final long serialVersionUID = 2578351231621597521L;

	public RaportSimpluTerenuri(TerenuriRepository terenuriRepo) {
		H2 title = new H2("Raport Simplu Terenuri");
		add(title);
		List<TERENURI> list = terenuriRepo.findAll();
		Grid<TERENURI> terenuriGrid = new Grid<TERENURI>(TERENURI.class, false);
		terenuriGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
		terenuriGrid.setDetailsVisibleOnClick(false);
		Grid.Column<TERENURI> NumeColumn = terenuriGrid.addColumn(RaportSimpluTerenuri::generateNumeProprietar)
				.setTooltipGenerator(teren -> teren.getPoprietarID().getNumeProprietar()).setAutoWidth(true)
				.setFlexGrow(0).setFooter(String.format("Nr. Total Terenuri %s", list.size()));

		Grid.Column<TERENURI> TarParColumn = terenuriGrid.addColumn(TERENURI::getTarPar)
				.setTooltipGenerator(teren -> teren.getTarPar()).setAutoWidth(true).setFlexGrow(0);
		Grid.Column<TERENURI> CFColumn = terenuriGrid.addColumn(TERENURI::getCF);
		Grid.Column<TERENURI> SupTotalaColumn = terenuriGrid.addColumn(TERENURI::getSuprafataTotala);
		Grid.Column<TERENURI> CotaColumn = terenuriGrid.addColumn(TERENURI::getCota)
				.setTooltipGenerator(teren -> String.valueOf(teren.getCota()) + "%");
		Grid.Column<TERENURI> SupUtilColumn = terenuriGrid.addColumn(TERENURI::getSuprafataUtilizata);
		Grid.Column<TERENURI> CategorieColumn = terenuriGrid.addColumn(TERENURI::getCategorie);
		Grid.Column<TERENURI> BlocFizicColumn = terenuriGrid.addColumn(TERENURI::getBlocFizic);

		Grid.Column<TERENURI> ContractColumn = terenuriGrid.addColumn(TERENURI::getContract)
				.setTooltipGenerator(teren -> teren.getContract());
		Column<TERENURI> details = terenuriGrid.addColumn(createToggleDetailsRenderer(terenuriGrid)).setAutoWidth(true);

		Column<TERENURI> down = terenuriGrid.addColumn(createActionRenderer()).setFrozenToEnd(true).setAutoWidth(true)
				.setFlexGrow(0);

		GridListDataView<TERENURI> dataView = terenuriGrid.setItems(list);

		TerenuriFilter terenuriFilter = new TerenuriFilter(dataView);

		terenuriGrid.getHeaderRows().clear();
		HeaderRow headerRow = terenuriGrid.appendHeaderRow();

		headerRow.getCell(NumeColumn)
				.setComponent(createFilterHeader("Nume Proprietar Teren", terenuriFilter::setNumeProprietar));
		headerRow.getCell(TarParColumn).setComponent(createFilterHeader("Tar/Par", terenuriFilter::setTarPar));
		headerRow.getCell(CFColumn).setComponent(createFilterHeader("CF", terenuriFilter::setCF));
		headerRow.getCell(SupTotalaColumn)
				.setComponent(createFilterHeader("Sup. Totala", terenuriFilter::setSuprafataTotala));
		headerRow.getCell(CotaColumn).setComponent(createFilterHeader("Cota", terenuriFilter::setCota));
		headerRow.getCell(SupUtilColumn)
				.setComponent(createFilterHeader("Sup. Utilizata", terenuriFilter::setSuprafataUtilizata));
		headerRow.getCell(CategorieColumn).setComponent(createFilterHeader("Categorie", terenuriFilter::setCategorie));
		headerRow.getCell(BlocFizicColumn).setComponent(createFilterHeader("Bloc Fizic", terenuriFilter::setBlocFizic));
		headerRow.getCell(ContractColumn).setComponent(createFilterHeader("Tip Contract", terenuriFilter::setContract));

		headerRow.getCell(details).setComponent(new Span("Detalii"));
		// headerRow.getCell(edit).setComponent(new Span("Editor"));
		headerRow.getCell(down).setComponent(new Span("Download"));
		terenuriGrid.setItemDetailsRenderer(createTerenDetailsRenderer());
		new TerenContextMenu(terenuriGrid, terenuriRepo);

		add(terenuriGrid);

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private static String generateNumeProprietar(TERENURI teren) {
		return teren.getPoprietarID().getNumeProprietar();
	}

	private static ComponentRenderer<TerenDetailsFormLayout, TERENURI> createTerenDetailsRenderer() {
		return new ComponentRenderer<>(TerenDetailsFormLayout::new, TerenDetailsFormLayout::setTeren);
	}

	private static Renderer<TERENURI> createToggleDetailsRenderer(Grid<TERENURI> grid) {
		return LitRenderer
				.<TERENURI>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Detalii</vaadin-button>")
				.withFunction("handleClick", teren -> grid.setDetailsVisible(teren, !grid.isDetailsVisible(teren)));
	}

	private static Renderer<TERENURI> createActionRenderer() {
		return LitRenderer
				.<TERENURI>of("<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">Download</vaadin-button>")
				.withFunction("handleClick", t -> {
					if (t.getDOSAR() != null) {

						java.sql.Blob dosar = t.getDOSAR();
						try {
							InputStream is = dosar.getBinaryStream();

							File targetFile = new File(
									System.getProperty("user.home") + File.separatorChar + "dosar.pdf");
							OutputStream outStream = new FileOutputStream(targetFile);

							byte[] buffer = new byte[8 * 1024];
							int bytesRead;
							while ((bytesRead = is.read(buffer)) != -1) {
								outStream.write(buffer, 0, bytesRead);
							}
							IOUtils.closeQuietly(is);
							IOUtils.closeQuietly(outStream);

							final StreamResource resource = new StreamResource(targetFile.getName(), () -> {
								try {
									return new ByteArrayInputStream(FileUtils.readFileToByteArray(targetFile));
								} catch (IOException e) {
									System.out.println(e.getLocalizedMessage());
								}
								return is;
							});

							final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry()
									.registerResource(resource);
							UI.getCurrent().getPage().open(registration.getResourceUri().toString());

						} catch (Exception e) {
							System.out.println(e.getLocalizedMessage());
						}
					} else {
						Notification.show("Nothing to download", 2500, Position.BOTTOM_END);
					}
				});

	}

	private static Component createFilterHeader(String labelText, Consumer<String> filterChangeConsumer) {
		NativeLabel label = new NativeLabel(labelText);
		label.getStyle().set("padding-top", "var(--lumo-space-m)").set("font-size", "var(--lumo-font-size-xs)");
		TextField textField = new TextField();
		textField.setValueChangeMode(ValueChangeMode.EAGER);
		textField.setClearButtonVisible(true);
		textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
		textField.setWidthFull();
		textField.getStyle().set("max-width", "100%");
		textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
		VerticalLayout layout = new VerticalLayout(label, textField);
		layout.getThemeList().clear();
		layout.getThemeList().add("spacing-xs");

		return layout;
	}

	private static class TerenuriFilter {
		private final GridListDataView<TERENURI> dataView;

		private String NumeProprietar;
		private String TarPar;
		private String CF;
		private double SuprafataTotala;
		private double cota;
		private double SupratafataUtilizata;
		private String Categorie;
		private int BlocFizic;
		private String Contract;

		public TerenuriFilter(GridListDataView<TERENURI> dataView) {
			this.dataView = dataView;
			this.dataView.addFilter(this::test);
		}

		public void setNumeProprietar(String numeProprietar) {
			this.NumeProprietar = numeProprietar;
			this.dataView.refreshAll();
		}

		public void setTarPar(String tarpar) {
			this.TarPar = tarpar;
			this.dataView.refreshAll();
		}

		public void setCF(String cf) {
			this.CF = cf;
			this.dataView.refreshAll();
		}

		public void setSuprafataTotala(String supTotala) {
			try {
				this.SuprafataTotala = Integer.parseInt(supTotala);
			} catch (Exception e) {
				this.SuprafataTotala = 1;
			}
			this.dataView.refreshAll();
		}

		public void setCota(String cota) {
			try {
				this.cota = Integer.parseInt(cota);
			} catch (Exception e) {
				this.cota = 0;
			}
			this.dataView.refreshAll();
		}

		public void setSuprafataUtilizata(String supUtilizata) {
			try {
				this.SupratafataUtilizata = Integer.parseInt(supUtilizata);
			} catch (Exception e) {
				this.SupratafataUtilizata = 1;
			}
			this.dataView.refreshAll();
		}

		public void setCategorie(String categorie) {
			this.Categorie = categorie;
			this.dataView.refreshAll();
		}

		public void setBlocFizic(String blocFizic) {
			try {
				this.BlocFizic = Integer.parseInt(blocFizic);
			} catch (Exception e) {
				this.cota = 0;
			}
			this.dataView.refreshAll();
		}

		public void setContract(String contract) {
			this.Contract = contract;
			this.dataView.refreshAll();
		}

		public boolean test(TERENURI teren) {
			boolean matchesNumeProprietar = matches(teren.getPoprietarID().getNumeProprietar(), NumeProprietar);
			boolean matchesTarPar = matches(teren.getTarPar(), TarPar);
			boolean matchesCF = matches(teren.getCF(), CF);
			boolean matchesSupTotala = matchesDouble(teren.getSuprafataTotala(), SuprafataTotala);
			boolean matchesCota = matchesDouble(teren.getCota(), cota);
			boolean matchesSupUtilizata = matchesDouble(teren.getSuprafataUtilizata(), SupratafataUtilizata);
			boolean matchesCategorie = matches(teren.getCategorie(), Categorie);
			boolean matchesBlocFizic = matchesInt(teren.getBlocFizic(), BlocFizic);
			boolean matchesContract = matches(teren.getContract(), Contract);

			return matchesNumeProprietar && matchesTarPar && matchesCF && matchesSupTotala && matchesCota
					&& matchesSupUtilizata && matchesCategorie && matchesBlocFizic && matchesContract;
		}

		private boolean matches(String value, String searchTerm) {
			return searchTerm == null || value == null || searchTerm.isEmpty()
					|| value.toLowerCase().contains(searchTerm.toLowerCase());
		}

		private boolean matchesInt(int value, int searchTerm) {

			return searchTerm == 0 || value == 0
					|| String.valueOf(value).toLowerCase().contains(String.valueOf(searchTerm));
		}

		private boolean matchesDouble(double value, double searchTerm) {

			return searchTerm == 0 || value == 0
					|| String.valueOf(value).toLowerCase().contains(String.valueOf(searchTerm));
		}
	}

	private static class TerenDetailsFormLayout extends FormLayout {
		private static final long serialVersionUID = 6425872388237011077L;
		private final IntegerField ID = new IntegerField("ID");
		private final TextField NumeProprietar = new TextField("Nume Proprietar");
		private final TextField CNP_CUIProprietar = new TextField("CNP/CUI Proprietar");
		private final TextField LocalitateProprietar = new TextField("Localitate Proprietar");
		private final TextField NumarCasaProprietar = new TextField("Nr. Casa Proprietar");
		private final TextField TelefonProprietar = new TextField("Telefon Proprietar");
		private final TextField ReprezentantProprietar = new TextField("Reprezentant Proprietar");
		private final TextField TarPar = new TextField("TarPar");
		private final TextField CF = new TextField("Carte Funciara");
		private final TextField SuprafataTotala = new TextField("Suprafata Totala");
		private final TextField Cota = new TextField("Cota");
		private final TextField SuprafataUtilizata = new TextField("Suprafata Utilizata");
		private final TextField Categorie = new TextField("Categorie");
		private final TextField BlocFizic = new TextField("Bloc Fizic");
		private final TextField Contract = new TextField("Contract");

		/*
		 * FileBuffer FileMemoryBuffer = new FileBuffer(); Upload upload = new
		 * Upload(FileMemoryBuffer);
		 */

		public TerenDetailsFormLayout() {
			add(ID, NumeProprietar, CNP_CUIProprietar, LocalitateProprietar, NumarCasaProprietar, TelefonProprietar,
					ReprezentantProprietar, TarPar, CF, SuprafataTotala, Cota, SuprafataUtilizata, Categorie, BlocFizic,
					Contract);
			NumeProprietar.setReadOnly(true);
			CNP_CUIProprietar.setReadOnly(true);
			LocalitateProprietar.setReadOnly(true);
			NumarCasaProprietar.setReadOnly(true);
			TelefonProprietar.setReadOnly(true);
			ReprezentantProprietar.setReadOnly(true);
			TarPar.setReadOnly(true);
			CF.setReadOnly(true);
			SuprafataTotala.setReadOnly(true);
			Cota.setReadOnly(true);
			SuprafataUtilizata.setReadOnly(true);
			Categorie.setReadOnly(true);
			BlocFizic.setReadOnly(true);
			Contract.setReadOnly(true);
			setResponsiveSteps(new ResponsiveStep("0", 3));
			ID.setEnabled(false);

		}

		public void setTeren(final TERENURI teren) {
			ID.setValue(teren.getId());

			NumeProprietar.setValue(teren.getPoprietarID().getNumeProprietar());
			CNP_CUIProprietar.setValue(teren.getPoprietarID().getCNP_CUI());
			LocalitateProprietar.setValue(teren.getPoprietarID().getADRESA_LOCALITATE());
			NumarCasaProprietar.setValue(teren.getPoprietarID().getADRESA_NUMAR());
			TelefonProprietar.setValue(teren.getPoprietarID().getTELEFON());
			if (!(teren.getPoprietarID().getREPREZENTANT_PJ() == null))
				ReprezentantProprietar.setValue(teren.getPoprietarID().getREPREZENTANT_PJ());

			TarPar.setValue(teren.getTarPar());
			if (!(teren.getCF() == null))
				CF.setValue(teren.getCF());
			if (teren.getSuprafataTotala() != null)
				SuprafataTotala.setValue(String.valueOf(teren.getSuprafataTotala()));
			Cota.setValue(String.valueOf(teren.getCota()));
			if (teren.getSuprafataUtilizata() != null)
				SuprafataUtilizata.setValue(String.valueOf(teren.getSuprafataUtilizata()));

			Categorie.setValue(teren.getCategorie());
			if (teren.getBlocFizic() != null && teren.getBlocFizic() > 0)
				BlocFizic.setValue(String.valueOf(teren.getBlocFizic()));

			if (!(teren.getContract() == null))
				Contract.setValue(teren.getContract());

		}

	}

	private static class TerenContextMenu extends GridContextMenu<TERENURI> {
		private static final long serialVersionUID = 8883017315048714541L;
		private InputStream fileData;

		public TerenContextMenu(Grid<TERENURI> target, TerenuriRepository terenuriRepo) {
			super(target);

			addItem("Edit", e -> e.getItem().ifPresent(teren -> {
				Dialog editDialog = new Dialog();
				editDialog.setHeaderTitle("Update Teren");

				TextField numeProprietar = new TextField("Nume Proprietar");
				numeProprietar.setValue(teren.getPoprietarID().getNumeProprietar());
				numeProprietar.setRequiredIndicatorVisible(true);
				numeProprietar.setErrorMessage("Nume proprietar required!");
				numeProprietar.setEnabled(false);
				// TODO ALEGE PROPrietar

				TextField TarParTeren = new TextField("Tar/Par Teren");
				TarParTeren.setValue(teren.getTarPar());
				TarParTeren.setRequiredIndicatorVisible(true);
				TarParTeren.setErrorMessage("Tar/Par required!");

				TextField CFTeren = new TextField("CF Teren");
				if (teren.getCF() != null)
					CFTeren.setValue(teren.getCF());

				TextField SuprafataTotala = new TextField("Suprafata Totala Teren");
				if (teren.getSuprafataTotala() != 0)
					SuprafataTotala.setValue(String.valueOf(teren.getSuprafataTotala()));

				TextField CotaTeren = new TextField("Cota teren");
				if (teren.getCota() >= 1)
					CotaTeren.setValue(String.valueOf(teren.getCota()));

				TextField SuprafataUtilizata = new TextField("Suprafata Utilizata Teren");
				if (teren.getSuprafataUtilizata() >= 1)
					SuprafataUtilizata.setValue(String.valueOf(teren.getSuprafataUtilizata()));

				ComboBox<String> CategorieTeren = new ComboBox<String>("Categorie Teren");
				CategorieTeren.setItems("ARABIL", "PASUNE", "FANEATA", "PADURE", "VIE", "NEPRODUCTIV");
				CategorieTeren.setValue("ARABIL");
				CategorieTeren.setValue(teren.getCategorie());
				CategorieTeren.setRequiredIndicatorVisible(true);
				CategorieTeren.setErrorMessage("Categorie Teren required!");

				TextField BlocFizicTeren = new TextField("Bloc Fizic Teren");
				if (teren.getBlocFizic() > 0)
					BlocFizicTeren.setValue(String.valueOf(teren.getBlocFizic()));

				TextField ContractTeren = new TextField("Contract");
				if (teren.getContract() != null)
					ContractTeren.setValue(teren.getContract());
				// ContractTeren.setRequiredIndicatorVisible(true);
				/// ContractTeren.setErrorMessage("Contract required!");
				ContractTeren.setEnabled(false);

				FileBuffer FileMemoryBuffer = new FileBuffer();
				Upload upload = new Upload(FileMemoryBuffer);
				Button uploadButton = new Button("Upload PDF...");
				uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				upload.setUploadButton(uploadButton);
				upload.setAcceptedFileTypes("application/pdf", ".pdf");
				upload.setMaxFiles(1);
				upload.setDropAllowed(false);
				int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
				upload.setMaxFileSize(maxFileSizeInBytes);
				upload.setMinWidth(15, Unit.EM);

				upload.addSucceededListener(event -> {

					fileData = FileMemoryBuffer.getInputStream();
					String fileName = event.getFileName();

					String mimeType = event.getMIMEType();

					System.out.println(fileName + " " + mimeType);
				});
				upload.addFileRejectedListener(event -> {
					String errorMessage = event.getErrorMessage();

					Notification notification = Notification.show(errorMessage, 5000,
							Notification.Position.BOTTOM_CENTER);
					notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
				});

				upload.getElement().addEventListener("max-files-reached-changed", event -> {
					boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
					uploadButton.setEnabled(!maxFilesReached);
				}).addEventData("event.detail.value");

				FormLayout editLayout = new FormLayout();
				editLayout.setResponsiveSteps(

						new ResponsiveStep("0", 1),

						new ResponsiveStep("500px", 2));

				VerticalLayout dialogEditLayout = new VerticalLayout(editLayout);
				editLayout.add(numeProprietar, TarParTeren, CFTeren, SuprafataTotala, CotaTeren, SuprafataUtilizata,
						CategorieTeren, BlocFizicTeren, upload);
				dialogEditLayout.setAlignItems(Alignment.CENTER);
				dialogEditLayout.setJustifyContentMode(JustifyContentMode.CENTER);
				dialogEditLayout.setPadding(false);
				dialogEditLayout.setSpacing(false);

				editDialog.add(dialogEditLayout);
				Button saveButton = new Button("Update!");
				saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				saveButton.addClickListener(evt -> {
					if (!TarParTeren.getValue().isEmpty() && teren.getTarPar() != TarParTeren.getValue().trim())
						teren.setTarPar(TarParTeren.getValue().trim());
					if (!CFTeren.getValue().isEmpty() && teren.getCF() != CFTeren.getValue().trim())
						teren.setCF(CFTeren.getValue().trim());
					if (!SuprafataTotala.getValue().isEmpty()
							&& String.valueOf(teren.getSuprafataTotala()) != SuprafataTotala.getValue().trim())
						teren.setSuprafataTotala(Double.parseDouble(SuprafataTotala.getValue().trim()));
					if (!CotaTeren.getValue().isEmpty()
							&& String.valueOf(teren.getCota()) != CotaTeren.getValue().trim())
						teren.setCota(Double.parseDouble(CotaTeren.getValue().trim()));

					if (!CategorieTeren.getValue().isEmpty()
							&& teren.getCategorie() != CategorieTeren.getValue().trim())
						teren.setCategorie(CategorieTeren.getValue().trim());
					if (!BlocFizicTeren.getValue().isEmpty()
							&& String.valueOf(BlocFizicTeren.getValue().trim()) != BlocFizicTeren.getValue().trim())
						teren.setBlocFizic(Integer.parseInt(BlocFizicTeren.getValue().trim()));

					if (!ContractTeren.getValue().isEmpty() && teren.getContract() != ContractTeren.getValue().trim())
						teren.setContract(ContractTeren.getValue().trim());

					if (fileData != null) {
						Blob fd = null;
						try {
							fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

								@Override
								public Exception interceptException(Exception sqlEx) {
									System.out.println("Erroare1 la upload PJ!");
									return null;
								}

								@Override
								public ExceptionInterceptor init(Properties props, Log log) {
									System.out.println("Erroare2 la upload PJ!");
									return null;
								}

								@Override
								public void destroy() {
									System.out.println("Erroare3 la upload PJ!");

								}
							});
						} catch (IOException ex) {
							System.out.println(ex.getMessage());
						}

						teren.setDOSAR(fd);
					}
					terenuriRepo.saveAndFlush(teren);
					target.getDataProvider().refreshAll();
					editDialog.close();

				});
				Button cancelButton = new Button("Cancel", evt -> editDialog.close());
				editDialog.getFooter().add(cancelButton);
				editDialog.getFooter().add(saveButton);
				editDialog.open();

			}));
			addItem("Delete", e -> e.getItem().ifPresent(teren -> {
				ConfirmDialog deleteDialog = new ConfirmDialog();
				deleteDialog.setHeader("Stergere Teren");
				deleteDialog.setText("Sigur doriti sa stergeti " + teren.getTarPar() + " ?");
				deleteDialog.setCancelable(true);
				// deleteDialog.setRejectable(true);
				deleteDialog.setConfirmText("Delete");
				deleteDialog.setConfirmButtonTheme("error primary");
				deleteDialog.addConfirmListener(event -> {
					terenuriRepo.delete(teren);
					target.getDataProvider().refreshAll();
					this.close();
				});
				deleteDialog.open();

			}));

			add(new Hr());

			GridMenuItem<TERENURI> tarPar = addItem("Tar/Par", e -> e.getItem().ifPresent(teren -> {

				UI.getCurrent().getPage()
						.executeJs("navigator.clipboard.writeText( String.raw`" + teren.getTarPar() + "`)");
			}));
			GridMenuItem<TERENURI> suprafata = addItem("Sup. Totala", e -> e.getItem().ifPresent(teren -> {

				UI.getCurrent().getPage()
						.executeJs("navigator.clipboard.writeText(" + String.valueOf(teren.getSuprafataTotala()) + ")");
			}));

			setDynamicContentHandler(teren -> {
				// Do not show context menu when header is clicked
				if (teren == null)
					return false;
				tarPar.setText(String.format("Tar/Par: %s", teren.getTarPar()));
				suprafata.setText(String.format("Sup. Totala: %s", teren.getSuprafataTotala()));
				return true;
			});
		}

	}
}
