package com.spectral369.views;

import com.spectral369.utils.Utils;
import com.spectral369.views.about.AboutView;
import com.spectral369.views.home.HomeView;
import com.spectral369.views.procesare.ProcesareView;
import com.spectral369.views.rapoarte.RapoarteView;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.theme.lumo.LumoUtility;

/**
 * The main view is a top-level placeholder for other views.
 */
public class MainLayout extends AppLayout {

	private static final long serialVersionUID = -3507468497186466445L;
	private H2 viewTitle;
	private SideNav nav;

	public MainLayout() {

		setPrimarySection(Section.DRAWER);
		addDrawerContent();
		addHeaderContent();
		switch (Utils.CODE) {
		case 0:
			break;
		case 1:
			getStyle().set("opacity", "0.4");
			Notification.show(
					"Subscription will expire in less than 5 days. Plase check with the admin about this application.")
					.setDuration(10000);
			break;
		case 2:
			getStyle().set("opacity", "0.8");
			Notification.show(
					"Subscription will expire in less than 10 days. Plase check with the admin about this application.")
					.setDuration(10000);
			break;
		case 3:
			getStyle().set("opacity", "0.2");
			Notification.show(
					"Subscription expired or missing internet connection! Plase contact the administrator about this issue.")
					.setDuration(25000);
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + Utils.CODE);
		}

	}

	private void addHeaderContent() {
		DrawerToggle toggle = new DrawerToggle();
		toggle.getElement().setAttribute("aria-label", "Menu toggle");

		viewTitle = new H2();
		viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);

		addToNavbar(true, toggle, viewTitle);
	}

	private void addDrawerContent() {
		H1 appName = new H1("PDV APIA");

		appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
		Image log = new Image("icons/log.png", "logo");
		log.setHeight(4f, Unit.EM);
		Header header = new Header(log);
		header.add(appName);

		header.setWidthFull();
		appName.setWidthFull();
		appName.getStyle().set("text-align", "center");

		nav = createNavigation();

		appName.addClickListener(evt -> {
			this.getUI().ifPresent(ui -> {
				ui.navigate(HomeView.class);
			});
		});
		Scroller scroller = new Scroller(nav);

		addToDrawer(header, scroller, createFooter());
	}

	private SideNav createNavigation() {
		SideNav nav = new SideNav();

		nav.addItem(new SideNavItem("Home", HomeView.class, VaadinIcon.HOME_O.create()));
		nav.addItem(new SideNavItem("Procesare", ProcesareView.class, VaadinIcon.COG_O.create()));
		nav.addItem(new SideNavItem("Rapoarte", RapoarteView.class, VaadinIcon.FILE_O.create()));
		nav.addItem(new SideNavItem("About", AboutView.class, VaadinIcon.EXCLAMATION_CIRCLE_O.create()));

		return nav;
	}

	private Footer createFooter() {
		Footer layout = new Footer();

		return layout;
	}

	@Override
	protected void afterNavigation() {
		super.afterNavigation();
		viewTitle.setText(getCurrentPageTitle());

	}

	private String getCurrentPageTitle() {
		PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
		return title == null ? "" : title.value();
	}

}
