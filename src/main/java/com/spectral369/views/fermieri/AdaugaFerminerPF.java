package com.spectral369.views.fermieri;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.fermieri.FERMIERI;
import com.spectral369.db.fermieri.FermieriRepository;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.converter.StringToBigIntegerConverter;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.ValueProvider;

/**
 * 
 * AdaugaFermierPF - clasa pentru adaugarea recordului fermier in baza de date
 * 
 * @author spectral369
 *
 * 
 */
public class AdaugaFerminerPF extends VerticalLayout {

	private static final long serialVersionUID = 6510806926311568232L;
	private Binder<AdaugaFerminerPF> binder;
	private InputStream fileData = null;
	private TextField IdFermier;
	private TextField DenumireFermier;
	private TextField cnp;
	private TextField adresaLocalitate;
	private TextField adresaStrada;
	private TextField adresaNumar;
	private TextField adresaBloc;
	private TextField adresaEtaj;
	private TextField adresaApartament;
	private TextField adresaJudet;
	private TextField telefon;
	private IntegerField rol;
	private TextField IdApia;
	private TextField volum;
	private ComboBox<String> tip;
	private TextField pf_pj;
	private Upload upload;
	private IntegerField pozitie;

	public AdaugaFerminerPF(FermieriRepository fermierRepo) {
		binder = new Binder<AdaugaFerminerPF>(AdaugaFerminerPF.class);
		H2 title = new H2("Adauga Ferminer PF");
		add(title);

		HorizontalLayout content = new HorizontalLayout();
		VerticalLayout panel1 = new VerticalLayout();
		VerticalLayout panel2 = new VerticalLayout();
		content.setAlignItems(Alignment.CENTER);
		content.setJustifyContentMode(JustifyContentMode.CENTER);

		FormLayout formLayout1 = new FormLayout();
		FormLayout formLayout2 = new FormLayout();
		panel1.add(formLayout1);
		panel2.add(formLayout2);
		content.add(panel1, panel2);

		IdFermier = new TextField("ID Fermier");
		IdFermier.setRequiredIndicatorVisible(true);
		binder.forField(IdFermier).asRequired().bind(new ValueProvider<AdaugaFerminerPF, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaFerminerPF source) {
				return null;
			}
		}, new Setter<AdaugaFerminerPF, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaFerminerPF bean, String fieldvalue) {

			}
		});
		IdFermier.setErrorMessage("ID Fermier Required !");
		IdFermier.setWidth("100%");
		IdFermier.setMaxLength(12);
		IdFermier.setPlaceholder("ID Fermier");
		formLayout1.add(IdFermier);

		DenumireFermier = new TextField("Nume Fermier");
		DenumireFermier.setRequiredIndicatorVisible(true);
		DenumireFermier.setAllowedCharPattern("[A-Za-z-\s]");
		binder.forField(DenumireFermier).asRequired()
				.withValidator(str -> str.toString().length() > 4,
						"Nume fermierului trebuie sa aibe mai mult de 4 caractere")
				.bind(new ValueProvider<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaFerminerPF source) {
						return null;
					}
				}, new Setter<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaFerminerPF bean, String fieldvalue) {

					}
				});
		DenumireFermier.setErrorMessage("Nume Required !");
		DenumireFermier.setWidth("100%");
		DenumireFermier.setMaxLength(30);
		DenumireFermier.setPlaceholder("Nume Fermier");
		formLayout1.add(DenumireFermier);

		cnp = new TextField("CNP Ferminer");
		cnp.setRequiredIndicatorVisible(true);
		binder.forField(cnp).asRequired().withValidator(str -> str.toString().length() == 13, "CNP-ul are 13 cifre")
				.withValidator(str -> fermierRepo.isCNPPresent(str).get() == false,
						"CNP-ul este deja exista in baza de date!")
				.withConverter(new StringToBigIntegerConverter("CNP-ul poate contine doar cifre"))

				.bind(new ValueProvider<AdaugaFerminerPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(AdaugaFerminerPF source) {
						return null;
					}
				}, new Setter<AdaugaFerminerPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaFerminerPF bean, BigInteger fieldvalue) {

					}
				});
		cnp.setErrorMessage("CNP Required !");
		cnp.setWidth("100%");
		cnp.setMaxLength(13);
		cnp.setPlaceholder("CNP Ferminer");
		cnp.setMinWidth(25f, Unit.EM);
		formLayout1.add(cnp);

		adresaLocalitate = new TextField("Adresa Localiatate");
		adresaLocalitate.setRequiredIndicatorVisible(true);
		binder.forField(adresaLocalitate).asRequired()
				.withValidator(str -> str.toString().length() > 4,
						"Localitatea trebuie sa aibe mai mult de 4 caractere")
				.bind(new ValueProvider<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaFerminerPF source) {
						return null;
					}
				}, new Setter<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaFerminerPF bean, String fieldvalue) {

					}
				});
		adresaLocalitate.setErrorMessage("Localitate Required !");
		adresaLocalitate.setWidth("100%");
		adresaLocalitate.setMaxLength(25);
		adresaLocalitate.setPlaceholder("Localitate");
		adresaLocalitate.setMinWidth(15f, Unit.EM);
		formLayout1.add(adresaLocalitate);

		adresaStrada = new TextField("Adresa Strada");
		adresaStrada.setWidth("100%");
		adresaStrada.setMaxLength(35);
		adresaStrada.setPlaceholder("Strada");
		adresaStrada.setMinWidth(12f, Unit.EM);
		formLayout1.add(adresaStrada);

		adresaNumar = new TextField("Adresa Numar");
		adresaNumar.setRequiredIndicatorVisible(true);
		binder.forField(adresaNumar).asRequired()
				.withValidator(str -> str.matches("^(\\d){1,4}[/]{0,1}[a-zA-Z]{0,1}$"), "Numar de casa invalid !")
				.bind(new ValueProvider<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaFerminerPF source) {
						return null;
					}
				}, new Setter<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaFerminerPF bean, String fieldvalue) {

					}
				});
		adresaNumar.setErrorMessage("Numar Required !");
		adresaNumar.setWidth("100%");
		adresaNumar.setMaxLength(8);
		adresaNumar.setPlaceholder("Numar");
		formLayout1.add(adresaNumar);

		adresaBloc = new TextField("Adresa Bloc");
		adresaBloc.setWidth("100%");
		adresaBloc.setMaxLength(4);
		adresaBloc.setPlaceholder("Bloc");
		formLayout1.add(adresaBloc);

		adresaEtaj = new TextField("Adresa Etaj");
		adresaEtaj.setWidth("100%");
		adresaEtaj.setMaxLength(4);
		adresaEtaj.setPlaceholder("Etaj");
		formLayout1.add(adresaEtaj);

		adresaApartament = new TextField("Adresa Apartament");
		adresaApartament.setWidth("100%");
		adresaApartament.setMaxLength(4);
		adresaApartament.setPlaceholder("Apartament");
		formLayout1.add(adresaApartament);

		adresaJudet = new TextField("Judet");
		adresaJudet.setRequiredIndicatorVisible(true);
		binder.forField(adresaJudet).asRequired()
				.withValidator(str -> str.toString().length() > 3,
						"Localitatea trebuie sa aibe mai mult de 3 caractere")
				.bind(new ValueProvider<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(AdaugaFerminerPF source) {
						return null;
					}
				}, new Setter<AdaugaFerminerPF, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaFerminerPF bean, String fieldvalue) {

					}
				});
		adresaJudet.setErrorMessage("Judet PF Required!");
		adresaJudet.setWidth("100%");
		adresaJudet.setMaxLength(15);
		adresaJudet.setPlaceholder("Judet");
		formLayout1.add(adresaJudet);

		telefon = new TextField("Telefon");
		telefon.setRequiredIndicatorVisible(true);
		binder.forField(telefon).asRequired()
				.withValidator(str -> str.matches("^([00]{0,1}[\\d]{1,3}[0-9]{9,12})$"), "Numar de telefon invalid !")
				.withConverter(new StringToBigIntegerConverter("Nr. telefon poate contine doar cifre"))
				.bind(new ValueProvider<AdaugaFerminerPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(AdaugaFerminerPF source) {
						return null;
					}
				}, new Setter<AdaugaFerminerPF, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(AdaugaFerminerPF bean, BigInteger fieldvalue) {

					}
				});
		telefon.setErrorMessage("Telefon Required !");
		telefon.setWidth("100%");
		telefon.setMaxLength(14);
		telefon.setPlaceholder("07xxxxxxxx");
		telefon.setMinWidth(15f, Unit.EM);
		formLayout1.add(telefon);

		rol = new IntegerField("Rol");
		/*
		 * rol.setRequiredIndicatorVisible(true);
		 * binder.forField(rol).asRequired().bind(new ValueProvider<AdaugaFerminerPF,
		 * Integer>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public Integer apply(AdaugaFerminerPF source) { return null; } },
		 * new Setter<AdaugaFerminerPF, Integer>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public void accept(AdaugaFerminerPF bean, Integer fieldvalue) {
		 * 
		 * } }); rol.setErrorMessage("Rol Required !");
		 */
		rol.setWidth("100%");
		rol.setPlaceholder("Rol");
		rol.setValueChangeMode(ValueChangeMode.TIMEOUT);
		rol.setValueChangeTimeout(1000);
		formLayout2.add(rol);

		IdApia = new TextField("ID APIA");
		/*
		 * IdApia.setRequiredIndicatorVisible(true);
		 * binder.forField(IdApia).asRequired().bind(new ValueProvider<AdaugaFerminerPF,
		 * String>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public String apply(AdaugaFerminerPF source) { return null; } },
		 * new Setter<AdaugaFerminerPF, String>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public void accept(AdaugaFerminerPF bean, String fieldvalue) {
		 * 
		 * } }); IdApia.setErrorMessage("ID APIA Required !");
		 */
		IdApia.setWidth("100%");
		IdApia.setMaxLength(12);
		IdApia.setPlaceholder("ID APIA");
		formLayout2.add(IdApia);

		volum = new TextField("Volum");
		/*
		 * volum.setRequiredIndicatorVisible(true);
		 * binder.forField(volum).asRequired().bind(new ValueProvider<AdaugaFerminerPF,
		 * String>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public String apply(AdaugaFerminerPF source) { return null; } },
		 * new Setter<AdaugaFerminerPF, String>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public void accept(AdaugaFerminerPF bean, String fieldvalue) {
		 * 
		 * } }); volum.setErrorMessage("Volum Required !");
		 */
		volum.setWidth("100%");
		volum.setMaxLength(8);
		volum.setPlaceholder("Volum");
		formLayout2.add(volum);

		pozitie = new IntegerField("Pozitie");
		/*
		 * pozitie.setRequiredIndicatorVisible(true);
		 * binder.forField(pozitie).asRequired().bind(new
		 * ValueProvider<AdaugaFerminerPF, Integer>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public Integer apply(AdaugaFerminerPF source) { return null; } },
		 * new Setter<AdaugaFerminerPF, Integer>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public void accept(AdaugaFerminerPF bean, Integer fieldvalue) {
		 * 
		 * } }); pozitie.setErrorMessage("Pozitie Required !");
		 */
		pozitie.setWidth("100%");
		pozitie.setPlaceholder("Pozitie");
		formLayout2.add(pozitie);

		tip = new ComboBox<String>("Tip PF");
		tip.setItems("PF Locala", "PF Externa");
		tip.setPlaceholder("TIP PF ?");
		tip.setRequiredIndicatorVisible(true);
		binder.forField(tip).asRequired().bind(new ValueProvider<AdaugaFerminerPF, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaFerminerPF source) {
				return null;
			}
		}, new Setter<AdaugaFerminerPF, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaFerminerPF bean, String fieldvalue) {

			}
		});
		tip.setErrorMessage("Tip Required !");
		tip.setWidth("100%");
		formLayout2.add(tip);

		pf_pj = new TextField("PF/PJ");
		pf_pj.setRequiredIndicatorVisible(true);
		pf_pj.setValue("Persoana Fizica");
		pf_pj.setEnabled(false);
		pf_pj.setErrorMessage("PF/PJ Required !");
		pf_pj.setWidth("100%");
		pf_pj.setMaxLength(18);
		pf_pj.setPlaceholder("PF/PJ");
		formLayout2.add(pf_pj);

		FileBuffer FileMemoryBuffer = new FileBuffer();
		upload = new Upload(FileMemoryBuffer);
		Button uploadButton = new Button("Upload PDF...");
		uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		upload.setUploadButton(uploadButton);
		upload.setAcceptedFileTypes("application/pdf", ".pdf");
		upload.setMaxFiles(1);
		upload.setDropAllowed(false);
		int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
		upload.setMaxFileSize(maxFileSizeInBytes);
		upload.setMinWidth(15, Unit.EM);

		upload.addSucceededListener(event -> {

			fileData = FileMemoryBuffer.getInputStream();
			String fileName = event.getFileName();
			String mimeType = event.getMIMEType();
			System.out.println(fileName + " " + mimeType);
		});
		upload.addFileRejectedListener(event -> {
			String errorMessage = event.getErrorMessage();

			Notification notification = Notification.show(errorMessage, 5000, Notification.Position.BOTTOM_CENTER);
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
		});

		upload.getElement().addEventListener("max-files-reached-changed", event -> {
			boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
			uploadButton.setEnabled(!maxFilesReached);
		}).addEventData("event.detail.value");

		formLayout2.add(upload);

		formLayout1.setResponsiveSteps(
				// Use one column by default
				new ResponsiveStep("0", 1),
				// Use two columns, if layout's width exceeds 500px
				new ResponsiveStep("500px", 2));
		formLayout1.setColspan(cnp, 2);
		formLayout1.setColspan(DenumireFermier, 2);
		formLayout1.setColspan(adresaLocalitate, 2);
		formLayout1.setColspan(adresaStrada, 2);

		formLayout2.setResponsiveSteps(new ResponsiveStep("0", 1), new ResponsiveStep("500px", 2));

		HorizontalLayout fields4 = new HorizontalLayout();
		fields4.setAlignItems(Alignment.CENTER);

		Button submit = new Button("Insert");
		submit.setEnabled(false);
		submit.addClickListener(evt -> {

			FERMIERI fermier = new FERMIERI();
			fermier.setID_FERMIER(Integer.parseInt(IdFermier.getValue().trim()));
			fermier.setDENUMIRE_FERMIER(DenumireFermier.getValue().trim());
			fermier.setCNP_CUI(cnp.getValue().trim());
			fermier.setADRESA_LOCALITATE(adresaLocalitate.getValue().trim());
			fermier.setADRESA_STRADA(adresaStrada.getValue().trim());
			fermier.setADRESA_NUMAR(adresaNumar.getValue());
			if (!adresaBloc.isEmpty())
				fermier.setADRESA_BLOC(adresaBloc.getValue().trim());

			if (!adresaEtaj.isEmpty())
				fermier.setADRESA_ETAJ(adresaEtaj.getValue().trim());

			if (!adresaApartament.isEmpty())
				fermier.setADRESA_APARTAMENT(adresaApartament.getValue().trim());
			fermier.setADRESA_JUDET(adresaJudet.getValue().trim());
			fermier.setTELEFON(telefon.getValue().trim());
			if (!IdApia.isEmpty())
				fermier.setID_APIA(IdApia.getValue().trim());
			fermier.setVOLUM(volum.getValue().trim());
			if (!pozitie.isEmpty())
				fermier.setPOZITIE(pozitie.getValue());
			fermier.setTIP(tip.getValue().trim());
			if (!rol.isEmpty())
				fermier.setROL(rol.getValue());
			fermier.setPF_PJ(pf_pj.getValue().trim());
			if (fileData != null) {
				Blob fd = null;
				try {
					fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

						@Override
						public Exception interceptException(Exception sqlEx) {
							System.out.println("Erroare1 la upload PF!");
							return null;
						}

						@Override
						public ExceptionInterceptor init(Properties props, Log log) {
							System.out.println("Erroare2 la upload PF!");
							return null;
						}

						@Override
						public void destroy() {
							System.out.println("Erroare3 la upload PF!");

						}
					});
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}

				fermier.setDOSAR(fd);
			}
			fermierRepo.save(fermier);
			clear();

			Notification notification = Notification.show("Insertion(PF)...Done !", 5000,
					Notification.Position.BOTTOM_END);
			notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
			this.removeFromParent();// test
		});
		Button clear = new Button("Clear");
		clear.addClickListener(event -> {

			clear();
			Notification.show("Field Reset...Done!", 5000, Notification.Position.BOTTOM_END);

		});
		binder.addStatusChangeListener(event -> {

			if (binder.isValid()) {
				submit.setEnabled(true);
			} else {
				submit.setEnabled(false);
			}
		});
		fields4.add(submit);
		fields4.add(clear);

		add(content);
		add(fields4);
		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	public void clear() {
		IdFermier.clear();
		cnp.clear();
		DenumireFermier.clear();
		adresaLocalitate.clear();
		adresaStrada.clear();
		adresaNumar.clear();
		adresaBloc.clear();
		adresaEtaj.clear();
		adresaApartament.clear();
		adresaJudet.clear();
		telefon.clear();
		IdApia.clear();
		volum.clear();
		pozitie.clear();
		tip.clear();
		rol.clear();
	}

}
