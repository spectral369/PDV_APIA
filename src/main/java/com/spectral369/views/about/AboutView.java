package com.spectral369.views.about;

import com.spectral369.views.MainLayout;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("About")
@Route(value = "about", layout = MainLayout.class)
public class AboutView extends VerticalLayout {

	private static final long serialVersionUID = -5102894712067106568L;

	public AboutView() {
		setSpacing(false);

		add(new H2("PDV APIA - pre alfa 0.1"));
		Paragraph p1 = new Paragraph();
		Text t1 = new Text("Author: ");
		Anchor link1 = new Anchor("https://freelancingpeter.eu", "spectral369");
		link1.setTarget(AnchorTarget.BLANK);
		p1.add(t1, link1);

		Paragraph p2 = new Paragraph();
		Text t2 = new Text("Repository: ");
		Anchor link2 = new Anchor("https://codeberg.org/spectral369", "PDV_APIA");
		link2.setTarget(AnchorTarget.BLANK);
		p2.add(t2, link2);
		add(p1, p2);
		Paragraph p3 = new Paragraph();
		Text t3 = new Text("Changelog: ");
		Anchor link3 = new Anchor("https://codeberg.org/spectral369/PDV_APIA/commits/branch/main", "Changelog");
		link3.setTarget(AnchorTarget.BLANK);
		p3.add(t3, link3);
		add(p3);

		setSizeFull();
		setJustifyContentMode(JustifyContentMode.CENTER);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

}
