package com.spectral369.views.procesare;

import com.spectral369.db.contracte.ContracteRepository;
import com.spectral369.db.fermieri.FermieriRepository;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.spectral369.utils.Schdules;
import com.spectral369.utils.Utils;
import com.spectral369.views.MainLayout;
import com.spectral369.views.contracte.AdaugaContract;
import com.spectral369.views.fermieri.AdaugaFerminerPF;
import com.spectral369.views.fermieri.AdaugaFerminerPJ;
import com.spectral369.views.home.HomeView;
import com.spectral369.views.proprietari.AdaugaProprietarPF;
import com.spectral369.views.proprietari.AdaugaProprietarPJ;
import com.spectral369.views.terenuri.AdaugaTeren;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Procesare")
@Route(value = "procesare", layout = MainLayout.class)
public class ProcesareView extends VerticalLayout {

	private static final long serialVersionUID = -9091673312799392013L;
	HorizontalLayout content = null;

	public ProcesareView(FermieriRepository fermierRepo, ProprietariRepository proprietariRepo,
			TerenuriRepository terenRepo, ContracteRepository contracteRepo) {
		setSpacing(false);

		HorizontalLayout topButtons = new HorizontalLayout();
		Button adaugaFerminer = new Button("Adauga Ferminer");
		adaugaFerminer.addClickListener(evt -> {
			adaugaFermierImpl(evt, fermierRepo);

		});
		adaugaFerminer.setId("adaugaFerminer");
		adaugaFerminer.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		Button adaugaProprietar = new Button("Adauga Proprietari");
		adaugaProprietar.addClickListener(evt -> {
			adaugaPropImpl(evt, proprietariRepo);
		});
		adaugaProprietar.setId("adaugaProprietar");
		adaugaProprietar.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		Button adaugaTeren = new Button("Adauga Teren");
		adaugaTeren.addClickListener(evt -> {
			adaugaTerenImpl(evt, terenRepo, proprietariRepo);
		});
		adaugaTeren.setId("adaugaTeren");
		adaugaTeren.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		Button adaugaContract = new Button("Adauga Contract");
		adaugaContract.addClickListener(evt -> {
			adaugaContractImpl(evt, terenRepo, proprietariRepo, contracteRepo, fermierRepo);
		});
		adaugaContract.setId("adaugaContract");
		adaugaContract.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		topButtons.add(adaugaFerminer, adaugaProprietar, adaugaTeren, adaugaContract);
		topButtons.setAlignItems(Alignment.CENTER);
		add(topButtons);

		content = new HorizontalLayout();

		Schdules sc = new Schdules();
		sc.setContracteRepo(contracteRepo);
		add(content);
		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	private void adaugaContractImpl(ClickEvent<Button> evt, TerenuriRepository terenRepo,
			ProprietariRepository proprietariRepo, ContracteRepository contracteRepo, FermieriRepository fermierRepo) {
		if (Utils.CODE < 2) {
			if (content.getComponentCount() > 0) {
				content.removeAll();
				content.add(new AdaugaContract(proprietariRepo, terenRepo, contracteRepo, fermierRepo));
			} else {

				content.add(new AdaugaContract(proprietariRepo, terenRepo, contracteRepo, fermierRepo));
			}
		} else {
			UI.getCurrent().navigate(HomeView.class);
		}
	}

	private void adaugaTerenImpl(ClickEvent<Button> evt, TerenuriRepository terenRepo,
			ProprietariRepository proprietariRepo) {
		if (content.getComponentCount() > 0) {
			content.removeAll();
			content.add(new AdaugaTeren(terenRepo, proprietariRepo));
		} else {

			content.add(new AdaugaTeren(terenRepo, proprietariRepo));
		}
	}

	private void adaugaFermierImpl(ClickEvent<Button> evt, FermieriRepository fermierRepo) {

		if (content.getComponentCount() > 0) {
			content.removeAll();
			fermierDialog(fermierRepo);
		} else {
			/*
			 * Dialog dialog = new Dialog();
			 * 
			 * dialog.setHeaderTitle("Alege tip fermier");
			 * 
			 * Button PFButton = new Button("Persoana Fizica", (e) -> { dialog.close();
			 * 
			 * content.add(new AdaugaFerminerPF(fermierRepo)); });
			 * PFButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			 * PFButton.getStyle().set("margin-right", "auto");
			 * dialog.getFooter().add(PFButton);
			 * 
			 * Button PJButton = new Button("Persoana Juridica", (e) -> { dialog.close();
			 * 
			 * content.add(new AdaugaFerminerPJ(fermierRepo));
			 * 
			 * }); PJButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			 * dialog.getFooter().add(PJButton); dialog.open();
			 */
			fermierDialog(fermierRepo);

		}
	}

	private void adaugaPropImpl(ClickEvent<Button> evt, ProprietariRepository proprietarRepo) {
		if (content.getComponentCount() > 0) {
			content.removeAll();
			proprietarDialog(proprietarRepo);
		} else {
			/*
			 * Dialog dialog = new Dialog();
			 * 
			 * dialog.setHeaderTitle("Alege tip Proprietar");
			 * 
			 * Button PFButton = new Button("Persoana Fizica", (e) -> { dialog.close();
			 * 
			 * content.add(new AdaugaProprietarPF(proprietarRepo)); });
			 * PFButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			 * PFButton.getStyle().set("margin-right", "auto");
			 * dialog.getFooter().add(PFButton);
			 * 
			 * Button PJButton = new Button("Persoana Juridica", (e) -> { dialog.close();
			 * 
			 * content.add(new AdaugaProprietarPJ(proprietarRepo));
			 * 
			 * }); PJButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			 * dialog.getFooter().add(PJButton); dialog.open();
			 */

			proprietarDialog(proprietarRepo);

		}
	}

	private void fermierDialog(FermieriRepository fermierRepo) {
		Dialog dialog = new Dialog();

		dialog.setHeaderTitle("Alege tip fermier");

		Button PFButton = new Button("Persoana Fizica", (e) -> {
			dialog.close();

			content.add(new AdaugaFerminerPF(fermierRepo));
		});
		PFButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		PFButton.getStyle().set("margin-right", "auto");
		dialog.getFooter().add(PFButton);

		Button PJButton = new Button("Persoana Juridica", (e) -> {
			dialog.close();

			content.add(new AdaugaFerminerPJ(fermierRepo));

		});
		PJButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		dialog.getFooter().add(PJButton);
		dialog.open();
	}

	private void proprietarDialog(ProprietariRepository proprietarRepo) {
		Dialog dialog = new Dialog();

		dialog.setHeaderTitle("Alege tip Proprietar");

		Button PFButton = new Button("Persoana Fizica", (e) -> {
			dialog.close();

			content.add(new AdaugaProprietarPF(proprietarRepo));
		});
		PFButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		PFButton.getStyle().set("margin-right", "auto");
		dialog.getFooter().add(PFButton);

		Button PJButton = new Button("Persoana Juridica", (e) -> {
			dialog.close();

			content.add(new AdaugaProprietarPJ(proprietarRepo));

		});
		PJButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		dialog.getFooter().add(PJButton);
		dialog.open();
	}

}
