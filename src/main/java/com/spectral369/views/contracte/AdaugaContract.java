package com.spectral369.views.contracte;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;

import com.mysql.cj.exceptions.ExceptionInterceptor;
import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.log.Log;
import com.spectral369.db.contracte.CONTRACTE;
import com.spectral369.db.contracte.ContracteRepository;
import com.spectral369.db.fermieri.FERMIERI;
import com.spectral369.db.fermieri.FermieriRepository;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.proprietari.ProprietariRepository;
import com.spectral369.db.terenuri.TERENURI;
import com.spectral369.db.terenuri.TerenuriRepository;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.ValueProvider;

public class AdaugaContract extends VerticalLayout {

	private static final long serialVersionUID = -7021932422729576905L;
	private Binder<AdaugaContract> binder;
	private List<PROPRIETARI> proprietar;
	private FERMIERI fermier;
	private List<TERENURI> teren;
	// private TextField proprietarField;
	private Upload upload;
	private InputStream fileData;
	// private TextField tarparField;
	// private TextField cfField;
	// private TextField suprafataField;
	private IntegerField numarContract;
	private DatePicker dataContract;
	private TextField valabilitateContract;
	private IntegerField numarContractFermier;
	private DatePicker dataContractFermier;
	private DatePicker dataInceputContract;
	private TextField perioada;
	private DatePicker dataSfarsitContract;
	private Grid<TERENURI> gridtpcfsup;
	private ComboBox<String> tipContract;
	private TextField fermierField;
	private Grid<PROPRIETARI> gridprops;

	public AdaugaContract(ProprietariRepository proprietarRepo, TerenuriRepository terenRepo,
			ContracteRepository contractRepo, FermieriRepository fermierRepo) {
		binder = new Binder<AdaugaContract>(AdaugaContract.class);
		proprietar = new LinkedList<>();
		teren = new LinkedList<>();
		H2 title = new H2("Adauga Contract");
		add(title);

		HorizontalLayout content = new HorizontalLayout();
		VerticalLayout panel1 = new VerticalLayout();
		FormLayout formLayout1 = new FormLayout();
		panel1.add(formLayout1);
		content.add(panel1);

		numarContract = new IntegerField("Numar Contract");
		numarContract.setRequiredIndicatorVisible(true);
		numarContract.setEnabled(false);
		numarContract.setErrorMessage("Numar Contract Required!");
		numarContract.setWidth("100%");
		numarContract.setPlaceholder("Numar Contract");
		formLayout1.add(numarContract);

		dataContract = new DatePicker("Data Contract");

		dataContract.setLocale(new Locale("ro", "RO"));
		dataContract.setRequiredIndicatorVisible(true);
		binder.forField(dataContract).asRequired().bind(new ValueProvider<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public LocalDate apply(AdaugaContract source) {
				return null;
			}
		}, new Setter<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaContract bean, LocalDate fieldvalue) {

			}
		});
		dataContract.setErrorMessage("Data Contract required!");
		dataContract.setWidth("100%");
		formLayout1.add(dataContract);

		valabilitateContract = new TextField("valabilitate Contract");
		valabilitateContract.setRequiredIndicatorVisible(true);
		valabilitateContract.setEnabled(false);
		valabilitateContract.setErrorMessage("Valabilitate Contract Required!");
		valabilitateContract.setWidth("100%");
		valabilitateContract.setPlaceholder("Valabilitate Contract");
		formLayout1.add(valabilitateContract);

		Button searchProprietar = new Button("Cauta Proprietar", VaadinIcon.SEARCH.create());
		searchProprietar.addClickListener(evt -> {
			Dialog dialog = new Dialog();

			dialog.setHeaderTitle("Alege proprietar");

			VerticalLayout contentLayout = new VerticalLayout();
			contentLayout.setAlignItems(Alignment.CENTER);
			contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

			HorizontalLayout searchLayout = new HorizontalLayout();
			searchLayout.setAlignItems(Alignment.CENTER);
			searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
			TextField searchField = new TextField("Cauta CNP/CUI");
			searchField.setPlaceholder("CNP sau CUI");
			searchField.setRequiredIndicatorVisible(true);
			searchField.setErrorMessage("CNP/CUI Required !");
			searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
			searchField.setValueChangeTimeout(1000);
			searchField.setWidth("100%");
			searchField.setMaxLength(14);

			MultiSelectListBox<String> listbox = new MultiSelectListBox<String>();

			List<PROPRIETARI> props = proprietarRepo.findAll();
			List<String> numep = new ArrayList<String>();
			for (PROPRIETARI p : props) {
				numep.add(p.getID() + ":" + p.getNumeProprietar());

			}
			listbox.setItems(numep);

			Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
			searchbtn.setEnabled(false);
			searchbtn.addClickListener(event -> {
				listbox.clear();
				numep.clear();
				List<PROPRIETARI> propByCnpCui = proprietarRepo
						.findByCNPCUIContainingIgnoreCase(searchField.getValue().trim());
				for (PROPRIETARI p : propByCnpCui) {
					numep.add(p.getID() + ":" + p.getNumeProprietar());
				}
				listbox.setItems(numep);

			});

			searchLayout.add(searchField, searchbtn);
			contentLayout.add(searchLayout, listbox);
			dialog.add(contentLayout);
			Button ok = new Button("OK", (e) -> {
				// int pid = Integer.parseInt(listbox.getValue().substring(0,
				// listbox.getValue().indexOf(":")));
				// proprietar = proprietarRepo.findProprietarByID(pid);
				// dialog.close();
				// proprietarField.setValue(proprietar.getNumeProprietar());
				Set<String> lpids = listbox.getValue();
				Set<Integer> pids = new HashSet<Integer>();
				proprietar.clear();
				for (String lp : lpids) {

					int idss = Integer.parseInt(lp.substring(0, lp.indexOf(":")));
					pids.add(idss);
					proprietar.add(proprietarRepo.findProprietarByID(idss));
				}
				// int pid = Integer.parseInt(listbox.getValue().substring(0,
				// listbox.getValue().indexOf(":")));
				// teren = terenRepo.findByIDIgnoreCase(pid);

				gridprops.setItems(proprietar);
				gridprops.select(proprietar.get(0));
				dialog.close();
				// for(TERENURI t : teren) {

				// }

			});
			ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			ok.getStyle().set("margin-right", "auto");
			dialog.getFooter().add(ok);
			Button cancel = new Button("Cancel", (e) -> {
				dialog.close();

			});
			cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

			dialog.getFooter().add(cancel);
			dialog.setModal(true);
			dialog.setCloseOnEsc(true);
			dialog.setCloseOnOutsideClick(false);
			UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
				dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
				dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

			});
			searchField.addValueChangeListener(vcl -> {
				if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
					searchbtn.setEnabled(true);
				} else {
					searchbtn.setEnabled(false);
				}
			});

			dialog.open();

		});

		/*
		 * proprietarField = new TextField("Proprietar");
		 * proprietarField.setEnabled(false);
		 * proprietarField.setRequiredIndicatorVisible(true);
		 * proprietarField.setErrorMessage("Alegerea Proprietarului Required !");
		 * proprietarField.setPlaceholder("Nume Proprietar");
		 * formLayout1.add(searchProprietar, proprietarField);
		 */

		gridprops = new Grid<PROPRIETARI>(PROPRIETARI.class);
		// gridprops.setSelectionMode(SelectionMode.MULTI);
		gridprops.removeAllColumns();

		gridprops.addColumn(PROPRIETARI::getNumeProprietar).setHeader("Proprietari");
		formLayout1.add(searchProprietar, gridprops);

		numarContractFermier = new IntegerField("Numar Contract Fermier");
		numarContractFermier.setRequiredIndicatorVisible(true);
		binder.forField(numarContractFermier).asRequired().bind(new ValueProvider<AdaugaContract, Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Integer apply(AdaugaContract source) {
				return null;
			}
		}, new Setter<AdaugaContract, Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaContract bean, Integer fieldvalue) {

			}
		});
		numarContractFermier.setErrorMessage("Numar Contract Fermier required !");
		numarContractFermier.setWidth("100%");
		numarContractFermier.setPlaceholder("Numar Contract Fermier");
		formLayout1.add(numarContractFermier);

		dataContractFermier = new DatePicker("Data Contract Fermier");
		dataContractFermier.setLocale(new Locale("ro", "RO"));
		dataContractFermier.setRequiredIndicatorVisible(true);
		binder.forField(dataContractFermier).asRequired().bind(new ValueProvider<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public LocalDate apply(AdaugaContract source) {
				return null;
			}
		}, new Setter<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaContract bean, LocalDate fieldvalue) {

			}
		});
		dataContractFermier.setErrorMessage("Data Contract Fermier required!");
		dataContractFermier.setWidth("100%");
		formLayout1.add(dataContractFermier);
		Button searchTeren = new Button("Cauta Teren", VaadinIcon.SEARCH.create());
		searchTeren.addClickListener(evt -> {
			Dialog dialog = new Dialog();

			dialog.setHeaderTitle("Alege Teren");

			VerticalLayout contentLayout = new VerticalLayout();
			contentLayout.setAlignItems(Alignment.CENTER);
			contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

			HorizontalLayout searchLayout = new HorizontalLayout();
			searchLayout.setAlignItems(Alignment.CENTER);
			searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
			TextField searchField = new TextField("Cauta Tarla/parcela");
			searchField.setPlaceholder("Tarla/Parcela");
			searchField.setRequiredIndicatorVisible(true);
			searchField.setErrorMessage("Tar/Par Required !");
			searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
			searchField.setValueChangeTimeout(1000);
			searchField.setWidth("100%");
			searchField.setMaxLength(14);

			// ListBox<String> listbox = new ListBox<String>();
			MultiSelectListBox<String> listbox = new MultiSelectListBox<String>();
			if (proprietar == null)
				return;
			// List<TERENURI> terenuriAllByID;
			List<String> terens = new ArrayList<String>();

			for (PROPRIETARI idprop : proprietar) {
				List<TERENURI> tere = terenRepo.findTerenFromProp2(idprop);
				for (TERENURI tr : tere) {
					terens.add(tr.getId() + ":" + tr.getTarPar() + " " + tr.getCF() + " " + tr.getSuprafataUtilizata());
				}

			}

			listbox.setItems(terens);

			Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
			searchbtn.setEnabled(false);
			searchbtn.addClickListener(event -> {
				listbox.clear();
				terens.clear();

				for (PROPRIETARI pr : proprietar) {
					List<TERENURI> terenuriByID = terenRepo.findTerenFromPropAndTarPar(pr,
							searchField.getValue().trim());
					for (TERENURI tr : terenuriByID) {
						terens.add(tr.getId() + ":" + tr.getTarPar() + " " + tr.getCF() + " "
								+ tr.getSuprafataUtilizata());
					}
				}

				listbox.setItems(terens);

			});

			searchLayout.add(searchField, searchbtn);
			contentLayout.add(searchLayout, listbox);
			dialog.add(contentLayout);
			Button ok = new Button("OK", (e) -> {
				Set<String> lpids = listbox.getValue();
				Set<Integer> pids = new HashSet<Integer>();
				teren.clear();
				for (String lp : lpids) {
					int idss = Integer.parseInt(lp.substring(0, lp.indexOf(":")));
					pids.add(idss);

					teren.add(terenRepo.findByIDIgnoreCase(idss));
				}
				// int pid = Integer.parseInt(listbox.getValue().substring(0,
				// listbox.getValue().indexOf(":")));
				// teren = terenRepo.findByIDIgnoreCase(pid);
				gridtpcfsup.setItems(teren);

				dialog.close();

				// tarparField.setValue(teren.getTarPar());
				// cfField.setValue(teren.getCF());
				// suprafataField.setValue(String.valueOf(teren.getSuprafataUtilizata()));

			});
			ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			ok.getStyle().set("margin-right", "auto");
			dialog.getFooter().add(ok);
			Button cancel = new Button("Cancel", (e) -> {
				dialog.close();

			});
			cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

			dialog.getFooter().add(cancel);
			dialog.setModal(true);
			dialog.setCloseOnEsc(true);
			dialog.setCloseOnOutsideClick(false);
			UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
				dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
				dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

			});
			searchField.addValueChangeListener(vcl -> {
				if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
					searchbtn.setEnabled(true);
				} else {
					searchbtn.setEnabled(false);
				}
			});

			dialog.open();

		});
		formLayout1.add(searchTeren);

		gridtpcfsup = new Grid<TERENURI>(TERENURI.class);
		gridtpcfsup.removeAllColumns();
		gridtpcfsup.addColumn(TERENURI::getTarPar).setHeader("Tar/Par");

		gridtpcfsup.addColumn(TERENURI::getCF).setHeader("C.F");
		gridtpcfsup.addColumn(TERENURI::getSuprafataUtilizata).setHeader("Suprafata");
		formLayout1.add(gridtpcfsup);

		/*
		 * tarparField = new TextField("Tar/Par");
		 * tarparField.setRequiredIndicatorVisible(true); tarparField.setEnabled(false);
		 * tarparField.setPlaceholder("Tarla/Parcela"); formLayout1.add(tarparField);
		 * 
		 * cfField = new TextField("C.F"); cfField.setRequiredIndicatorVisible(true);
		 * cfField.setEnabled(false); cfField.setPlaceholder("Numar Carte Funciara");
		 * formLayout1.add(cfField);
		 * 
		 * suprafataField = new TextField("Suprafata Teren");
		 * suprafataField.setRequiredIndicatorVisible(true);
		 * suprafataField.setEnabled(false);
		 * suprafataField.setPlaceholder("Suprafata Teren");
		 * formLayout1.add(suprafataField);
		 */

		// alege fermier
		Button searchFermier = new Button("Cauta Fermier", VaadinIcon.SEARCH.create());
		searchFermier.addClickListener(evt -> {
			Dialog dialog = new Dialog();

			dialog.setHeaderTitle("Alege Fermier");

			VerticalLayout contentLayout = new VerticalLayout();
			contentLayout.setAlignItems(Alignment.CENTER);
			contentLayout.setJustifyContentMode(JustifyContentMode.CENTER);

			HorizontalLayout searchLayout = new HorizontalLayout();
			searchLayout.setAlignItems(Alignment.CENTER);
			searchLayout.setJustifyContentMode(JustifyContentMode.CENTER);
			TextField searchField = new TextField("Cauta CNP/CUI");
			searchField.setPlaceholder("CNP sau CUI");
			searchField.setRequiredIndicatorVisible(true);
			searchField.setErrorMessage("CNP/CUI Required !");
			searchField.setValueChangeMode(ValueChangeMode.TIMEOUT);
			searchField.setValueChangeTimeout(1000);
			searchField.setWidth("100%");
			searchField.setMaxLength(14);

			ListBox<String> listbox = new ListBox<String>();

			List<FERMIERI> ferms = fermierRepo.findAll();
			List<String> fermsp = new ArrayList<String>();
			for (FERMIERI f : ferms) {
				fermsp.add(f.getID() + ":" + f.getDENUMIRE_FERMIER());

			}
			listbox.setItems(fermsp);

			Button searchbtn = new Button("Cauta", VaadinIcon.SEARCH.create());
			searchbtn.setEnabled(false);
			searchbtn.addClickListener(event -> {
				listbox.clear();
				fermsp.clear();
				List<FERMIERI> fermsByCnpCui = fermierRepo
						.findByCNP_CUIContainingIgnoreCase(searchField.getValue().trim());
				for (FERMIERI p : fermsByCnpCui) {
					fermsp.add(p.getID() + ":" + p.getDENUMIRE_FERMIER());
				}
				listbox.setItems(fermsp);

			});

			searchLayout.add(searchField, searchbtn);
			contentLayout.add(searchLayout, listbox);
			dialog.add(contentLayout);
			Button ok = new Button("OK", (e) -> {
				int pid = Integer.parseInt(listbox.getValue().substring(0, listbox.getValue().indexOf(":")));
				fermier = fermierRepo.findByIDIgnoreCase(pid);
				dialog.close();
				fermierField.setValue(fermier.getDENUMIRE_FERMIER());

			});
			ok.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			ok.getStyle().set("margin-right", "auto");
			dialog.getFooter().add(ok);
			Button cancel = new Button("Cancel", (e) -> {
				dialog.close();

			});
			cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

			dialog.getFooter().add(cancel);
			dialog.setModal(true);
			dialog.setCloseOnEsc(true);
			dialog.setCloseOnOutsideClick(false);
			UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
				dialog.setWidth(details.getWindowInnerWidth() / 2.8f, Unit.PIXELS);
				dialog.setHeight(details.getWindowInnerHeight() / 1.2f, Unit.PIXELS);

			});
			searchField.addValueChangeListener(vcl -> {
				if (!searchField.isEmpty() && searchField.getValue().trim().length() > 2) {
					searchbtn.setEnabled(true);
				} else {
					searchbtn.setEnabled(false);
				}
			});

			dialog.open();

		});

		fermierField = new TextField("Fermier");
		fermierField.setEnabled(false);
		fermierField.setRequiredIndicatorVisible(true);
		fermierField.setErrorMessage("Alegerea Proprietarului Required !");
		fermierField.setPlaceholder("Nume Proprietar");
		formLayout1.add(searchFermier, fermierField);

		tipContract = new ComboBox<String>("Tip Contract");
		tipContract.setItems("Contract de arendare", "Contract de comodat", "Declaratie", "Schimb lucrativ");
		tipContract.setPlaceholder("Selectare Tip Contract");
		tipContract.setRequiredIndicatorVisible(true);
		binder.forField(tipContract).asRequired().bind(new ValueProvider<AdaugaContract, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(AdaugaContract source) {
				return null;
			}
		}, new Setter<AdaugaContract, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaContract bean, String fieldvalue) {

			}
		});
		tipContract.setErrorMessage("Selectare Tip Contract required !");
		tipContract.addValueChangeListener(eventl -> {
			Optional<Integer> nextContract = contractRepo.getLastNrContractByTip(tipContract.getValue());
			if (nextContract.isEmpty()) {
				numarContract.setValue(1);
			} else {
				numarContract.setValue(nextContract.get());
			}
		});
		formLayout1.add(tipContract);

		dataInceputContract = new DatePicker("Data Inceput Contract");
		dataInceputContract.setLocale(new Locale("ro", "RO"));
		dataInceputContract.setRequiredIndicatorVisible(true);
		binder.forField(dataInceputContract).asRequired().bind(new ValueProvider<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public LocalDate apply(AdaugaContract source) {
				return null;
			}
		}, new Setter<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaContract bean, LocalDate fieldvalue) {

			}
		});
		dataInceputContract.setErrorMessage("Data Inceput Contract required!");
		dataInceputContract.setWidth("100%");
		formLayout1.add(dataInceputContract);

		dataSfarsitContract = new DatePicker("Data Sfarsit Contract");
		dataSfarsitContract.setLocale(new Locale("ro", "RO"));
		dataSfarsitContract.setRequiredIndicatorVisible(true);
		binder.forField(dataSfarsitContract).asRequired().bind(new ValueProvider<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public LocalDate apply(AdaugaContract source) {
				return null;
			}
		}, new Setter<AdaugaContract, LocalDate>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(AdaugaContract bean, LocalDate fieldvalue) {

			}
		});
		dataSfarsitContract.setErrorMessage("Data Sfarsit Contract required!");
		dataSfarsitContract.setWidth("100%");
		dataSfarsitContract.addValueChangeListener(eventsc -> {
			if (!dataInceputContract.isEmpty() && !dataSfarsitContract.isEmpty()) {
				LocalDate st = dataInceputContract.getValue();
				st.format(DateTimeFormatter.ISO_LOCAL_DATE);
				LocalDate sf = dataSfarsitContract.getValue();
				sf.format(DateTimeFormatter.ISO_LOCAL_DATE);
				LocalDate now = LocalDate.now();

				Period p1 = Period.between(st, sf);
				if (p1.getYears() < 2 && p1.getYears() > 0) {
					perioada.setValue(String.valueOf(p1.getYears()) + " AN");
				} else if (p1.getYears() > 1) {
					perioada.setValue(String.valueOf(p1.getYears()) + " ANI");
				} else {
					perioada.setValue("Erroare(an<1) sau ?!?!?");
				}
				// System.out.println("Now is after st "+now.isAfter(st) +"
				// "+st.getDayOfMonth()+"-"+st.getMonth()+"-"+st.getYear());
				// System.out.println("Now is before sf "+now.isBefore(sf) +"
				// "+sf.getDayOfMonth()+"-"+sf.getMonth()+"-"+sf.getYear());

				boolean df = (now.isAfter(st) && now.isBefore(sf));
				if (df || (now.isEqual(st) && now.isBefore(sf))) {
					valabilitateContract.setValue("Valabil");

				} else {
					valabilitateContract.setValue("Expirat");
				}
				// TODO Prelungit Automat; reziliat
			}

		});
		formLayout1.add(dataSfarsitContract);

		perioada = new TextField("Perioada Contract");
		perioada.setEnabled(false);
		perioada.setPlaceholder("Perioada Contract");
		formLayout1.add(perioada);

		FileBuffer FileMemoryBuffer = new FileBuffer();
		upload = new Upload(FileMemoryBuffer);
		Button uploadButton = new Button("Upload PDF...");
		uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		upload.setUploadButton(uploadButton);
		upload.setAcceptedFileTypes("application/pdf", ".pdf");
		upload.setMaxFiles(1);
		upload.setDropAllowed(false);
		int maxFileSizeInBytes = 15 * 1024 * 1024; // 15MB
		upload.setMaxFileSize(maxFileSizeInBytes);
		upload.setMinWidth(15, Unit.EM);

		upload.addSucceededListener(event -> {

			fileData = FileMemoryBuffer.getInputStream();
			String fileName = event.getFileName();
			String mimeType = event.getMIMEType();
			System.out.println(fileName + " " + mimeType);
		});
		upload.addFileRejectedListener(event -> {
			String errorMessage = event.getErrorMessage();

			Notification notification = Notification.show(errorMessage, 5000, Notification.Position.BOTTOM_CENTER);
			notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
		});

		upload.getElement().addEventListener("max-files-reached-changed", event -> {
			boolean maxFilesReached = event.getEventData().getBoolean("event.detail.value");
			uploadButton.setEnabled(!maxFilesReached);
		}).addEventData("event.detail.value");

		formLayout1.add(upload);

		formLayout1.setResponsiveSteps(
				// Use one column by default
				new ResponsiveStep("0", 1),
				// Use two columns, if layout's width exceeds 500px
				new ResponsiveStep("500px", 2));
		formLayout1.setColspan(searchProprietar, 2);
		formLayout1.setColspan(gridprops, 2);
		formLayout1.setColspan(searchTeren, 2);
		formLayout1.setColspan(gridtpcfsup, 2);

		HorizontalLayout fields4 = new HorizontalLayout();
		fields4.setAlignItems(Alignment.CENTER);

		Button submit = new Button("Insert");
		submit.setEnabled(false);
		submit.addClickListener(evt -> {

			CONTRACTE contract = new CONTRACTE();
			contract.setNUMAR_CONTRACT(numarContract.getOptionalValue());
			contract.setDATA_CONTRACT(Date.valueOf(dataContract.getValue()));
			contract.setVALABILITATE_CONTRACT(valabilitateContract.getValue().trim());
			contract.setProprietarID(proprietar);
			contract.setNUMAR_CONTRACT_FERMIER(numarContractFermier.getValue());
			contract.setDATA_CONTRACT_FERMIER(Date.valueOf(dataContractFermier.getValue()));
			contract.setID_TEREN(teren);
			contract.setFERMIER(fermier);
			contract.setTIP_CONTRACT(tipContract.getValue().trim());
			contract.setDATA_INCEPUT_CONTRACT(Date.valueOf(dataInceputContract.getValue()));
			contract.setDATA_SFARSIT_CONTRACT(Date.valueOf(dataSfarsitContract.getValue()));
			contract.setPERIOADA_CONTRACT();

			if (fileData != null) {
				Blob fd = null;
				try {
					fd = new Blob(IOUtils.toByteArray(fileData), new ExceptionInterceptor() {

						@Override
						public Exception interceptException(Exception sqlEx) {
							System.out.println("Erroare1 la upload PF!");
							return null;
						}

						@Override
						public ExceptionInterceptor init(Properties props, Log log) {
							System.out.println("Erroare2 la upload PF!");
							return null;
						}

						@Override
						public void destroy() {
							System.out.println("Erroare3 la upload PF!");

						}
					});
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}

				contract.setDOSAR(fd);
			}
			contractRepo.save(contract);
			clear();

			Notification notification = Notification.show("Insertion(Contract)...Done !", 5000,
					Notification.Position.BOTTOM_END);
			notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
		});
		Button clear = new Button("Clear");
		clear.addClickListener(event -> {

			clear();
			Notification.show("Field Reset...Done!", 5000, Notification.Position.BOTTOM_END);

		});
		binder.addStatusChangeListener(event -> {

			if (binder.isValid()) {
				submit.setEnabled(true);
			} else {
				submit.setEnabled(false);
			}
		});
		fields4.add(submit);
		fields4.add(clear);

		add(content);
		add(fields4);
		setSizeFull();
		setJustifyContentMode(JustifyContentMode.START);
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getStyle().set("text-align", "center");
	}

	public void clear() {
		numarContract.clear();
		dataContract.clear();
		dataContractFermier.clear();
		dataInceputContract.clear();
		dataSfarsitContract.clear();
		// proprietarField.clear();
		valabilitateContract.clear();
		tipContract.clear();
		// tarparField.clear();
		// cfField.clear();
		// suprafataField.clear();
		gridtpcfsup.removeAllColumns();
		gridprops.removeAllColumns();
		perioada.clear();
		numarContractFermier.clear();
		upload.clearFileList();
	}
}
