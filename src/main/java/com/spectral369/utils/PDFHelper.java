package com.spectral369.utils;

import java.io.File;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.Text;

public class PDFHelper {

	public static final Style bold10nr = new Style();
	public static final Style bold12nr = new Style();
	public static final Style bold14nr = new Style();
	public static final Style bold9nr = new Style();

	public static Paragraph getPlainStr(final String str) {
		Paragraph par = new Paragraph();
		par.add(new Tab());
		Text plainstr = new Text(str).simulateBold();
		par.add(plainstr);
		par.add(new Tab());

		return par;
	}

	public static Text addTab() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 8; i++)
			sb.append("\u00a0");
		return new Text(sb.toString());
	}

	public static Paragraph getEmptySpace(int size) {
		Paragraph space = new Paragraph();
		space.setMaxWidth(size);
		for (int i = 0; i < size; i++) {
			space.add("\u00a0");
		}
		return space;
	}

	public static Paragraph createAdjustableParagraph(int widthInSpaces, Paragraph innerContent) {
		AutoScalingParagraph paragraph = new AutoScalingParagraph(innerContent);
		// paragraph.setBorder(new SolidBorder(1));

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < widthInSpaces; i++) {
			sb.append("\u00a0");
		}
		paragraph.add(sb.toString());
		return paragraph;
	}

	public static Paragraph getPhraseStrWithDots(final int dots, final String str) {
		final int strSize = str.length();
		final Paragraph sb = new Paragraph();
		int dotsRemained;
		if (strSize > dots) {
			dotsRemained = 0;
		} else {
			int nrLitereMari = 0;
			int nrLitereMici = 0;
			for (int k = 0; k < str.length(); k++) {
				if (Character.isUpperCase(str.charAt(k)))
					nrLitereMari++;
				if (Character.isLowerCase(str.charAt(k)))
					nrLitereMici++;
			}
			if (nrLitereMari > 0) {
				dotsRemained = dots - (nrLitereMari * 2);
				dotsRemained = dotsRemained - nrLitereMici;
			} else {
				dotsRemained = dots - strSize;
			}
			// dotsRemained = dots - strSize;
		}

		Paragraph chDots = new Paragraph();

		for (int i = 0; i < dotsRemained; ++i) {
			if (i == dotsRemained / 2) {

				sb.add(chDots);
				sb.add(new Text(str).addStyle(bold12nr).setTextRise(1.7f));
			}
			sb.add(".");
		}
		sb.add(chDots);
		return sb;
	}

	public static String getStrWithDots(final int dots, final String str) {
		final int strSize = str.length();
		final StringBuilder sb = new StringBuilder();
		int dotsRemained;
		if (strSize > dots) {
			dotsRemained = 0;
		} else {
			dotsRemained = dots - strSize;
		}
		for (int i = 0; i < dotsRemained; ++i) {
			if (i == dotsRemained / 2) {
				sb.append(str);
			}
			sb.append(".");
		}
		return sb.toString();
	}

	public static String getStrWithDash(final int dots, final String str) {
		final int strSize = str.length();
		final StringBuilder sb = new StringBuilder();
		int dotsRemained;
		if (strSize > dots) {
			dotsRemained = 0;
		} else {
			dotsRemained = dots - strSize;
		}
		for (int i = 0; i < dotsRemained; ++i) {
			if (i == dotsRemained / 2) {
				sb.append(str);
			}
			sb.append("_");
		}
		return sb.toString();
	}

	public static String getStrWithSpace(final int dots, final String str) {
		final int strSize = str.length();
		final StringBuilder sb = new StringBuilder();
		int dotsRemained;
		if (strSize > dots) {
			dotsRemained = 0;
		} else {
			dotsRemained = dots - strSize;
		}
		for (int i = 0; i < dotsRemained; ++i) {
			if (i == dotsRemained / 2) {
				sb.append(str);
			}
			sb.append("\u00a0");
		}
		return sb.toString();
	}

	public static Image getAntetLogo() {
		Image antetLogo = null;
		try {

			antetLogo = new Image(ImageDataFactory
					.create(Utils.getResourcePath() + "images" + File.separator + "Stema_Romaniei.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return antetLogo;
	}

	// stackoverflow solution
	public static String capitalizeWords(final String words) {
		return Stream.of(words.trim().split("\\s")).filter(word -> word.length() > 0)
				.map(word -> word.substring(0, 1).toUpperCase() + word.substring(1)).collect(Collectors.joining(" "));
	}

	public static String capitalizeWordsWithSpace(final String words) {
		return Stream.of(words.split("\\s")).filter(word -> word.length() > 0)
				.map(word -> word.substring(0, 1).toUpperCase() + word.substring(1)).collect(Collectors.joining(" "));
	}

	public static String solveIfEmpty(String str) {

		if (str.isEmpty() || str.isBlank()) {
			str = "________";
		}
		return str;
	}

}