package com.spectral369.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.spectral369.db.contracte.CONTRACTE;
import com.spectral369.db.contracte.ContracteRepository;

@Configuration
@EnableScheduling
public class Schdules {

	private ContracteRepository contracteRepo;

	public void setContracteRepo(ContracteRepository contracteRepo) {
		this.contracteRepo = contracteRepo;
	}

	@Scheduled(cron = "0 0 1 * * *") // orig @daily
	public void runTasks() {
		List<CONTRACTE> contracte = contracteRepo.findAll();
		for (CONTRACTE contract : contracte) {
			int ID = contract.getID();
			LocalDate st = contract.getDATA_INCEPUT_CONTRACT().toLocalDate();
			st.format(DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate sf = contract.getDATA_SFARSIT_CONTRACT().toLocalDate();
			sf.format(DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate now = LocalDate.now();
			boolean df = (now.isAfter(st) && now.isBefore(sf));
			// valabilitate !!!
			if (df || (now.isEqual(st) && now.isBefore(sf))) {
				if (contract.getVALABILITATE_CONTRACT() != "Valabil")
					contracteRepo.updateValabilitateDaily("Valabil", ID);

			} else {
				if (contract.getVALABILITATE_CONTRACT() != "Expirat")
					contracteRepo.updateValabilitateDaily("Expirat", ID);
			}
			// prelungire
			if (contract.getPRELUNGIT() == false && now.isBefore(sf.minusDays(1))) {
				contracteRepo.updatePrelungire(ID);

			}
			contracteRepo.save(contract);
		}

	}

	@Scheduled(cron = "0 0 */3 * * ?")
	public void checkSubs() {
		Utils.setCheckInt(Utils.checkIfOK(Utils.getid()));

	}

}
