package com.spectral369.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Utils {

	private static String basepath = null;
	public static final String FOLDER_PDF = "pdfs";
	public static DateTimeFormatter DATETIMEFORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH:mm");
	public static DateTimeFormatter DATETIMEFORMATTER_TO_DATE = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH:mm");
	public static DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//	public static Locale RO_LOCALE = new Locale("ro", "RO"); //deprecated 
	public static Locale RO_LOCALE = new Locale.Builder().setLanguage("ro").setRegion("RO").build();
	private static final String CONFIG_FILENAME = "pdvapia.config";
	public static int CODE;

	public static void setBasePath(String args) {
		basepath = args;
	}

	public static boolean isSetupFilePresent() {

		File f = new File(basepath + "//configuration.ini");
		return f.getAbsoluteFile().exists();// check
	}

	public static File getSetupFile() {

		return new File(basepath + "//configuration.ini");
	}

	public static String getSaveFileLocation(String fileName) {

		LocalDate date = LocalDate.now();
		String year = String.valueOf(date.getYear());
		String day = String.valueOf(date.getDayOfMonth() + "-" + date.getMonthValue() + "-" + date.getYear());

		File dir = new File(basepath + File.separator + Utils.FOLDER_PDF);
		if (!dir.exists())
			dir.mkdirs();
		File yearDir = new File(dir.getPath() + File.separator + year);
		if (!yearDir.exists())
			yearDir.mkdirs();
		File dateDir = new File(yearDir.getPath() + File.separator + day);
		if (!dateDir.exists())
			dateDir.mkdirs();
		return dateDir.getPath() + File.separator + fileName;

	}

	public static String getResourcePath() {

		// URL resource1 = Utils.class.getClassLoader().getResource("");
		String path = Utils.class.getClassLoader().getResource("").getPath();
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			path = path.substring(1);
		}
		return path + "META-INF" + File.separator + "resources" + File.separator;
	}

	public static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static String ProcesareTipProprietar(String Localitate, String TipPersoana) {
		if (Localitate.toLowerCase().contains("dudestii") && Localitate.toLowerCase().contains("vechi")
				&& TipPersoana.toLowerCase().contains("fizica"))
			return String.valueOf(1);
		else if (Localitate.toLowerCase().contains("cheglevici") && TipPersoana.toLowerCase().contains("fizica"))
			return String.valueOf(1);
		else if (Localitate.toLowerCase().contains("bulgara") && TipPersoana.toLowerCase().contains("fizica"))
			return String.valueOf(1);
		else if (Localitate.toLowerCase().contains("cociohat") && TipPersoana.toLowerCase().contains("fizica"))
			return String.valueOf(1);
		else if ((!Localitate.toLowerCase().contains("bulgara") || !Localitate.toLowerCase().contains("cheglevici")
				|| (!Localitate.toLowerCase().contains("dudestii") && !Localitate.toLowerCase().contains("vechi")))
				&& TipPersoana.toLowerCase().contains("fizica"))
			return String.valueOf(2);
		if (Localitate.toLowerCase().contains("dudestii") && Localitate.toLowerCase().contains("vechi")
				&& TipPersoana.toLowerCase().contains("juridic"))
			return String.valueOf(3);
		else if (Localitate.toLowerCase().contains("cheglevici") && TipPersoana.toLowerCase().contains("juridic"))
			return String.valueOf(3);
		else if (Localitate.toLowerCase().contains("bulgara") && TipPersoana.toLowerCase().contains("juridic"))
			return String.valueOf(3);
		else if (Localitate.toLowerCase().contains("cociohat") && TipPersoana.toLowerCase().contains("juridic"))
			return String.valueOf(3);
		else if ((!Localitate.toLowerCase().contains("bulgara") || !Localitate.toLowerCase().contains("cheglevici")
				|| (!Localitate.toLowerCase().contains("dudestii") && !Localitate.toLowerCase().contains("vechi")))
				&& TipPersoana.toLowerCase().contains("juridic"))
			return String.valueOf(4);

		else
			return "err";
	}

	public static int checkIfOK(String ID) {
		URL url = null;
		int days = 0;
		try {
			url = URI.create("https://subscription.freelancingpeter.eu/params/" + ID).toURL();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
			for (String line; (line = reader.readLine()) != null;) {
				days = Integer.parseInt(line.substring(line.lastIndexOf(":") + 1).trim());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Days remaining: " + days);

		if (days > 5 && days < 10)
			return 2;
		else if (days > 0 && days <= 5)
			return 1;
		else if (days < 1) {
			return 3;
		} else
			return 0;
		// return 0;
	}

	public static String getid() {
		String OS = System.getProperty("os.name").toLowerCase();
		String machineId = null;
		if (OS.indexOf("win") >= 0) {
			StringBuffer output = new StringBuffer();
			Process process;
			String[] cmd = { "cmd", "/c", "reg query", "HKLM\\SOFTWARE\\Microsoft\\Cryptography\\", "/v",
					"MachineGuid" };
			try {
				process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
				BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = "";
				while ((line = reader.readLine()) != null) {
					if (line.contains("REG_SZ"))
						output.append(line.substring(line.indexOf("REG_SZ") + 6).trim());

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			machineId = output.toString();
		} else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0) {
			StringBuffer output = new StringBuffer();
			Process process;
			String[] cmd = { "/bin/sh", "-c", "cat /var/lib/dbus/machine-id" };
			try {
				process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
				BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = "";
				while ((line = reader.readLine()) != null) {
					output.append(line.trim());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			machineId = output.toString().replace(System.getProperty("line.separator"), "").trim();
		}

		return getMachineName().concat("_Apia_").concat(machineId);
	}

	public static String getMachineName() {
		String hostname = new String("unknown");
		try {
			InetAddress addr;
			addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch (UnknownHostException ex) {
			System.out.println("Hostname can not be resolved");
		}

		return hostname.trim();
	}

	public static boolean saveSettings(Map<String, String[]> map) {
		String homedir = System.getProperty("user.home");
		File configFile = new File(homedir + File.separator + CONFIG_FILENAME);
		FileWriter fw;
		StringBuilder strBuilder = new StringBuilder();
		try {
			fw = new FileWriter(configFile);

			for (Map.Entry<String, String[]> item : map.entrySet()) {
				String key = item.getKey();
				String[] vals = item.getValue();
				StringBuilder sb = new StringBuilder();
				for (String sub_item : vals) {
					sb.append(sub_item);
					sb.append(";");
				}
				strBuilder.append(key + ":" + sb);
				strBuilder.append(System.lineSeparator());

			}
			fw.write(strBuilder.toString());

			fw.close();

		} catch (IOException e) {

			return false;
		}

		return true;
	}

	public static Map<String, String[]> getSettingsInfo() {

		Map<String, String[]> map = new HashMap<String, String[]>();
		String homedir = System.getProperty("user.home");
		File configFile = new File(homedir + File.separator + CONFIG_FILENAME);
		FileReader fr;
		try {
			fr = new FileReader(configFile);

			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] data = line.split(":");
				String[] dataValues = data[1].split(";");

				map.put(data[0], dataValues);

			}
			br.close();
			fr.close();
		} catch (IOException e) {
			return null;
		}

		if (map.isEmpty())
			return null;
		else

			return map;
	}

	public static void setCheckInt(int code) {
		CODE = code;
	}

}
