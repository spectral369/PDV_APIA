package com.spectral369;

import com.spectral369.utils.Utils;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * The entry point of the Spring Boot application.
 *
 * Use the @PWA annotation make the application installable on phones, tablets
 * and some desktop browsers.
 *
 */
@SpringBootApplication
@PWA(name = "PDV APIA", shortName = "PDVAPIA", description = "Primaria Dudestii-Vechi - APIA Management Application", iconPath = "icons/log.png", offlineResources = {
		"icons/log.png" })
@Theme(value = "pdvapia")
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {

	private static final long serialVersionUID = 5150591947076584183L;

	public static void main(String[] args) {
		Utils.setBasePath(System.getProperty("user.home"));
		Utils.setCheckInt(Utils.checkIfOK(Utils.getid()));
		SpringApplication.run(Application.class, args);
	}

}
