package com.spectral369.db.contracte;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spectral369.db.fermieri.FERMIERI;

public interface ContracteRepository extends JpaRepository<CONTRACTE, Integer> {
	@Query("from CONTRACTE e where e.NUMAR_CONTRACT = :NRCONTRACT")
	List<CONTRACTE> findByNrContractContainingIgnoreCase(@Param("NRCONTRACT") String numar_contract);

	@Query("SELECT TIP_CONTRACT from CONTRACTE e where e.TIP_CONTRACT = :TIP")
	Optional<Integer> getLastNrContractByTip(@Param("TIP") String tipContract);

	@Modifying
	@Query("UPDATE CONTRACTE c SET c.VALABILITATE_CONTRACT = :VALABILITATE where c.id = :ID")
	void updateValabilitateDaily(@Param("VALABILITATE") String Valabilitate, @Param("ID") int ID);

	@Query("from CONTRACTE e where e.FERMIER = :FERMIER")
	List<CONTRACTE> getContracteByFermier(@Param("FERMIER") FERMIERI fermier);

	@Modifying
	@Query("UPDATE CONTRACTE c SET c.PRELUNGIT = true where c.id = :ID")
	void updatePrelungire(@Param("ID") int ID);
}
