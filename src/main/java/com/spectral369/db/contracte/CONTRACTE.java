package com.spectral369.db.contracte;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.format.annotation.DateTimeFormat;

import com.spectral369.db.fermieri.FERMIERI;
import com.spectral369.db.proprietari.PROPRIETARI;
import com.spectral369.db.terenuri.TERENURI;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "contracte")
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class CONTRACTE implements Serializable {

	private static final long serialVersionUID = -4570338844467331521L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	@NotNull
	private int NUMAR_CONTRACT;
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date DATA_CONTRACT;
	@NotNull
	private String VALABILITATE_CONTRACT;
	@ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//	@JoinColumn(name = "proprietari", referencedColumnName = "ID")
	@JoinTable(name = "proprietari", joinColumns = { @JoinColumn(name = "id") })
	private List<PROPRIETARI> ProprietarID;
	@NotNull
	private int NUMAR_CONTRACT_FERMIER;
	@NotNull
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date DATA_CONTRACT_FERMIER;
	@ManyToMany(cascade = CascadeType.REFRESH)
//	@JoinColumn(name = "terenuri", referencedColumnName = "ID")
	@JoinTable(name = "terenuri", joinColumns = { @JoinColumn(name = "id") })
	private List<TERENURI> ID_TEREN;
	@NotNull
	private String TIP_CONTRACT;
	@NotNull
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date DATA_INCEPUT_CONTRACT;
	@NotNull
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date DATA_SFARSIT_CONTRACT;
	@NotNull
	private String PERIOADA_CONTRACT;
	@Lob
	private Blob DOSAR;
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "fermieri", referencedColumnName = "ID")
	private FERMIERI FERMIER;
	@NotNull
	@Column(columnDefinition = "boolean default false")
	private Boolean PRELUNGIT = Boolean.FALSE;
	@NotNull
	@Column(columnDefinition = "boolean default false")
	private Boolean INCETAT = Boolean.FALSE;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getNUMAR_CONTRACT() {

		return NUMAR_CONTRACT;
	}

	public void setNUMAR_CONTRACT(Optional<Integer> ultimulNrDeContract) {
		if (ultimulNrDeContract.isEmpty()) {
			NUMAR_CONTRACT = 1;
		} else {
			NUMAR_CONTRACT = ultimulNrDeContract.get().intValue() + 1;
		}

	}

	public Date getDATA_CONTRACT() {
		return DATA_CONTRACT;
	}

	public void setDATA_CONTRACT(Date dATA_CONTRACT) {
		DATA_CONTRACT = dATA_CONTRACT;
	}

	public String getVALABILITATE_CONTRACT() {
		return VALABILITATE_CONTRACT;
	}

	public void setVALABILITATE_CONTRACT(String vALABILITATE_CONTRACT) {
		VALABILITATE_CONTRACT = vALABILITATE_CONTRACT;
	}

	public List<PROPRIETARI> getProprietarID() {
		return ProprietarID;
	}

	public void setProprietarID(PROPRIETARI proprietarID) {
		ProprietarID.add(proprietarID);
	}

	public void setProprietarID(List<PROPRIETARI> proprietarID) {
		ProprietarID = proprietarID;
	}

	public int getNUMAR_CONTRACT_FERMIER() {
		return NUMAR_CONTRACT_FERMIER;
	}

	public void setNUMAR_CONTRACT_FERMIER(int nUMAR_CONTRACT_FERMIER) {
		NUMAR_CONTRACT_FERMIER = nUMAR_CONTRACT_FERMIER;
	}

	public Date getDATA_CONTRACT_FERMIER() {
		return DATA_CONTRACT_FERMIER;
	}

	public void setDATA_CONTRACT_FERMIER(Date dATA_CONTRACT_FERMIER) {
		DATA_CONTRACT_FERMIER = dATA_CONTRACT_FERMIER;
	}

	public List<TERENURI> getID_TEREN() {
		return ID_TEREN;
	}

	public void setID_TEREN(TERENURI iD_TEREN) {
		ID_TEREN.add(iD_TEREN);
	}

	public void setID_TEREN(List<TERENURI> iD_TEREN) {
		ID_TEREN = iD_TEREN;
	}

	public String getTIP_CONTRACT() {
		return TIP_CONTRACT;
	}

	public void setTIP_CONTRACT(String tIP_CONTRACT) {
		TIP_CONTRACT = tIP_CONTRACT;
	}

	public Date getDATA_INCEPUT_CONTRACT() {
		return DATA_INCEPUT_CONTRACT;
	}

	public void setDATA_INCEPUT_CONTRACT(Date dATA_INCEPUT_CONTRACT) {
		DATA_INCEPUT_CONTRACT = dATA_INCEPUT_CONTRACT;
	}

	public Date getDATA_SFARSIT_CONTRACT() {
		return DATA_SFARSIT_CONTRACT;
	}

	public void setDATA_SFARSIT_CONTRACT(Date dATA_SFARSIT_CONTRACT) {
		DATA_SFARSIT_CONTRACT = dATA_SFARSIT_CONTRACT;
	}

	public String getPERIOADA_CONTRACT() {
		return PERIOADA_CONTRACT;
	}

	public void setPERIOADA_CONTRACT() {
		LocalDate inceput = DATA_INCEPUT_CONTRACT.toLocalDate();
		LocalDate sfarsit = DATA_SFARSIT_CONTRACT.toLocalDate();

		Period p1 = Period.between(inceput, sfarsit);
		if (p1.getYears() < 2 && p1.getYears() > 0) {
			PERIOADA_CONTRACT = p1.getYears() + " AN";
		} else if (p1.getYears() > 1) {
			PERIOADA_CONTRACT = p1.getYears() + " ANI";
		} else {
			PERIOADA_CONTRACT = "Erroare(an<1) sau ?!?!?";
		}

	}

	public Blob getDOSAR() {
		return DOSAR;
	}

	public void setDOSAR(Blob dOSAR) {
		DOSAR = dOSAR;
	}

	public FERMIERI getFERMIER() {
		return FERMIER;
	}

	public void setFERMIER(FERMIERI fERMIER) {
		FERMIER = fERMIER;
	}

	public Boolean getPRELUNGIT() {
		return PRELUNGIT;
	}

	public void setPRELUNGIT(Boolean pRELUNGIT) {
		PRELUNGIT = pRELUNGIT;
	}

	public Boolean getINCETAT() {
		return INCETAT;
	}

	public void setINCETAT(Boolean iNCETAT) {
		INCETAT = iNCETAT;
	}

}
