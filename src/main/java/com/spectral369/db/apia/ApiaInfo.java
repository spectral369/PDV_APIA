package com.spectral369.db.apia;

public record ApiaInfo(Integer NrCrt, String Utilizator, String TarPar, Integer BlocFizic, Double SupfrafataUtilizata,
		String Forma, String Categorie, String Proprietar, String NrDataContract, String ExpirareContract, String CNP,
		String Domiciliu, String NrCasa, String Judet, String Telefon, Integer Rol, String Volum, Integer Pozitie,
		String Tip) {
}
