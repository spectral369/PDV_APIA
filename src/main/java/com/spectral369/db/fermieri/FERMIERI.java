package com.spectral369.db.fermieri;

import java.io.Serializable;
import java.sql.Blob;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "fermieri")
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class FERMIERI implements Serializable {

	private static final long serialVersionUID = 5001348831408262047L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	@NotNull
	@Column(unique = true)
	private int ID_FERMIER;
	// @NotNull
	@Column(unique = true, nullable = true)
	private String ID_APIA;
	@NotNull
	private String DENUMIRE_FERMIER;
	@Column(unique = true, nullable = true)
	private String CNP_CUI;
	@NotNull
	private String ADRESA_LOCALITATE;
	private String ADRESA_STRADA;
	@NotNull
	private String ADRESA_NUMAR;
	private String ADRESA_BLOC;
	private String ADRESA_ETAJ;
	private String ADRESA_APARTAMENT;
	@NotNull
	private String ADRESA_JUDET;
	@NotNull
	private String TELEFON;
	// @NotNull
	private String VOLUM;
	// @NotNull
	private int POZITIE;
	@NotNull
	private String TIP;
	@Column(unique = true, nullable = true)
	// @NotNull
	private Integer ROL;
	@NotNull
	private String PF_PJ;
	private String REPREZENTANT_PJ;
	@Lob
	private Blob DOSAR;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getDENUMIRE_FERMIER() {
		return DENUMIRE_FERMIER;
	}

	public void setDENUMIRE_FERMIER(String dENUMIRE_FERMIER) {
		DENUMIRE_FERMIER = dENUMIRE_FERMIER;
	}

	public String getCNP_CUI() {
		return CNP_CUI;
	}

	public void setCNP_CUI(String cNP_CUI) {
		CNP_CUI = cNP_CUI;
	}

	public String getADRESA_LOCALITATE() {
		return ADRESA_LOCALITATE;
	}

	public void setADRESA_LOCALITATE(String aDRESA_LOCALITATE) {
		ADRESA_LOCALITATE = aDRESA_LOCALITATE;
	}

	public String getADRESA_STRADA() {
		return ADRESA_STRADA;
	}

	public void setADRESA_STRADA(String aDRESA_STRADA) {
		ADRESA_STRADA = aDRESA_STRADA;
	}

	public String getADRESA_NUMAR() {
		return ADRESA_NUMAR;
	}

	public void setADRESA_NUMAR(String aDRESA_NUMAR) {
		ADRESA_NUMAR = aDRESA_NUMAR;
	}

	public String getADRESA_BLOC() {
		return ADRESA_BLOC;
	}

	public void setADRESA_BLOC(String aDRESA_BLOC) {
		ADRESA_BLOC = aDRESA_BLOC;
	}

	public String getADRESA_ETAJ() {
		return ADRESA_ETAJ;
	}

	public void setADRESA_ETAJ(String aDRESA_ETAJ) {
		ADRESA_ETAJ = aDRESA_ETAJ;
	}

	public String getADRESA_APARTAMENT() {
		return ADRESA_APARTAMENT;
	}

	public void setADRESA_APARTAMENT(String aDRESA_APARTAMENT) {
		ADRESA_APARTAMENT = aDRESA_APARTAMENT;
	}

	public String getADRESA_JUDET() {
		return ADRESA_JUDET;
	}

	public void setADRESA_JUDET(String aDRESA_JUDET) {
		ADRESA_JUDET = aDRESA_JUDET;
	}

	public String getTELEFON() {
		return TELEFON;
	}

	public void setTELEFON(String tELEFON) {
		TELEFON = tELEFON;
	}

	public String getVOLUM() {
		return VOLUM;
	}

	public void setVOLUM(String vOLUM) {
		VOLUM = vOLUM;
	}

	public int getPOZITIE() {
		return POZITIE;
	}

	public void setPOZITIE(int pOZITIE) {
		POZITIE = pOZITIE;
	}

	public String getTIP() {
		return TIP;
	}

	public void setTIP(String tIP) {
		TIP = tIP;
	}

	public Integer getROL() {
		return ROL;
	}

	public void setROL(Integer rOL) {
		ROL = rOL;
	}

	public String getPF_PJ() {
		return PF_PJ;
	}

	public void setPF_PJ(String pF_PJ) {
		PF_PJ = pF_PJ;
	}

	public String getREPREZENTANT_PJ() {
		return REPREZENTANT_PJ;
	}

	public void setREPREZENTANT_PJ(String rEPREZENTANT_PJ) {
		REPREZENTANT_PJ = rEPREZENTANT_PJ;
	}

	public Blob getDOSAR() {
		return DOSAR;
	}

	public void setDOSAR(Blob dOSAR) {
		DOSAR = dOSAR;
	}

	public int getID_FERMIER() {
		return ID_FERMIER;
	}

	public void setID_FERMIER(int iD_FERMIER) {
		ID_FERMIER = iD_FERMIER;
	}

	public String getID_APIA() {
		return ID_APIA;
	}

	public void setID_APIA(String iD_APIA) {
		ID_APIA = iD_APIA;
	}

}