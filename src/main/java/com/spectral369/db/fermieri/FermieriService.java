package com.spectral369.db.fermieri;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class FermieriService {

	private final FermieriRepository fermieriRepository;

	public FermieriService(FermieriRepository fermieriRepo) {

		this.fermieriRepository = fermieriRepo;

	}

	public List<FERMIERI> findAllFermieri(String stringFilter) {
		if (stringFilter == null || stringFilter.isEmpty()) {

			return (List<FERMIERI>) fermieriRepository.findAll();
		} else {
			// return userRepository..search(stringFilter);
		}
		return null;
	}

	public long countUsers() {
		return fermieriRepository.count();
	}

	public void deleteFermier(FERMIERI fermieri) {
		fermieriRepository.delete(fermieri);
	}

	public void saveFermier(FERMIERI fermier) {
		if (fermier == null) {

			System.err.println("Contact is null. Are you sure you have connected your form to the application?");
			return;
		}
		fermieriRepository.save(fermier);
	}

}