package com.spectral369.db.fermieri;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FermieriRepository extends JpaRepository<FERMIERI, Integer> {

	@Query("from FERMIERI e where e.CNP_CUI like %:cnp_cui%")
	List<FERMIERI> findByCNP_CUIContainingIgnoreCase(@Param("cnp_cui") String CNP_CUI);

	@Query("from FERMIERI e where e.Id = :ID")
	FERMIERI findByIDIgnoreCase(@Param("ID") int id);

	@Query("SELECT EXISTS (SELECT ID_APIA from FERMIERI f where f.ID_APIA = :idApia )")
	Optional<Boolean> isIdApiaPresent(@Param("idApia") String idApia);

	/**
	 * 
	 * @param cnp_cui CNP sau CUI pentru cautare
	 * @return 0(false) daca recordul lipseste 1(true) daca este prezent
	 */
	@Query("SELECT EXISTS (SELECT CNP_CUI from FERMIERI f where f.CNP_CUI = :CNP )")
	Optional<Boolean> isCNPPresent(@Param("CNP") String cnp_cui);

}