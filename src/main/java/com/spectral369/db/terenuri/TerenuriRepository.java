package com.spectral369.db.terenuri;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spectral369.db.proprietari.PROPRIETARI;

public interface TerenuriRepository extends JpaRepository<TERENURI, Integer> {
	@Query("from TERENURI e where e.TarPar like %:TARPAR%")
	List<TERENURI> findByTARPARContainingIgnoreCase(@Param("TARPAR") String tar_par);

	/*
	 * @Query("from TERENURI e where e.ProprietarID = :IDPROP") List<TERENURI>
	 * findTerenFromProp(@Param("IDPROP") int id_prop);
	 */
	@Query("from TERENURI e where e.ProprietarID = :IDPROP")
	List<TERENURI> findTerenFromProp2(@Param("IDPROP") PROPRIETARI id_prop);

	@Query("from TERENURI e where e.ProprietarID = :IDPROP AND e.TarPar like %:TARPAR%")
	List<TERENURI> findTerenFromPropAndTarPar(@Param("IDPROP") PROPRIETARI id_prop, @Param("TARPAR") String tar_par);

	@Query("from TERENURI e where e.Id = :ID")
	TERENURI findByIDIgnoreCase(@Param("ID") int id);

}
