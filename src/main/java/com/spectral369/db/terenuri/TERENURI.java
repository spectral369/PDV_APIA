package com.spectral369.db.terenuri;

import java.io.Serializable;
import java.sql.Blob;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.spectral369.db.proprietari.PROPRIETARI;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "terenuri")
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class TERENURI implements Serializable {

	private static final long serialVersionUID = -4907889234773653036L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "ProprietarID", referencedColumnName = "ID"/* , foreignKey = @ForeignKey(name="FK_PROP_ID") */)
	private PROPRIETARI ProprietarID;
	@NotNull
	private String TarPar;
	private String CF;
	private Double SuprafataTotala;
	private Double cota;
	private Double SuprafataUtilizata;
	@NotNull
	private String Categorie;
	private Integer blocFizic;
	private String Contract;
	@Lob
	private Blob DOSAR;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getTarPar() {
		return TarPar;
	}

	public void setTarPar(String tarPar) {
		TarPar = tarPar;
	}

	public String getCF() {
		return CF;
	}

	public void setCF(String cF) {
		CF = cF;
	}

	public Double getSuprafataTotala() {
		return SuprafataTotala;
	}

	public void setSuprafataTotala(Double suprafataTotala) {
		SuprafataTotala = suprafataTotala;
	}

	public Double getCota() {
		return cota;
	}

	public void setCota(Double cota) {
		this.cota = cota;
	}

	public Double getSuprafataUtilizata() {
		return SuprafataUtilizata;
	}

	public void setSuprafataUtilizata(Double suprafataUtilizata) {
		SuprafataUtilizata = suprafataUtilizata;
	}

	public void setSuprafataUtilizata() {
		SuprafataUtilizata = SuprafataTotala * cota;
	}

	public String getCategorie() {
		return Categorie;
	}

	public void setCategorie(String categorie) {
		Categorie = categorie;
	}

	public String getContract() {
		return Contract;
	}

	public void setContract(String contract) {
		Contract = contract;
	}

	public void setContract() {
		Contract = "Fara Contract";
	}

	public Blob getDOSAR() {
		return DOSAR;
	}

	public void setDOSAR(Blob dOSAR) {
		DOSAR = dOSAR;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PROPRIETARI getPoprietarID() {
		return ProprietarID;
	}

	public void setPoprietarID(PROPRIETARI proprietarID) {
		ProprietarID = proprietarID;
	}

	public Integer getBlocFizic() {
		return blocFizic;
	}

	public void setBlocFizic(Integer blocFizic) {
		this.blocFizic = blocFizic;
	}

}
