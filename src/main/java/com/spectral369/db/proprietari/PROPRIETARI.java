package com.spectral369.db.proprietari;

import java.io.Serializable;
import java.sql.Blob;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "proprietari")
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class PROPRIETARI implements Serializable {

	private static final long serialVersionUID = 3312901691718970679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	@NotNull
	private String NumeProprietar;
	@Column(unique = true)
	@NotNull
	private String CNP_CUI;
	@NotNull
	private String ADRESA_LOCALITATE;
	private String ADRESA_STRADA;
	@NotNull
	private String ADRESA_NUMAR;
	private String ADRESA_BLOC;
	private String ADRESA_ETAJ;
	private String ADRESA_APARTAMENT;
	@NotNull
	private String TELEFON;
	@Column(unique = true, nullable = true)
	@Nullable
	// @NotNull
	private Integer ROL;
	@NotNull
	private String PF_PJ;
	private String REPREZENTANT_PJ;
	@Lob
	private Blob DOSAR;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getNumeProprietar() {
		return NumeProprietar;
	}

	public void setNumeProprietar(String numeProprietar) {
		NumeProprietar = numeProprietar;
	}

	public String getCNP_CUI() {
		return CNP_CUI;
	}

	public void setCNP_CUI(String cNP_CUI) {
		CNP_CUI = cNP_CUI;
	}

	public String getADRESA_LOCALITATE() {
		return ADRESA_LOCALITATE;
	}

	public void setADRESA_LOCALITATE(String aDRESA_LOCALITATE) {
		ADRESA_LOCALITATE = aDRESA_LOCALITATE;
	}

	public String getADRESA_STRADA() {
		return ADRESA_STRADA;
	}

	public void setADRESA_STRADA(String aDRESA_STRADA) {
		ADRESA_STRADA = aDRESA_STRADA;
	}

	public String getADRESA_NUMAR() {
		return ADRESA_NUMAR;
	}

	public void setADRESA_NUMAR(String aDRESA_NUMAR) {
		ADRESA_NUMAR = aDRESA_NUMAR;
	}

	public String getADRESA_BLOC() {
		return ADRESA_BLOC;
	}

	public void setADRESA_BLOC(String aDRESA_BLOC) {
		ADRESA_BLOC = aDRESA_BLOC;
	}

	public String getADRESA_ETAJ() {
		return ADRESA_ETAJ;
	}

	public void setADRESA_ETAJ(String aDRESA_ETAJ) {
		ADRESA_ETAJ = aDRESA_ETAJ;
	}

	public String getADRESA_APARTAMENT() {
		return ADRESA_APARTAMENT;
	}

	public void setADRESA_APARTAMENT(String aDRESA_APARTAMENT) {
		ADRESA_APARTAMENT = aDRESA_APARTAMENT;
	}

	public Integer getROL() {
		return ROL;
	}

	public void setROL(Integer rOL) {
		ROL = rOL;
	}

	public String getPF_PJ() {
		return PF_PJ;
	}

	public void setPF_PJ(String pF_PJ) {
		PF_PJ = pF_PJ;
	}

	public String getREPREZENTANT_PJ() {
		return REPREZENTANT_PJ;
	}

	public void setREPREZENTANT_PJ(String rEPREZENTANT_PJ) {
		REPREZENTANT_PJ = rEPREZENTANT_PJ;
	}

	public Blob getDOSAR() {
		return DOSAR;
	}

	public void setDOSAR(Blob dOSAR) {
		DOSAR = dOSAR;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTELEFON() {
		return TELEFON;
	}

	public void setTELEFON(String tELEFON) {
		TELEFON = tELEFON;
	}

}
