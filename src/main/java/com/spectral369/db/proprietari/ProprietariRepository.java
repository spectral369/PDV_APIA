package com.spectral369.db.proprietari;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProprietariRepository extends JpaRepository<PROPRIETARI, Integer> {
	@Query("from PROPRIETARI e where e.CNP_CUI like %:CNPCUI%")
	List<PROPRIETARI> findByCNPCUIContainingIgnoreCase(@Param("CNPCUI") String cnpcui);

	@Query("from PROPRIETARI e where e.id = :ID")
	PROPRIETARI findProprietarByID(@Param("ID") int id);
}
